// Queens3adj.cpp : インプリメンテーション ファイル
//

#include "stdafx.h"
#include "queens3.h"
#include "Queens3adj.h"
#include "queens3Dlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CQueens3adj

IMPLEMENT_DYNCREATE(CQueens3adj, CFrameWnd)

CQueens3adj::CQueens3adj()
{
	m_bKey1Up=true;
}

CQueens3adj::~CQueens3adj()
{
}


BEGIN_MESSAGE_MAP(CQueens3adj, CFrameWnd)
	//{{AFX_MSG_MAP(CQueens3adj)
	ON_WM_KEYDOWN()
	ON_WM_KEYUP()
	ON_WM_PAINT()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CQueens3adj メッセージ ハンドラ

void CQueens3adj::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
//	CDC* pDC;
//	char text[256];
//	int status;

//    SYSTEMTIME st;
	if(m_bKey1Up)
	{
		m_bKey1Up=false;
	    TRACE("OnKeyDown:0x%x\n",nChar);
		if(nChar==0x31) ((CQueens3Dlg *)AfxGetMainWnd())->AdjustOclock();
	}
	CFrameWnd::OnKeyDown(nChar, nRepCnt, nFlags);
}

void CQueens3adj::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	m_bKey1Up=true;
	TRACE("OnKeyUp:0x%x\n",nChar);
	
	CFrameWnd::OnKeyUp(nChar, nRepCnt, nFlags);
}


void CQueens3adj::TextOutFont(int x, int y, CString& str, int point,COLORREF color)
{
    CDC *pDC;
	TEXTMETRIC tm;
	CFont fontText;
	CString strText;
	CSize sizeText;
	COLORREF oldColor;

	pDC=GetDC();
	//pDC->ResetDC();
	fontText.CreateFont(-point,0,0,0,400,FALSE,FALSE,0,
		SHIFTJIS_CHARSET,OUT_DEFAULT_PRECIS,
		CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,
		DEFAULT_PITCH | FF_DONTCARE, "ＭＳ明朝");
	CFont* pOldFont = (CFont*) pDC->SelectObject(&fontText);
	pDC->GetTextMetrics(&tm);
	sizeText=pDC->GetTextExtent(str);
	oldColor=pDC->SetTextColor(color);
	pDC->TextOut(x,y,str);
	pDC->SelectObject(pOldFont);
	pDC->SetTextColor(oldColor);
	ReleaseDC(pDC);
}

void CQueens3adj::OnPaint() 
{
	CString str;
	CPaintDC dc(this); // 描画用のデバイス コンテキスト
	
	str="（重要）";
	TextOutFont(0,0,str,10,RGB(255,0,0));
	str="このウィンドウは時刻設定のために常にアクティブにする必要がありますので";
	TextOutFont(40,0,str,10,RGB(0,0,0));
	str="他のウィンドウを使用したあとは、このウィンドウを選択しておいてください。";
	TextOutFont(40,10,str,10,RGB(0,0,0));
	
	// 描画用メッセージとして CFrameWnd::OnPaint() を呼び出してはいけません
}
