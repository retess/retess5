#if !defined(AFX_QUEENS3TIMER_H__DA79E8A0_C832_49EC_BD8B_2C02B0AE8E60__INCLUDED_)
#define AFX_QUEENS3TIMER_H__DA79E8A0_C832_49EC_BD8B_2C02B0AE8E60__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Queens3timer.h : ヘッダー ファイル
//
#include "queens3midi.h"
#include "Queens3adj.h"


/////////////////////////////////////////////////////////////////////////////
// CQueens3timer スレッド

UINT TimerThreadProc( LPVOID pParam );
void CALLBACK TimeProc(UINT uID,UINT uMsg,DWORD dwUser,DWORD dw1,DWORD dw2);

class CQueens3timer : public CWinThread
{
	DECLARE_DYNCREATE(CQueens3timer)
public:
	CQueens3timer();           // 動的生成に使用されるプロテクト コンストラクタ
	virtual ~CQueens3timer();

// アトリビュート
public:
	HWND m_hWnd;
	UINT ShutDownTime;
	double TideInterval;
	int * seqGroup;
	UINT TideIdx;
	UINT * seqChannel;
	LPSTR * pTopTrack;
	QUEEN_play_time * seqPlayTime;
	UINT * trackEnv;
	UINT * nTrack;
	int * seqInitial;
	BOOL * seqTimerCommand;
	int * seqSirenCommand;
	UINT nseq;
	TimerEntry (* pTimerEntry)[5];
	int TimingCount;
	MMRESULT TimerID;
	BOOL TimeAdjustFlg;
	Cqueens3View * view;
	CQueens3midi * player;
	BOOL start;
	BOOL DisplayFlg;
	CQueens3adj * adjuster;
	CWinThread * TimerThread;

// オペレーション
public:
	void copyMemFromPlayer();
	BOOL TimerSeq(CDC *pDC,int *count,WORD day,WORD hour,WORD minute,WORD second,WORD millisec);
	void deleteMem();
	UINT TimerProc(UINT uID);
	BOOL maintainTime(int *count,CDC *pDC);
//	int GetSensorStep(int ach);
//	void InitSensorStep(int mon);
	void InitSystemDat();
	void UnsetTimer(int part);
	void Recover(int part);
	void ReportLog(DWORD dwEventID,WORD wCategory);
//	void GetTableFromFile(FILE *fp,int *cSt,SensorTable **pSt);
//	int GetStepFromTable(double sens,int stc,SensorTable *stb);
	BOOL ModifyEntryTable(int area, int hour, int select);

// オーバーライド
	// ClassWizard は仮想関数のオーバーライドを生成します。
	//{{AFX_VIRTUAL(CQueens3timer)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// インプリメンテーション
protected:

	// 生成されたメッセージ マップ関数
	//{{AFX_MSG(CQueens3timer)
		// メモ - ClassWizard はこの位置にメンバ関数を追加または削除します。
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ は前行の直前に追加の宣言を挿入します。

#endif // !defined(AFX_QUEENS3TIMER_H__DA79E8A0_C832_49EC_BD8B_2C02B0AE8E60__INCLUDED_)
