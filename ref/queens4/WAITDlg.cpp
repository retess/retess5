// WAITDlg.cpp : インプリメンテーション ファイル
//

#include "stdafx.h"
#include "queens3.h"
#include "WAITDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CWAITDlg ダイアログ


CWAITDlg::CWAITDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CWAITDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CWAITDlg)
		// メモ - ClassWizard はこの位置にマッピング用のマクロを追加または削除します。
	//}}AFX_DATA_INIT
}


void CWAITDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CWAITDlg)
                // メモ - ClassWizard はこの位置にマッピング用のマクロを追加または削除します。
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CWAITDlg, CDialog)
	//{{AFX_MSG_MAP(CWAITDlg)
		// メモ - ClassWizard はこの位置にマッピング用のマクロを追加または削除します。
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CWAITDlg メッセージ ハンドラ
