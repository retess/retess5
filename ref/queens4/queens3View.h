#if !defined(AFX_QUEENS3VIEW_H__622F4D95_F6A3_4427_AB38_EC72041D07B4__INCLUDED_)
#define AFX_QUEENS3VIEW_H__622F4D95_F6A3_4427_AB38_EC72041D07B4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// queens3View.h : ヘッダー ファイル
//

/////////////////////////////////////////////////////////////////////////////
// Cqueens3View ビュー

class Cqueens3View : public CFrameWnd
{
protected:
	DECLARE_DYNCREATE(Cqueens3View)

// アトリビュート
public:

// オペレーション
public:
	virtual ~Cqueens3View();
	Cqueens3View();           // 動的生成に使用されるプロテクト コンストラクタ

// オーバーライド
	// ClassWizard は仮想関数のオーバーライドを生成します。

	//{{AFX_VIRTUAL(Cqueens3View)
	protected:
	virtual void OnDraw(CDC* pDC);      // このビューを描画するためにオーバーライドしました。
	//}}AFX_VIRTUAL

// インプリメンテーション
protected:

	// 生成されたメッセージ マップ関数
protected:
	//{{AFX_MSG(Cqueens3View)
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ は前行の直前に追加の宣言を挿入します。

#endif // !defined(AFX_QUEENS3VIEW_H__622F4D95_F6A3_4427_AB38_EC72041D07B4__INCLUDED_)
