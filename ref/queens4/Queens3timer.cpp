// Queens3timer.cpp : インプリメンテーション ファイル
//

#include "stdafx.h"
#include "queens3.h"
#include "Queens3timer.h"
#include "queens3Dlg.h"

#include <stdio.h>
#include <mmsystem.h>
#include <wtypes.h>
#include <direct.h>
#include <math.h>
#include <windows.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CQueens3timer

IMPLEMENT_DYNCREATE(CQueens3timer, CWinThread)

CQueens3timer::CQueens3timer()
{
	TimeAdjustFlg = FALSE;//DAQStartFlg = 
	TimerID = NULL;
	TimingCount = ShutDownTime = 0;
	seqTimerCommand = (BOOL *)NULL;
	seqSirenCommand = seqInitial = seqGroup = (int *)NULL;
	seqPlayTime = (QUEEN_play_time *)NULL;
	seqChannel = trackEnv = nTrack = (UINT *)NULL;
	pTopTrack = (LPSTR *)NULL;
	DisplayFlg = FALSE;
	pTimerEntry = (TimerEntry (*)[5])NULL;
	adjuster = (CQueens3adj *)NULL;
}

CQueens3timer::~CQueens3timer()
{
	if(TimerID != NULL)
	{
		timeKillEvent(TimerID);
		deleteMem();
	}
	if(TimerThread != NULL) TimerThread = NULL;
}

BOOL CQueens3timer::InitInstance()
{
	// TODO: この位置にスレッド単位の初期化コードを追加してください。
	return TRUE;
}

int CQueens3timer::ExitInstance()
{
	// TODO: この位置にスレッド単位の初期化コードを追加してください。
	return CWinThread::ExitInstance();
}

BEGIN_MESSAGE_MAP(CQueens3timer, CWinThread)
	//{{AFX_MSG_MAP(CQueens3timer)
		// メモ - ClassWizard はこの位置にマッピング用のマクロを追加します。
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CQueens3timer メッセージ ハンドラ

UINT TimerThreadProc( LPVOID pParam )
{
	CWinThread * ct;
	DWORD dwUser;

	ct = AfxGetThread();
	dwUser = (DWORD)pParam;

	TRACE("TimerThreadProc:timeSetEvent %x\n",ct->m_nThreadID);
//	timeSetEvent(1000,0,TimeProc,dwUser,TIME_PERIODIC);
	timeSetEvent(200,0,TimeProc,dwUser,TIME_PERIODIC);

	return(0);
}

void CALLBACK TimeProc(UINT uID,UINT uMsg,DWORD dwUser,DWORD dw1,DWORD dw2)
{
	CWinThread * ct;
	CQueens3timer *pObj = (CQueens3timer *)dwUser;
	ct = AfxGetThread();

//TRACE("TimeProc Start\n");

	if(pObj == NULL)
	{
		timeKillEvent(uID);
	    TRACE("TimeProc:terminated 1 thread=%x",ct->m_nThreadID);
		//::PostThreadMessage((unsigned long)(pObj->m_hWnd),IDM_TIMER_FINISHED,0,0);
		return;
	}
	else if(pObj->start != TRUE)
	{
		timeKillEvent(uID);
		pObj->TimerID = NULL;
		pObj->TimerThread = NULL;
		//pObj->deleteMem();
	    TRACE("TimeProc:terminated 2 thread=%x",ct->m_nThreadID);
		//::PostThreadMessage((unsigned long)(pObj->m_hWnd),IDM_TIMER_FINISHED,0,0);
		return;
	}
	else if(!pObj->IsKindOf(RUNTIME_CLASS(CQueens3timer)))
		return;
  if(pObj->TimerID == NULL)
  {
    TRACE("TimerID=%d\n",uID);
    pObj->TimerID = uID;
  }
	pObj->TimerProc(uID);
	//TRACE("@");

//TRACE("TimeProc End\n");

}


UINT CQueens3timer::TimerProc(UINT uID)
{
	CDC* pDC;
	char text[256],ss[20][2];
	int i,j,tr,vup;
	BOOL fif,fof;
	pDC =(CDC*)NULL;

	if(start)
	{
		if(DisplayFlg && view) 
		{
			pDC = view->GetDC();
		}
		else pDC=NULL;

//		GetAnalogInput(3);
		if(maintainTime(&TimingCount,pDC))
		{
			TimingCount=1;
		}

		if(DisplayFlg && pDC != NULL)
		{
			wsprintf(text," FadeIn =%x     ",player->FIPMIDIChannel);
			pDC->TextOut(590,70,text);
			wsprintf(text," FadeOut=%x     ",player->FOPMIDIChannel);
			pDC->TextOut(590,90,text);
			pDC->TextOut(620,10,"Playing Sequence");
			for(i=0;i<20;i++)
			{
				ss[i][1]=NULL;
				fif=fof=FALSE;
				if(i<(int)player->cUpSeq)
				{
				    vup=(int)player->vUpSeq[i];
				    for(j=tr=0;j<vup;j++) tr+=nTrack[j];
				    for(j=tr;j<(int)(tr+nTrack[vup]);j++)
				    {
					    if(!player->trackEndFlg[j])
					    {
						    if( (player->trackEnv[j] & FIP_COMMAND_TRACK) !=0)
						    {
							    fif=TRUE;
						    }
						    else if((player->trackEnv[j] & FOP_COMMAND_TRACK) != 0)
						    {
							    fof=TRUE;
						    }
					    }
				    }
					if(fif && !fof)
					{
						ss[i][0]='+';
					}
					else if(!fif && fof)
					{
						ss[i][0]='-';
					}
					else if(fif && fof)
					{
						ss[i][0]='*';
					}
					else
					{
						ss[i][0]=' ';
					}
				}
				else
				{
					ss[i][0]=' ';
				}

			}
				switch(player->cUpSeq)
				{
				case 0:
					wsprintf(text,"                                                 ");
					pDC->TextOut(620,30,text);
					pDC->TextOut(620,50,text);
					break;
				case 1:
					wsprintf(text,"%s%d                                             ",
						ss[0],player->vUpSeq[0]);
					pDC->TextOut(620,30,text);
					wsprintf(text,"                                                 ");
					pDC->TextOut(620,50,text);
					break;
				case 2:
					wsprintf(text,"%s%d%s%d                                        ",
						ss[0],player->vUpSeq[0],ss[1],player->vUpSeq[1]);
					pDC->TextOut(620,30,text);
					wsprintf(text,"                                                 ");
					pDC->TextOut(620,50,text);
					break;
				case 3:
					wsprintf(text,"%s%d%s%d%s%d                                   ",
						ss[0],player->vUpSeq[0],ss[1],player->vUpSeq[1],
						ss[2],player->vUpSeq[2]);
					pDC->TextOut(620,30,text);
					wsprintf(text,"                                                 ");
					pDC->TextOut(620,50,text);
					break;
				case 4:
					wsprintf(text,"%s%d%s%d%s%d%s%d                              ",
						ss[0],player->vUpSeq[0],ss[1],player->vUpSeq[1],
						ss[2],player->vUpSeq[2],ss[3],player->vUpSeq[3]);
					pDC->TextOut(620,30,text);
					wsprintf(text,"                                                 ");
					pDC->TextOut(620,50,text);
					break;
				case 5:
					wsprintf(text,"%s%d%s%d%s%d%s%d%s%d                         ",
						ss[0],player->vUpSeq[0],ss[1],player->vUpSeq[1],ss[2],player->vUpSeq[2],
						ss[3],player->vUpSeq[3],ss[4],player->vUpSeq[4]);
					pDC->TextOut(620,30,text);
					wsprintf(text,"                                                 ");
					pDC->TextOut(620,50,text);
					break;
				case 6:
					wsprintf(text,"%s%d%s%d%s%d%s%d%s%d%s%d                    ",
						ss[0],player->vUpSeq[0],ss[1],player->vUpSeq[1],ss[2],player->vUpSeq[2],
						ss[3],player->vUpSeq[3],ss[4],player->vUpSeq[4],ss[5],player->vUpSeq[5]);
					pDC->TextOut(620,30,text);
					wsprintf(text,"                                                 ");
					pDC->TextOut(620,50,text);
					break;
				case 7:
					wsprintf(text,"%s%d%s%d%s%d%s%d%s%d%s%d%s%d               ",
						ss[0],player->vUpSeq[0],ss[1],player->vUpSeq[1],ss[2],player->vUpSeq[2],
						ss[3],player->vUpSeq[3],ss[4],player->vUpSeq[4],ss[5],player->vUpSeq[5],
						ss[6],player->vUpSeq[6]);
					pDC->TextOut(620,30,text);
					wsprintf(text,"                                                 ");
					pDC->TextOut(620,50,text);
					break;
				case 8:
					wsprintf(text,"%s%d%s%d%s%d%s%d%s%d%s%d%s%d%s%d          ",
						ss[0],player->vUpSeq[0],ss[1],player->vUpSeq[1],ss[2],player->vUpSeq[2],
						ss[3],player->vUpSeq[3],ss[4],player->vUpSeq[4],ss[5],player->vUpSeq[5],
						ss[6],player->vUpSeq[6],ss[7],player->vUpSeq[7]);
					pDC->TextOut(620,30,text);
					wsprintf(text,"                                                 ");
					pDC->TextOut(620,50,text);
					break;
				case 9:
					wsprintf(text,"%s%d%s%d%s%d%s%d%s%d%s%d%s%d%s%d%s%d     ",
						ss[0],player->vUpSeq[0],ss[1],player->vUpSeq[1],ss[2],player->vUpSeq[2],
						ss[3],player->vUpSeq[3],ss[4],player->vUpSeq[4],ss[5],player->vUpSeq[5],
						ss[6],player->vUpSeq[6],ss[7],player->vUpSeq[7],ss[8],player->vUpSeq[8]);
					pDC->TextOut(620,30,text);
					wsprintf(text,"                                                 ");
					pDC->TextOut(620,50,text);
					break;
				case 10:
					wsprintf(text,"%s%d%s%d%s%d%s%d%s%d%s%d%s%d%s%d%s%d%s%d",
						ss[0],player->vUpSeq[0],ss[1],player->vUpSeq[1],ss[2],player->vUpSeq[2],
						ss[3],player->vUpSeq[3],ss[4],player->vUpSeq[4],ss[5],player->vUpSeq[5],
						ss[6],player->vUpSeq[6],ss[7],player->vUpSeq[7],ss[8],player->vUpSeq[8],
						ss[9],player->vUpSeq[9]);
					pDC->TextOut(620,30,text);
					wsprintf(text,"                                                 ");
					pDC->TextOut(620,50,text);
					break;
				case 11:
					wsprintf(text,"%s%d%s%d%s%d%s%d%s%d%s%d%s%d%s%d%s%d%s%d",
						ss[0],player->vUpSeq[0],ss[1],player->vUpSeq[1],ss[2],player->vUpSeq[2],
						ss[3],player->vUpSeq[3],ss[4],player->vUpSeq[4],ss[5],player->vUpSeq[5],
						ss[6],player->vUpSeq[6],ss[7],player->vUpSeq[7],ss[8],player->vUpSeq[8],
						ss[9],player->vUpSeq[9]);
					pDC->TextOut(620,30,text);
					wsprintf(text,"%s%d                                             ",
						ss[10],player->vUpSeq[10]);
					pDC->TextOut(620,50,text);
					break;
				case 12:
					wsprintf(text,"%s%d%s%d%s%d%s%d%s%d%s%d%s%d%s%d%s%d%s%d",
						ss[0],player->vUpSeq[0],ss[1],player->vUpSeq[1],ss[2],player->vUpSeq[2],
						ss[3],player->vUpSeq[3],ss[4],player->vUpSeq[4],ss[5],player->vUpSeq[5],
						ss[6],player->vUpSeq[6],ss[7],player->vUpSeq[7],ss[8],player->vUpSeq[8],
						ss[9],player->vUpSeq[9]);
					pDC->TextOut(620,30,text);
					wsprintf(text,"%s%d%s%d                                        ",
						ss[10],player->vUpSeq[10],ss[11],player->vUpSeq[11]);
					pDC->TextOut(620,50,text);
					break;
				case 13:
					wsprintf(text,"%s%d%s%d%s%d%s%d%s%d%s%d%s%d%s%d%s%d%s%d",
						ss[0],player->vUpSeq[0],ss[1],player->vUpSeq[1],ss[2],player->vUpSeq[2],
						ss[3],player->vUpSeq[3],ss[4],player->vUpSeq[4],ss[5],player->vUpSeq[5],
						ss[6],player->vUpSeq[6],ss[7],player->vUpSeq[7],ss[8],player->vUpSeq[8],
						ss[9],player->vUpSeq[9]);
					pDC->TextOut(620,30,text);
					wsprintf(text,"%s%d%s%d%s%d                                  ",
						ss[10],player->vUpSeq[10],ss[11],player->vUpSeq[11],ss[12],player->vUpSeq[12]);
					pDC->TextOut(620,50,text);
					break;
				case 14:
					wsprintf(text,"%s%d%s%d%s%d%s%d%s%d%s%d%s%d%s%d%s%d%s%d",
						ss[0],player->vUpSeq[0],ss[1],player->vUpSeq[1],ss[2],player->vUpSeq[2],
						ss[3],player->vUpSeq[3],ss[4],player->vUpSeq[4],ss[5],player->vUpSeq[5],
						ss[6],player->vUpSeq[6],ss[7],player->vUpSeq[7],ss[8],player->vUpSeq[8],
						ss[9],player->vUpSeq[9]);
					pDC->TextOut(620,30,text);
					wsprintf(text,"%s%d%s%d%s%d%s%d                             ",
						ss[10],player->vUpSeq[10],ss[11],player->vUpSeq[11],ss[12],player->vUpSeq[12],
						ss[13],player->vUpSeq[13]);
					pDC->TextOut(620,50,text);
					break;
				case 15:
					wsprintf(text,"%s%d%s%d%s%d%s%d%s%d%s%d%s%d%s%d%s%d%s%d",
						ss[0],player->vUpSeq[0],ss[1],player->vUpSeq[1],ss[2],player->vUpSeq[2],
						ss[3],player->vUpSeq[3],ss[4],player->vUpSeq[4],ss[5],player->vUpSeq[5],
						ss[6],player->vUpSeq[6],ss[7],player->vUpSeq[7],ss[8],player->vUpSeq[8],
						ss[9],player->vUpSeq[9]);
					pDC->TextOut(620,30,text);
					wsprintf(text,"%s%d%s%d%s%d%s%d%s%d                        ",
						ss[10],player->vUpSeq[10],ss[11],player->vUpSeq[11],ss[12],player->vUpSeq[12],
						ss[13],player->vUpSeq[13],ss[14],player->vUpSeq[14]);
					pDC->TextOut(620,50,text);
					break;
				case 16:
					wsprintf(text,"%s%d%s%d%s%d%s%d%s%d%s%d%s%d%s%d%s%d%s%d",
						ss[0],player->vUpSeq[0],ss[1],player->vUpSeq[1],ss[2],player->vUpSeq[2],
						ss[3],player->vUpSeq[3],ss[4],player->vUpSeq[4],ss[5],player->vUpSeq[5],
						ss[6],player->vUpSeq[6],ss[7],player->vUpSeq[7],ss[8],player->vUpSeq[8],
						ss[9],player->vUpSeq[9]);
					pDC->TextOut(620,30,text);
					wsprintf(text,"%s%d%s%d%s%d%s%d%s%d%s%d                   ",
						ss[10],player->vUpSeq[10],ss[11],player->vUpSeq[11],ss[12],player->vUpSeq[12],
						ss[13],player->vUpSeq[13],ss[14],player->vUpSeq[14],ss[15],player->vUpSeq[15]);
					pDC->TextOut(620,50,text);
					break;
				case 17:
					wsprintf(text,"%s%d%s%d%s%d%s%d%s%d%s%d%s%d%s%d%s%d%s%d",
						ss[0],player->vUpSeq[0],ss[1],player->vUpSeq[1],ss[2],player->vUpSeq[2],
						ss[3],player->vUpSeq[3],ss[4],player->vUpSeq[4],ss[5],player->vUpSeq[5],
						ss[6],player->vUpSeq[6],ss[7],player->vUpSeq[7],ss[8],player->vUpSeq[8],
						ss[9],player->vUpSeq[9]);
					pDC->TextOut(620,30,text);
					wsprintf(text,"%s%d%s%d%s%d%s%d%s%d%s%d%s%d              ",
						ss[10],player->vUpSeq[10],ss[11],player->vUpSeq[11],ss[12],player->vUpSeq[12],
						ss[13],player->vUpSeq[13],ss[14],player->vUpSeq[14],ss[15],player->vUpSeq[15],
						ss[16],player->vUpSeq[16]);
					pDC->TextOut(620,50,text);
					break;
				case 18:
					wsprintf(text,"%s%d%s%d%s%d%s%d%s%d%s%d%s%d%s%d%s%d%s%d",
						ss[0],player->vUpSeq[0],ss[1],player->vUpSeq[1],ss[2],player->vUpSeq[2],
						ss[3],player->vUpSeq[3],ss[4],player->vUpSeq[4],ss[5],player->vUpSeq[5],
						ss[6],player->vUpSeq[6],ss[7],player->vUpSeq[7],ss[8],player->vUpSeq[8],
						ss[9],player->vUpSeq[9]);
					pDC->TextOut(620,30,text);
					wsprintf(text,"%s%d%s%d%s%d%s%d%s%d%s%d%s%d%s%d         ",
						ss[10],player->vUpSeq[10],ss[11],player->vUpSeq[11],ss[12],player->vUpSeq[12],
						ss[13],player->vUpSeq[13],ss[14],player->vUpSeq[14],ss[15],player->vUpSeq[15],
						ss[16],player->vUpSeq[16],ss[17],player->vUpSeq[17]);
					pDC->TextOut(620,50,text);
					break;
				case 19:
					wsprintf(text,"%s%d%s%d%s%d%s%d%s%d%s%d%s%d%s%d%s%d%s%d",
						ss[0],player->vUpSeq[0],ss[1],player->vUpSeq[1],ss[2],player->vUpSeq[2],
						ss[3],player->vUpSeq[3],ss[4],player->vUpSeq[4],ss[5],player->vUpSeq[5],
						ss[6],player->vUpSeq[6],ss[7],player->vUpSeq[7],ss[8],player->vUpSeq[8],
						ss[9],player->vUpSeq[9]);
					pDC->TextOut(620,30,text);
					wsprintf(text,"%s%d%s%d%s%d%s%d%s%d%s%d%s%d%s%d%s%d     ",
						ss[10],player->vUpSeq[10],ss[11],player->vUpSeq[11],ss[12],player->vUpSeq[12],
						ss[13],player->vUpSeq[13],ss[14],player->vUpSeq[14],ss[15],player->vUpSeq[15],
						ss[16],player->vUpSeq[16],ss[17],player->vUpSeq[17],ss[18],player->vUpSeq[18]);
					pDC->TextOut(620,50,text);
					break;
				case 20:
				default:
					wsprintf(text,"%s%d%s%d%s%d%s%d%s%d%s%d%s%d%s%d%s%d%s%d",
						ss[0],player->vUpSeq[0],ss[1],player->vUpSeq[1],ss[2],player->vUpSeq[2],
						ss[3],player->vUpSeq[3],ss[4],player->vUpSeq[4],ss[5],player->vUpSeq[5],
						ss[6],player->vUpSeq[6],ss[7],player->vUpSeq[7],ss[8],player->vUpSeq[8],
						ss[9],player->vUpSeq[9]);
					pDC->TextOut(620,30,text);
					wsprintf(text,"%s%d%s%d%s%d%s%d%s%d%s%d%s%d%s%d%s%d%s%d",
						ss[10],player->vUpSeq[10],ss[11],player->vUpSeq[11],ss[12],player->vUpSeq[12],
						ss[13],player->vUpSeq[13],ss[14],player->vUpSeq[14],ss[15],player->vUpSeq[15],
						ss[16],player->vUpSeq[16],ss[17],player->vUpSeq[17],ss[18],player->vUpSeq[18],
						ss[19],player->vUpSeq[19]);
					pDC->TextOut(620,50,text);
					break;
				}//end switch
			//}
		}
		if(pDC != NULL) view->ReleaseDC(pDC);
	}
	return(0);
}

BOOL CQueens3timer::maintainTime(int *cnt,CDC *pDC)
{
	static const char *ws[7] = {"日","月","火","水","木","金","土"};
	static const int deviceNumber = 1;	//←これは？
	//static const int chan = 15;
	static const int chan = 3;
	static const int gain = 1;
	static const UINT count = 600; // 1205 400;
	static const int timebase = 3;
	static const UINT sampleInterbal = 100;

	static short buffer[8192]; // 1205 4096

    SYSTEMTIME st;
	WORD y,m,d,w,h,n,s,t;
	char b[256];
	int status;
	BOOL ret;
	//CDC *pDCadj;

	GetLocalTime(&st);
	y = st.wYear;
	m = st.wMonth;
	d = st.wDay;
	w = st.wDayOfWeek;
	h = st.wHour;
	n = st.wMinute;
	s = st.wSecond;
	t = st.wMilliseconds;
	if(DisplayFlg && pDC != NULL)
	{
		wsprintf(b,"%d年%d月%d日%s曜日%d時%d分%d秒%d     ",y,m,d,ws[w],h,n,s,t);
		pDC->TextOut(0,10,b);
	}
	//時報および時間起動の曲選択
	ret = TimerSeq(pDC,cnt,d,h,n,s,t);
	//時刻あわせ
	//if((h == 6 || h == 11 || h == 18)
	if( h > 7
		&& h < 24
		&& n == 59)
	{
		if(s > 55 || s < 55)//1205
		{
			ret = FALSE;
		}
		else// if(s == 55)// && !DAQStartFlg)
		{
			//adjuster->ActivateFrame();
			status = 0;
			if(DisplayFlg && pDC != NULL)
			{
			    wsprintf(b,"Timer_Start:%d            ",status);
			    pDC->TextOut(300,10,b);
			}
			ReportLog(0x0,0x0);//
			ret = FALSE;
			if(TimeAdjustFlg) TimeAdjustFlg = FALSE;
		}
		//else if(TimeAdjustFlg) TimeAdjustFlg = FALSE;
	}
//	else if((h == 7 || h == 12 || h == 19)
	else if(h > 6
		&& h < 24
		&& n == 0)
	{
		if(s < 5 || s > 5) ret = FALSE;//1205
		else if(s == 5) //1205
		{
			//adjuster->ShowWindow(SW_HIDE);
			//AfxGetMainWnd()->ShowWindow(SW_SHOW); ←コメントアウトしてみた
			status = 0;
		}
	}
	return(ret);
}

BOOL CQueens3timer::TimerSeq(CDC *pDC,int *count,WORD day,WORD hour,WORD minute,WORD second,WORD millisec)
{
	int i,j,k,k2,tr,tq,mtr,etr,ftr,jtr,qtr;
	BOOL flg,sflg,dflg,pflg,qflg,fflg,ret;
	UINT jflg,tt;
	//WORD wh;
	WORD wp;///for siren

	//CDC *pDC;
//	char text[256];
	HANDLE hToken; 
	TOKEN_PRIVILEGES tkp; 

	//pDC = view->GetDC();

	tt = (UINT)(hour * 3600000L + minute * 60000L + second * 1000L);
	if(tt == ShutDownTime || tt == (ShutDownTime + 1000L) || tt==(ShutDownTime + 2000L))
	//if(tt >= ShutDownTime && tt <= (ShutDownTime + 2000L))
	{
		start = FALSE;
		player->start=FALSE;
		Sleep(1000);
		player->CloseMidiOut2();
    /*
		start = FALSE;
		if(TimerID != NULL)
		{
			TRACE("KILL TimerID=%d\n",TimerID);
			timeKillEvent(TimerID);
			TimerID=NULL;
		}
		deleteMem();
		player->start=FALSE;
		Sleep(1000);
		if(player->CheckLoopFlg!=NULL) delete []player->CheckLoopFlg;
		player->deleteMem();
		player->CloseMidiOut();
		delete player;
		for(i=0;i<fileNamesCount;i++)
		{
			delete [](fileNamesList[i]);
		}
		delete [] fileNamesList;
		delete this;
    */
	/* Get a token for this process. */ 
 
		if (OpenProcessToken(GetCurrentProcess(), 
			    TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken))
		{
		// error("OpenProcessToken"); 

	/* Get the LUID for the shutdown privilege. */ 
 
			LookupPrivilegeValue(NULL, SE_SHUTDOWN_NAME, 
				    &tkp.Privileges[0].Luid); 

			tkp.PrivilegeCount = 1;  /* one privilege to set    */ 
			tkp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED; 
 
	/* Get the shutdown privilege for this process. */ 
 
			AdjustTokenPrivileges(hToken, FALSE, &tkp, 0, 
				    (PTOKEN_PRIVILEGES)NULL, 0); 

	/* Cannot test the return value of AdjustTokenPrivileges. */ 
 
			if (GetLastError() == ERROR_SUCCESS)
			{
		// error("AdjustTokenPrivileges"); 
 
	/* Shut down the system and force all applications to close. */ 
 
				ExitWindowsEx(EWX_REBOOT | EWX_FORCE, 0); 
		// error("ExitWindowsEx"); 
			}
		}
	}

//////////konohen


	tt = (UINT)(hour * 3600000L + minute * 60000L + second * 1000L + millisec);
	for(i=0;i<(int)player->cUpSeq;i++)
	{
		if(seqInitial[player->vUpSeq[i]]!=0)
		{
			return(FALSE);
		}
	}
	(*count)++;
	for(i=0,jflg=0;i<(int)player->cUpSeq;i++)
	{
		if(seqSirenCommand[player->vUpSeq[i]] > 0)
		{
			jflg |= incTable[seqGroup[player->vUpSeq[i]]];
		}
	}
	for(i=tr=0,ret=TRUE;i<(int)nseq;tr+=nTrack[i],i++)
	{
		for(j=0,sflg=TRUE;j<(int)player->cUpSeq;j++)
		{
			if(i==(int)player->vUpSeq[j])
			{
				sflg=FALSE;//演奏中
//				TRACE("now playing i: %d\n",i);
				break;
			}
		}

		mtr=-1;
		if(seqTimerCommand[i])
		{
			if((jflg & incTable[seqGroup[i]])==0) //時報中でない
			{
				for(j=tr,dflg=fflg=TRUE,pflg=FALSE;j<tr+(int)nTrack[i];j++)
				{
					if((trackEnv[j] & ACTIVE_TRACK)!=0
					&& (trackEnv[j] & COMMAND_TRACK)==0)
					{
						mtr = j;
						if((trackEnv[j] & FADE_IN_OPTION)!=0
						&& player->seqFIPCommand[i])
						{
							pflg = TRUE;
						}
					}
					else if((trackEnv[j] & END_COMMAND_TRACK)!=0)
					{
						//
						etr = j;
					}
					else if((trackEnv[j] & FIP_COMMAND_TRACK)!=0)
					{
						ftr = j;
					}
					else if((trackEnv[j] & FOP_COMMAND_TRACK)!=0)
					{
						jtr = j;
					}
					else if((trackEnv[j] & TIMER_START_TRACK)!=0)
					{
						if(fflg
						&& seqPlayTime[j].type==TIMER_DEFINED
						&& ((seqPlayTime[j].rt.wDay!=(WORD)0
							&& seqPlayTime[j].rt.wDay<day)
							|| (  (seqPlayTime[j].rt.wDay==(WORD)0
								|| seqPlayTime[j].rt.wDay==day)
							  &&  ((seqPlayTime[j+1].time < 86400000L
								  && seqPlayTime[j].time <= tt
								  && seqPlayTime[j+1].time > tt)
								|| (seqPlayTime[j+1].time >= 86400000L
								  && (  (seqPlayTime[j].time <= tt
								      && tt < 86400000L)
								    ||  (seqPlayTime[j].time > tt
									  && seqPlayTime[j+1].time > 86400000L + tt))))))
						&& mtr>=0
						&& seqPlayTime[j+1].pair == seqPlayTime[j].pair
						&& (trackEnv[j+1] & TIMER_END_TRACK)!=0
						&& seqPlayTime[j+1].type == TIMER_DEFINED
						&& (seqPlayTime[j+1].rt.wDay>day
							|| (  (seqPlayTime[j+1].rt.wDay==(WORD)0
								|| seqPlayTime[j+1].rt.wDay==day)
							  &&  ((seqPlayTime[j+1].time < 86400000L
							      && seqPlayTime[j+1].time > tt
								  && seqPlayTime[j].time <= tt)
							    || (seqPlayTime[j+1].time >= 86400000L
							      &&  ( (seqPlayTime[j].time <= tt
								      && tt < 86400000L)
									||  (seqPlayTime[j].time > tt
									  && seqPlayTime[j+1].time > 86400000L + tt)))))))
						{
							dflg=FALSE;//対象時間内
							if(sflg)//演奏中でない
							{
								if(player->seqPlayTime[j].status==TIMER_WAIT)
								{
									player->seqPlayTime[j].status=TIMER_DONE;
									player->pTrack[mtr] = pTopTrack[mtr];
									player->trackTics[mtr] = 0;
									player->trackStatus[mtr]=0;
									player->trackStartTime[mtr]=player->currentTime;
									player->trackEndFlg[mtr]=FALSE;
									if((trackEnv[mtr] & END_OPTION)!=0)
									{
										player->pTrack[etr] = pTopTrack[etr];
										player->trackTics[etr] = 0;
										player->trackStartTime[etr]=player->currentTime;
										player->trackEndFlg[etr] = FALSE;
									}
									if((trackEnv[mtr] & LOOP)!=0)
									{
										player->loopFlg = TRUE;
										player->trackEnv[mtr] |= LOOP;
									}
									if(pflg)
									{
										player->pTrack[ftr] = pTopTrack[ftr];
										player->trackTics[ftr] = 0;
										player->trackStatus[ftr] = 0;
										player->trackStartTime[ftr]=player->currentTime;
										player->trackEndFlg[ftr] = FALSE;
									}
									player->ModifyCurrentSeq2(ADD_SEQ,i);
								}
							}
							break;
						}
						else
						{
							player->seqPlayTime[j].status=TIMER_WAIT;
						}
					}//end if
				}//end for j
				if(dflg && !sflg) //演奏中で対象時間外
				{
					if((player->trackEnv[mtr] & FADE_OUT_OPTION)!=0
					&& player->seqFOPCommand[i]
					&& player->trackEndFlg[jtr])
					{
						if((player->FOPMIDIChannel & seqChannel[i])!=0)
						{
						    player->pTrack[jtr] = pTopTrack[jtr];
						    player->trackTics[jtr] = 0;
						    player->trackStatus[jtr] = 0;
						    player->trackStartTime[jtr]=player->currentTime;
						    player->trackEndFlg[jtr] = FALSE;
						}
					}
					else if((player->trackEnv[mtr] & TO_END_OPTION)!=0)
					{
						player->trackEnv[mtr] &= (0xFFFFFFFF ^ LOOP);
					}
					else
					{
						player->trackEndFlg[mtr]=TRUE;
					    for(j=tq=0,flg=FALSE;j<(int)nseq;tq+=nTrack[j],j++)
					    {
						   for(k=0,dflg=FALSE;k<(int)(player->cUpSeq);k++)
						    {
							    if(j==player->vUpSeq[k])
							    {
								    dflg=TRUE;
								    break;
							    }
						    }
						    if(dflg)
						    {
						        for(k=tq;k<(int)(tq+nTrack[j]);k++)
						       {
							        if(trackEnv[k] & LOOP)
								    {
									    flg = TRUE;
									    break;
								    }
							    }
						    }
					    }
					    if(!flg)
					    {
						    player->loopFlg = FALSE;
					    }
				    }
				}
			}//end if
		}
		else if(seqSirenCommand[i] > 0)
		{
			////テーブルをひいて
			////今の曲を演奏する可能性があるかを判断する。
		//for(wh=hour;wh<(hour+2);wh++){
			for(wp=0,qflg=FALSE;wp<5;wp++)
			{
				if(minute > 50 
					&& pTimerEntry[hour][wp].seq==i	
					&& pTimerEntry[hour][wp].flg)
				{
					qflg=TRUE;
					qtr=pTimerEntry[hour][wp].track;
					break;
				}
				else if (minute < 10
					&& pTimerEntry[hour-1][wp].seq==i
					&& pTimerEntry[hour-1][wp].flg)
				{
					qflg=TRUE;
					qtr=pTimerEntry[hour-1][wp].track;
					break;
				}
			}
			if(qflg)
			{
		    for(j=tr,pflg=TRUE;j<tr+(int)nTrack[i];j++)
		    {
			    if((trackEnv[j] & ACTIVE_TRACK)!=0
			    && (trackEnv[j] & COMMAND_TRACK)==0)
				{
				    mtr = j;
				}
				else if((trackEnv[j] & END_COMMAND_TRACK)!=0)
				{
				    etr = j;
				}
				else if((trackEnv[j] & FOP_COMMAND_TRACK)!=0)
				{
					ftr = j;
				}
				else if((trackEnv[j] & TIMER_START_TRACK)!=0)
				{
//					TRACE("TIMER_START_TRACK\n");
					if( seqPlayTime[j].type==TIMER_DEFINED
					&& j == qtr									// for queen only
					&& ( ( seqPlayTime[j].rt.wDay==(WORD)0
						|| seqPlayTime[j].rt.wDay==day)
					  && (  (seqPlayTime[j+1].time < 86400000L
						  && seqPlayTime[j].time <= tt
					//	  とりあえずコメントアウト
					//	  && seqPlayTime[j].time + 2000 > tt
					//	  ここまで
						  && seqPlayTime[j+1].time > tt)
					//	||  (seqPlayTime[j+1].time >= 86400000L // over 24:00 end time
					//	  && (  (seqPlayTime[j].time <= tt		// both TSTA TEND exceed 24:00
					//		  && seqPlayTime[j].time + 2000 > tt
					//	      && tt < 86400000L)
					//	    ||  (seqPlayTime[j].time > tt		// other than ordered TSTA TEND
					//		  && seqPlayTime[j+1].time > 86400000L + tt)))
					     )
					   )
					&& mtr>=0
					&& seqPlayTime[j+1].pair == seqPlayTime[j].pair
					&& (trackEnv[j+1] & TIMER_END_TRACK)!=0
					  && seqPlayTime[j+1].type == TIMER_DEFINED
					  && (  (seqPlayTime[j+1].rt.wDay==(WORD)0
						  || seqPlayTime[j+1].rt.wDay==day)
					    && (  (seqPlayTime[j+1].time < 86400000L// checked already
						    && seqPlayTime[j].time <= tt
						    && seqPlayTime[j+1].time > tt)
					//	|| (seqPlayTime[j+1].time >= 86400000L	// over 24:00 end time
					//	  && (  (seqPlayTime[j].time <= tt
					//	      && tt < 86400000L)
					//	    ||  (seqPlayTime[j].time > tt
					//		  && seqPlayTime[j+1].time > 86400000L + tt)))
					       )
						 )
					   )
					{
						ret=FALSE;
						pflg = FALSE;
						if(sflg	
						&& player->seqPlayTime[j].status==TIMER_WAIT
						&& (jflg & incTable[seqGroup[i]])==0 )
						{
							//演奏中の他曲をFade outもしくは止める
						TRACE("start play %d\n", i);
							for(k=tq=0,fflg=TRUE;k<(int)nseq;tq+=nTrack[k],k++)
							{
								if(k==i || seqGroup[k] != seqGroup[i])
								{
									continue;
								}
								if(seqTimerCommand[k])
								{
									for(k2=tq;k2<tq+(int)nTrack[k];k2++)
									{
										if((trackEnv[k2] & END_OPTION)!=0)
										{
											fflg=FALSE;
										}
										else if(!fflg
										&& (trackEnv[k2] & END_COMMAND_TRACK)!=0)
										{
											jtr=k2;
										}
										player->seqPlayTime[k2].status=TIMER_WAIT;
									}
								}
								for(k2=0,flg=FALSE;k2<(int)player->cUpSeq;k2++)
								{
									if(k==(int)player->vUpSeq[k2])
									{
										flg=TRUE;//演奏中
										break;
									}
								}
								if(flg)//演奏中
								{
								    TRACE("TimerSeq:shut %d\n",k);
									if(player->seqFOPCommand[k])
									{
										for(k2=tq,dflg=TRUE;k2<tq+(int)nTrack[k];k2++)
										{
											if((trackEnv[k2] & ACTIVE_TRACK)!=0
											&& (trackEnv[k2] & COMMAND_TRACK)==0
											&& (trackEnv[k2] & FADE_OUT_OPTION)!=0)
											{
												dflg=FALSE;
											}
										}
										if(!dflg)
										{
								            TRACE("TimerSeq:shut FO\n");
											for(k2=tq,dflg=TRUE;k2<tq+(int)nTrack[k];k2++)
											{
													if((trackEnv[k2] & FOP_COMMAND_TRACK)!=0)
												{
									//Fade Out
													if(player->trackEndFlg[k2])
													{
													    player->FOPMIDIChannel |= player->seqChannel[k];
													    player->pTrack[k2] = player->pTopTrack[k2];
													    player->trackTics[k2] = 0;
													    player->trackStatus[k2] = 0;
													    player->trackStartTime[k2] = player->currentTime;
													    player->trackEndFlg[k2] = FALSE;
													}//end if
													dflg=FALSE;//Fade out起動した
												}
											}//end if
										}//end for
										if(dflg)//Fade out起動できなかった
										{
									//Stop
								            TRACE("TimerSeq:shut FO failed\n");
											for(k2=tq;k2<tq+(int)nTrack[k];k2++)
											{
												player->trackEndFlg[k2]=TRUE;
												player->trackTics[k2]=0;
												player->pTrack[k2]=pTopTrack[k2];
											}
											if(fflg)
											{
												player->trackTics[jtr] = 0;
												player->pTrack[jtr] = player->seqEndData[k];
												player->trackStartTime[jtr] 
													= (UINT)((float)(player->currentTime)
															-(float)(player->seqEndPoint[k]-player->seqStartPoint[k])
															/(float)(player->division[k])
															*(float)(player->Tempo[k]));
												player->trackEndFlg[jtr] = FALSE;
											}
											else
											{
												player->ModifyCurrentSeq2(DEL_SEQ,k);
											}//end else 
										}//end if dflg
									}//end if FOP
									else// 止める
									{
										//
										for(k2=tq;k2<tq+(int)nTrack[k];k2++)
										{
											player->trackEndFlg[k2]=TRUE;
											player->trackTics[k2]=0;
											player->pTrack[k2]=pTopTrack[k2];
										}
										if(fflg)
										{
											player->trackTics[jtr]=0;
											player->pTrack[jtr]=player->seqEndData[k];
											player->trackStartTime[jtr] 
												= (UINT)((float)(player->currentTime)
														-(float)(player->seqEndPoint[k]-player->seqStartPoint[k])
														/(float)(player->division[k])
														*(float)(player->Tempo[k]));
											player->trackEndFlg[jtr]=FALSE;
										}
										else
										{
											player->ModifyCurrentSeq2(DEL_SEQ,k);
										}
									}//end else
								}//end if flg
							}//end for k
							jflg |= incTable[seqGroup[i]];
							player->seqPlayTime[j].status=TIMER_DONE;
							player->trackTics[mtr]=0;
							player->trackStatus[mtr] = 0;
							player->pTrack[mtr]=pTopTrack[mtr];
							player->trackStartTime[mtr]=player->currentTime;
							player->trackEndFlg[mtr]=FALSE;
							if((trackEnv[mtr] & END_OPTION)!=0)
							{
								player->trackTics[etr]=0;
								player->pTrack[etr]=pTopTrack[etr];
								player->trackStartTime[etr]=player->currentTime;
							}
							player->ModifyCurrentSeq2(ADD_SEQ,i);
							*count=0;
						    break;
						}//end if TIMER_WAIT
					}//end if match timing
					else
					{
						player->seqPlayTime[j].status=TIMER_WAIT;
					}
				}//end if 曲トラック、開始時間トラック
			}//end for j
			if(pflg) //演奏中で対象時間外
			{
//			    TRACE("TimerSeq:shut playing %d\n",i);
			    if(sflg)
				{
					player->seqPlayTime[qtr].status=TIMER_WAIT;
				}
				else
				{
			        if((player->trackEnv[mtr] & FADE_OUT_OPTION)!=0
			        && player->seqFOPCommand[i]
			        && player->trackEndFlg[ftr])
			        {
				     //
				        player->pTrack[ftr] = pTopTrack[ftr];
				        player->trackTics[ftr] = 0;
				        player->trackStatus[ftr] = 0;
				        player->trackStartTime[ftr]=player->currentTime;
				        player->trackEndFlg[ftr] = FALSE;
			        }
			        else if((player->trackEnv[mtr] & TO_END_OPTION)!=0)
			        {
				        player->trackEnv[mtr] &= (0xFFFFFFFF ^ LOOP);
			        }
			        else
			        {
				        player->trackEndFlg[mtr]=TRUE;
					}
			    } //end else
			} //end if pflg

		}//end if qflg
		//}//end for hour

		}//end if siren
	}//end for i
	return(ret);
}

void CQueens3timer::InitSystemDat()
{
	char text[256];
	FILE *fp;
    errno_t err;

	SYSTEMTIME st;

	sprintf_s(text,256,"\\QUEEN3\\Init");
	SetCurrentDirectory(text);
	if((err=fopen_s(&fp,"ShutDown.DAT","r"))==0)
	{
	    fgets(text,255,fp);
	    player->GetTimeFromString(text,&st);
	    ShutDownTime = (UINT)(st.wHour * 3600000L + st.wMinute * 60000L + st.wSecond * 1000L);
	    fclose(fp);
	}
	else ShutDownTime = 86399000L; //23:59:59
}

void CQueens3timer::copyMemFromPlayer()
{
	UINT i,j,ntrk;

	nseq = player->nseq;
	seqSirenCommand = new int[nseq];
	seqTimerCommand = new BOOL[nseq];
	seqInitial = new int[nseq];
	seqChannel = new UINT[nseq];
	seqGroup = new int[nseq];
	nTrack = new UINT[nseq];
	for(i=ntrk=0;i<nseq;i++)
	{
		//
		seqSirenCommand[i] = player->seqSirenCommand[i];
		seqTimerCommand[i] = player->seqTimerCommand[i];
		seqInitial[i] = player->seqInitial[i];
//		seqSensor[i] = player->seqSensor[i];
		seqChannel[i] = player->seqChannel[i];
		seqGroup[i] = player->seqGroup[i];
		nTrack[i] = player->nTrack[i];
		ntrk+=nTrack[i];
	}
	trackEnv = new UINT[ntrk];
	seqPlayTime = new QUEEN_play_time[ntrk];
	pTopTrack = new LPSTR[ntrk];
	for(i=0;i<ntrk;i++)
	{
		trackEnv[i] = player->trackEnv[i];
		pTopTrack[i] = player->pTopTrack[i];
		seqPlayTime[i].type = player->seqPlayTime[i].type;
		seqPlayTime[i].pair = player->seqPlayTime[i].pair;
		seqPlayTime[i].status = player->seqPlayTime[i].status;
		seqPlayTime[i].rt.wDay = player->seqPlayTime[i].rt.wDay;
		seqPlayTime[i].rt.wHour = player->seqPlayTime[i].rt.wHour;
		seqPlayTime[i].rt.wMinute = player->seqPlayTime[i].rt.wMinute;
		seqPlayTime[i].rt.wSecond = player->seqPlayTime[i].rt.wSecond;
		seqPlayTime[i].rt.wMilliseconds = player->seqPlayTime[i].rt.wMilliseconds;
		seqPlayTime[i].time = player->seqPlayTime[i].time;
	}
	pTimerEntry = new TimerEntry[24][5];
	for(i=0;i<24;i++){
		for(j=0;j<5;j++){
			pTimerEntry[i][j].flg = player->pTimerEntry[i][j].flg;
			pTimerEntry[i][j].track = player->pTimerEntry[i][j].track;
			pTimerEntry[i][j].seq = player->pTimerEntry[i][j].seq;
		}
//		TRACE("%2d: %d %d %d %d %d\n"
//			, i
//			, pTimerEntry[i][0].track, pTimerEntry[i][1].track
//			, pTimerEntry[i][2].track, pTimerEntry[i][3].track
//			, pTimerEntry[i][4].track);
	}
}

void CQueens3timer::UnsetTimer(int part)
{
/*
	int i;

	for(i=0;0<nseq && i<nseq;i++)
	{
		if(part==seqGroup[i]){
			seqSirenCommand[i]=0;
		}
	}
	*/
}

void CQueens3timer::Recover(int part)
{
/*
	int i,j,tr,mtr,etr,ftr;
	SYSTEMTIME st;
	UINT tt;
	BOOL pflg;

	GetLocalTime(&st);
	tt = (UINT)(st.wHour * 3600000L + st.wMinute * 60000L + st.wSecond * 1000L
		+ st.wMilliseconds);

	for(i=tr=0;0<nseq && i<nseq;tr+=nTrack[i],i++)
	{
		mtr=-1;
		pflg=TRUE;
		if(part==seqGroup[i] && player->seqSirenCommand[i]>0)
		{
			for(j=tr;j<tr+nTrack[i];j++)
			{
			if((trackEnv[j] & ACTIVE_TRACK)!=0
			    && (trackEnv[j] & COMMAND_TRACK)==0)
				{
				    mtr = j;
				}
				else if((trackEnv[j] & END_COMMAND_TRACK)!=0)
				{
				    etr = j;
				}
				else if((trackEnv[j] & FOP_COMMAND_TRACK)!=0)
				{
					ftr = j;
				}
				else if((trackEnv[j] & TIMER_START_TRACK)!=0)
				{
					if( seqPlayTime[j].type==TIMER_DEFINED
					&& ( ( seqPlayTime[j].rt.wDay==(WORD)0
						|| seqPlayTime[j].rt.wDay==st.wDay)
					  && (  (seqPlayTime[j+1].time < 86400000L
						  && seqPlayTime[j].time + 1000 < tt
						  && seqPlayTime[j+1].time > tt)
						|| (seqPlayTime[j+1].time >= 86400000L
						  && (  seqPlayTime[j].time + 1000 < tt
						      && tt < 86400000L ))))
					&& mtr>=0
					&& seqPlayTime[j+1].pair == seqPlayTime[j].pair
					&& (trackEnv[j+1] & TIMER_END_TRACK)!=0
					&& seqPlayTime[j+1].type == TIMER_DEFINED
					&& (  (seqPlayTime[j+1].rt.wDay==(WORD)0
						|| seqPlayTime[j+1].rt.wDay==st.wDay)
					  && (  (seqPlayTime[j+1].time < 86400000L
						  && seqPlayTime[j].time <= tt
						  && seqPlayTime[j+1].time > tt)
						|| (seqPlayTime[j+1].time >= 86400000L
						  && (  (seqPlayTime[j].time <= tt
						      && tt < 86400000L)
						    ||  (seqPlayTime[j].time > tt
							  && seqPlayTime[j+1].time > 86400000L + tt))))))
					{
						pflg=FALSE;
						if(player->seqPlayTime[j].status==TIMER_WAIT)
						{
							//演奏しないが起動もかからない
							player->seqPlayTime[j].status=TIMER_DONE;
						    break;
						}//end if TIMER_WAIT
					}//end if match timing
				}//end if 曲トラック、開始時間トラック
			    if(pflg)
			    {
				    player->seqPlayTime[j].status=TIMER_WAIT;
			    }
			}//end for j
		}//end if
		seqSirenCommand[i] = player->seqSirenCommand[i];
	}//end for i
	*/
}

BOOL CQueens3timer::ModifyEntryTable(int area, int ahour, int select)
{
	//area 1:circle 2:pole 3:garden
	//select 0:stop 1:main 2:sub
	SYSTEMTIME st;
	int hour;
//	TRACE("Modify Entry Table\n");
//	TRACE("%d %d %d\n", area, hour, select);

//	UnsetTimer(area);

	GetLocalTime(&st);

//	直前の書き込みを防止する。
//	if((ahour - (int)(st.wHour)) < 2){
//		return FALSE;
//	}
//	ここまで

	hour=ahour-1;
	if(area==1){
		pTimerEntry[hour][0].flg = FALSE;
		pTimerEntry[hour][1].flg = FALSE;
		if(select==1){
			pTimerEntry[hour][0].flg = TRUE;
		}else if(select==2){
			pTimerEntry[hour][1].flg = TRUE;
		}
//		TRACE("%02d:00 %d %d %d %d %d\n"
//			, hour
//			, (int)pTimerEntry[hour][0].flg, (int)pTimerEntry[hour][1].flg
//			, (int)pTimerEntry[hour][2].flg, (int)pTimerEntry[hour][3].flg
//			, (int)pTimerEntry[hour][4].flg);
	}else if(area==2){
		pTimerEntry[hour][2].flg = FALSE;
		if(select==1){
			pTimerEntry[hour][2].flg = TRUE;
		}
//		TRACE("%02d:00 %d %d %d %d %d\n"
//			, hour
//			, (int)pTimerEntry[hour][0].flg, (int)pTimerEntry[hour][1].flg
//			, (int)pTimerEntry[hour][2].flg, (int)pTimerEntry[hour][3].flg
//			, (int)pTimerEntry[hour][4].flg);
	}else if(area==3){
		pTimerEntry[hour][3].flg = FALSE;
		pTimerEntry[hour][4].flg = FALSE;
		if(select==1){
			pTimerEntry[hour][3].flg = TRUE;
		}else if(select==2){
			pTimerEntry[hour][4].flg = TRUE;
		}

		TRACE("%02d:00 %d %d %d %d %d\n"
			, hour
			, (int)pTimerEntry[hour][0].flg, (int)pTimerEntry[hour][1].flg
			, (int)pTimerEntry[hour][2].flg, (int)pTimerEntry[hour][3].flg
			, (int)pTimerEntry[hour][4].flg);
	}

	return TRUE;
}

void CQueens3timer::deleteMem()
{
	if(seqSirenCommand != NULL) 
	{
		delete []seqSirenCommand;
		seqSirenCommand = NULL;
	}
	if(seqTimerCommand != NULL)
	{
		delete []seqTimerCommand;
		seqTimerCommand = NULL;
	}
	if(seqInitial != NULL)
	{
		delete []seqInitial;
		seqInitial = NULL;
	}
	if(seqChannel != NULL)
	{
		delete []seqChannel;
		seqChannel = NULL;
	}
	if(nTrack != NULL)
	{
		delete []nTrack;
		nTrack = NULL;
	}
	if(trackEnv != NULL)
	{
		delete []trackEnv;
		trackEnv = NULL;
	}
	if(pTopTrack != NULL)
	{
		delete []pTopTrack;
		pTopTrack = NULL;
	}
	if(seqPlayTime != NULL)
	{
		delete []seqPlayTime;
		seqPlayTime = NULL;
	}
	if(seqGroup != NULL)
	{
		delete []seqGroup;
		seqGroup = NULL;
	}
	if(pTimerEntry != NULL)
	{
		delete []pTimerEntry;
		pTimerEntry = NULL;
	}
}

void CQueens3timer::ReportLog(
    DWORD dwEventID,   // event identifier
    WORD wCategory)    // event category
{
    HANDLE h; 

    // Get a handle to the event log.
 
    h = RegisterEventSource(NULL,  // use local computer 
            (LPCSTR)L"Queens4");           // event source name 
    if (h == NULL) 
    {
        printf("Could not register the event source."); 
        return;
    }

    // Report the event.
 
    if (!ReportEvent(h,           // event log handle 
            EVENTLOG_INFORMATION_TYPE,  // event type 
            wCategory,            // event category  
            dwEventID,            // event identifier 
            NULL,                 // no user security identifier 
            0,             // number of substitution strings 
            0,                    // no data 
            NULL,                // pointer to strings 
            NULL))                // no data 
    {
        printf("Could not report the event."); 
    }
 
    DeregisterEventSource(h); 
    return;
}
