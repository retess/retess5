// queens3View.cpp : インプリメンテーション ファイル
//

#include "stdafx.h"
#include "queens3.h"
#include "queens3View.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Cqueens3View

IMPLEMENT_DYNCREATE(Cqueens3View, CFrameWnd)

Cqueens3View::Cqueens3View()
{
}

Cqueens3View::~Cqueens3View()
{
}


BEGIN_MESSAGE_MAP(Cqueens3View, CFrameWnd)
	//{{AFX_MSG_MAP(Cqueens3View)
	ON_WM_KEYDOWN()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Cqueens3View 描画

void Cqueens3View::OnDraw(CDC* pDC)
{
	// TODO: この位置に描画用のコードを追加してください
}

/////////////////////////////////////////////////////////////////////////////
// Cqueens3View 診断


/////////////////////////////////////////////////////////////////////////////
// Cqueens3View メッセージ ハンドラ

void Cqueens3View::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	TRACE("OnKeyDown:%x\n",nChar);
	CFrameWnd::OnKeyDown(nChar, nRepCnt, nFlags);
}
