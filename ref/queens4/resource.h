//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by queens3.rc
//
#define IDC_START                       3
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_QUEENS3_DIALOG              102
#define IDD_STATUSBARCTRL               103
#define IDR_MAINFRAME                   128
#define IDR_MENU1                       129
#define IDD_WAIT_DIALOG                 130
#define IDR_ACCELERATOR1                131
#define IDB_BITMAPPLAYING               141
#define IDB_BITMAPPRESET                147
#define IDB_BITMAPPREUNSET              148
#define IDM_MIDI_FINISHED               201
#define IDM_TIMER_FINISHED              202
#define IDC_TITLE                       1001
#define IDC_ALARM                       1002
#define IDC_WAIT2                       1003
#define IDC_DISPLAY                     1011
#define IDC_DEMO                        1014
#define IDC_RESTART                     1015
#define IDC_STATIC1                     1016
#define IDC_STATIC2                     1017
#define IDC_STATIC3                     1018
#define IDC_INITIALIZE                  1048
#define IDC_CIRCLE08_M                  1049
#define IDC_SAVE_TO_FILE                1050
#define IDC_CIRCLE08_S                  1051
#define IDC_CIRCLE08_N                  1052
#define IDC_POLE08_M                    1053
#define IDC_POLE08_N                    1054
#define IDC_GARDEN08_M                  1055
#define IDC_GARDEN08_S                  1056
#define IDC_GARDEN08_N                  1057
#define IDC_CIRCLE09_M                  1058
#define IDC_CIRCLE09_S                  1059
#define IDC_CIRCLE09_N                  1060
#define IDC_POLE09_M                    1061
#define IDC_POLE09_N                    1062
#define IDC_GARDEN09_M                  1063
#define IDC_GARDEN09_S                  1064
#define IDC_GARDEN09_N                  1065
#define IDC_CIRCLE10_M                  1066
#define IDC_CIRCLE10_S                  1067
#define IDC_CIRCLE10_N                  1068
#define IDC_POLE10_M                    1069
#define IDC_POLE10_N                    1070
#define IDC_GARDEN10_M                  1071
#define IDC_GARDEN10_S                  1072
#define IDC_GARDEN10_N                  1073
#define IDC_CIRCLE11_M                  1074
#define IDC_CIRCLE11_S                  1075
#define IDC_CIRCLE11_N                  1076
#define IDC_POLE11_M                    1077
#define IDC_POLE11_N                    1078
#define IDC_GARDEN11_M                  1079
#define IDC_GARDEN11_S                  1080
#define IDC_GARDEN11_N                  1081
#define IDC_CIRCLE12_M                  1082
#define IDC_CIRCLE12_S                  1083
#define IDC_CIRCLE12_N                  1084
#define IDC_POLE12_M                    1085
#define IDC_POLE12_N                    1086
#define IDC_GARDEN12_M                  1087
#define IDC_GARDEN12_S                  1088
#define IDC_GARDEN12_N                  1089
#define IDC_CIRCLE13_M                  1090
#define IDC_CIRCLE13_S                  1091
#define IDC_CIRCLE13_N                  1092
#define IDC_POLE13_M                    1093
#define IDC_POLE13_N                    1094
#define IDC_GARDEN13_M                  1095
#define IDC_GARDEN13_S                  1096
#define IDC_GARDEN13_N                  1097
#define IDC_CIRCLE14_M                  1098
#define IDC_CIRCLE14_S                  1099
#define IDC_CIRCLE14_N                  1100
#define IDC_POLE14_M                    1101
#define IDC_POLE14_N                    1102
#define IDC_GARDEN14_M                  1103
#define IDC_GARDEN14_S                  1104
#define IDC_GARDEN14_N                  1105
#define IDC_CIRCLE15_M                  1106
#define IDC_CIRCLE15_S                  1107
#define IDC_CIRCLE15_N                  1108
#define IDC_POLE15_M                    1109
#define IDC_POLE15_N                    1110
#define IDC_GARDEN15_M                  1111
#define IDC_GARDEN15_S                  1112
#define IDC_GARDEN15_N                  1113
#define IDC_CIRCLE16_M                  1114
#define IDC_CIRCLE16_S                  1115
#define IDC_CIRCLE16_N                  1116
#define IDC_POLE16_M                    1117
#define IDC_POLE16_N                    1118
#define IDC_GARDEN16_M                  1119
#define IDC_GARDEN16_S                  1120
#define IDC_GARDEN16_N                  1121
#define IDC_CIRCLE17_M                  1122
#define IDC_CIRCLE17_S                  1123
#define IDC_CIRCLE17_N                  1124
#define IDC_POLE17_M                    1125
#define IDC_POLE17_N                    1126
#define IDC_GARDEN17_M                  1127
#define IDC_GARDEN17_S                  1128
#define IDC_GARDEN17_N                  1129
#define IDC_CIRCLE18_M                  1130
#define IDC_CIRCLE18_S                  1131
#define IDC_CIRCLE18_N                  1132
#define IDC_POLE18_M                    1133
#define IDC_POLE18_N                    1134
#define IDC_GARDEN18_M                  1135
#define IDC_GARDEN18_S                  1136
#define IDC_GARDEN18_N                  1137
#define IDC_CIRCLE19_M                  1138
#define IDC_CIRCLE19_S                  1139
#define IDC_CIRCLE19_N                  1140
#define IDC_POLE19_M                    1141
#define IDC_POLE19_N                    1142
#define IDC_GARDEN19_M                  1143
#define IDC_GARDEN19_S                  1144
#define IDC_GARDEN19_N                  1145
#define IDC_CIRCLE20_M                  1146
#define IDC_CIRCLE20_S                  1147
#define IDC_CIRCLE20_N                  1148
#define IDC_POLE20_M                    1149
#define IDC_POLE20_N                    1150
#define IDC_GARDEN20_M                  1151
#define IDC_GARDEN20_S                  1152
#define IDC_GARDEN20_N                  1153
#define IDC_CIRCLE21_M                  1154
#define IDC_CIRCLE21_S                  1155
#define IDC_CIRCLE21_N                  1156
#define IDC_POLE21_M                    1157
#define IDC_POLE21_N                    1158
#define IDC_GARDEN21_M                  1159
#define IDC_GARDEN21_S                  1160
#define IDC_GARDEN21_N                  1161
#define IDC_CIRCLE22_M                  1162
#define IDC_CIRCLE22_S                  1163
#define IDC_CIRCLE22_N                  1164
#define IDC_POLE22_M                    1165
#define IDC_POLE22_N                    1166
#define IDC_GARDEN22_M                  1167
#define IDC_GARDEN22_S                  1168
#define IDC_GARDEN22_N                  1169
#define IDC_CIRCLE23_M                  1170
#define IDC_CIRCLE23_S                  1171
#define IDC_CIRCLE23_N                  1172
#define IDC_POLE23_M                    1173
#define IDC_POLE23_N                    1174
#define IDC_GARDEN23_M                  1175
#define IDC_GARDEN23_S                  1176
#define IDC_GARDEN23_N                  1177
#define IDC_WEEK_LIST                   1178
#define IDC_CIRCLE_M                    1181
#define IDC_CIRCLE_S                    1182
#define IDC_CIRCLE_N                    1183
#define IDC_POLE_M                      1184
#define IDC_POLE_N                      1185
#define IDC_GARDEN_M                    1186
#define IDC_GARDEN_S                    1187
#define IDC_GARDEN_N                    1188
#define IDS_CLOSED                      32771
#define ID_OCLOCK                       32776
#define IDR_ADJUST                      32774
#define ID_ADJUST_TIME                  32772
#define IDM_SHOW_ADJUSTER               32775

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        132
#define _APS_NEXT_COMMAND_VALUE         32773
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           104
#endif
#endif
