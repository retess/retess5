// queens3Dlg.cpp : インプリメンテーション ファイル
//

#include "stdafx.h"
#include "queens3.h"
#include "queens3Dlg.h"

#include "queens3timer.h"
#include "queens3PREDlgItem.h"
#include "WAITDlg.h"
#include <windows.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define ADJ_MILLISEC 5 // 時刻あわせ時の遅れミリ秒

//#ifndef DEMO_VERSION
//#include "NIDAQ.h"
//#define VERSION_STRING "Ver 2.0        "
//#else
#define VERSION_STRING "Ver 3.1"
//#endif

//extern HANDLE OpenThread(
//  DWORD dwDesiredAccess,  // access right
//  BOOL bInheritHandle,    // handle inheritance option
//  DWORD dwThreadId        // thread identifier
//);

CMap<CString,LPCSTR,int,int> * pChannelMap;/////

/////////////////////////////////////////////////////////////////////////////
// アプリケーションのバージョン情報で使われている CAboutDlg ダイアログ

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// ダイアログ データ
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard は仮想関数のオーバーライドを生成します
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV のサポート
	//}}AFX_VIRTUAL

// インプリメンテーション
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// メッセージ ハンドラがありません。
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CQueens3Dlg ダイアログ

CQueens3Dlg::CQueens3Dlg(CWnd* pParent /*=NULL*/)
	: CDialog(CQueens3Dlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CQueens3Dlg)
	m_strWeek = _T("");
	//}}AFX_DATA_INIT
	// メモ: LoadIcon は Win32 の DestroyIcon のサブシーケンスを要求しません。
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	DisplayFlg = FALSE;
	DebugFlg = FALSE;
	//view = (Cqueens3View *)NULL;
	player = (CQueens3midi *)NULL;
	sensors = (CQueens3timer *)NULL;
	start = FALSE;
	fileNamesCount = 0;
	fileNamesList = (LPSTR *)NULL;
	//statusbarctrl = (CStatusBarCtrl *)NULL;
}

void CQueens3Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CQueens3Dlg)
	DDX_LBString(pDX, IDC_WEEK_LIST, m_strWeek);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CQueens3Dlg, CDialog)
	//{{AFX_MSG_MAP(CQueens3Dlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_DISPLAY, OnDisplay)
	ON_LBN_SELCHANGE(IDC_WEEK_LIST, OnSelchangeWeekList)
	ON_BN_CLICKED(IDC_CIRCLE08_N, OnCircle08N)
	ON_BN_CLICKED(IDC_CIRCLE08_S, OnCircle08S)
	ON_BN_CLICKED(IDC_CIRCLE08_M, OnCircle08M)
	ON_BN_CLICKED(IDC_CIRCLE09_M, OnCircle09M)
	ON_BN_CLICKED(IDC_CIRCLE09_N, OnCircle09N)
	ON_BN_CLICKED(IDC_CIRCLE09_S, OnCircle09S)
	ON_BN_CLICKED(IDC_CIRCLE10_M, OnCircle10M)
	ON_BN_CLICKED(IDC_CIRCLE10_N, OnCircle10N)
	ON_BN_CLICKED(IDC_CIRCLE10_S, OnCircle10S)
	ON_BN_CLICKED(IDC_CIRCLE11_M, OnCircle11M)
	ON_BN_CLICKED(IDC_CIRCLE11_N, OnCircle11N)
	ON_BN_CLICKED(IDC_CIRCLE11_S, OnCircle11S)
	ON_BN_CLICKED(IDC_CIRCLE12_M, OnCircle12M)
	ON_BN_CLICKED(IDC_CIRCLE12_N, OnCircle12N)
	ON_BN_CLICKED(IDC_CIRCLE12_S, OnCircle12S)
	ON_BN_CLICKED(IDC_CIRCLE13_M, OnCircle13M)
	ON_BN_CLICKED(IDC_CIRCLE13_N, OnCircle13N)
	ON_BN_CLICKED(IDC_CIRCLE13_S, OnCircle13S)
	ON_BN_CLICKED(IDC_CIRCLE14_M, OnCircle14M)
	ON_BN_CLICKED(IDC_CIRCLE14_N, OnCircle14N)
	ON_BN_CLICKED(IDC_CIRCLE14_S, OnCircle14S)
	ON_BN_CLICKED(IDC_CIRCLE15_M, OnCircle15M)
	ON_BN_CLICKED(IDC_CIRCLE15_N, OnCircle15N)
	ON_BN_CLICKED(IDC_CIRCLE15_S, OnCircle15S)
	ON_BN_CLICKED(IDC_CIRCLE16_M, OnCircle16M)
	ON_BN_CLICKED(IDC_CIRCLE16_N, OnCircle16N)
	ON_BN_CLICKED(IDC_CIRCLE16_S, OnCircle16S)
	ON_BN_CLICKED(IDC_CIRCLE17_M, OnCircle17M)
	ON_BN_CLICKED(IDC_CIRCLE17_N, OnCircle17N)
	ON_BN_CLICKED(IDC_CIRCLE17_S, OnCircle17S)
	ON_BN_CLICKED(IDC_CIRCLE18_M, OnCircle18M)
	ON_BN_CLICKED(IDC_CIRCLE18_N, OnCircle18N)
	ON_BN_CLICKED(IDC_CIRCLE18_S, OnCircle18S)
	ON_BN_CLICKED(IDC_CIRCLE19_M, OnCircle19M)
	ON_BN_CLICKED(IDC_CIRCLE19_N, OnCircle19N)
	ON_BN_CLICKED(IDC_CIRCLE19_S, OnCircle19S)
	ON_BN_CLICKED(IDC_CIRCLE20_M, OnCircle20M)
	ON_BN_CLICKED(IDC_CIRCLE20_N, OnCircle20N)
	ON_BN_CLICKED(IDC_CIRCLE20_S, OnCircle20S)
	ON_BN_CLICKED(IDC_CIRCLE21_M, OnCircle21M)
	ON_BN_CLICKED(IDC_CIRCLE21_N, OnCircle21N)
	ON_BN_CLICKED(IDC_CIRCLE21_S, OnCircle21S)
	ON_BN_CLICKED(IDC_CIRCLE22_M, OnCircle22M)
	ON_BN_CLICKED(IDC_CIRCLE22_N, OnCircle22N)
	ON_BN_CLICKED(IDC_CIRCLE22_S, OnCircle22S)
	ON_BN_CLICKED(IDC_CIRCLE23_M, OnCircle23M)
	ON_BN_CLICKED(IDC_CIRCLE23_N, OnCircle23N)
	ON_BN_CLICKED(IDC_CIRCLE23_S, OnCircle23S)
	ON_BN_CLICKED(IDC_GARDEN08_M, OnGarden08M)
	ON_BN_CLICKED(IDC_GARDEN08_N, OnGarden08N)
	ON_BN_CLICKED(IDC_GARDEN08_S, OnGarden08S)
	ON_BN_CLICKED(IDC_GARDEN09_M, OnGarden09M)
	ON_BN_CLICKED(IDC_GARDEN09_N, OnGarden09N)
	ON_BN_CLICKED(IDC_GARDEN09_S, OnGarden09S)
	ON_BN_CLICKED(IDC_GARDEN10_M, OnGarden10M)
	ON_BN_CLICKED(IDC_GARDEN10_N, OnGarden10N)
	ON_BN_CLICKED(IDC_GARDEN10_S, OnGarden10S)
	ON_BN_CLICKED(IDC_GARDEN11_M, OnGarden11M)
	ON_BN_CLICKED(IDC_GARDEN11_N, OnGarden11N)
	ON_BN_CLICKED(IDC_GARDEN11_S, OnGarden11S)
	ON_BN_CLICKED(IDC_GARDEN12_M, OnGarden12M)
	ON_BN_CLICKED(IDC_GARDEN12_N, OnGarden12N)
	ON_BN_CLICKED(IDC_GARDEN12_S, OnGarden12S)
	ON_BN_CLICKED(IDC_GARDEN13_M, OnGarden13M)
	ON_BN_CLICKED(IDC_GARDEN13_N, OnGarden13N)
	ON_BN_CLICKED(IDC_GARDEN13_S, OnGarden13S)
	ON_BN_CLICKED(IDC_GARDEN14_M, OnGarden14M)
	ON_BN_CLICKED(IDC_GARDEN14_N, OnGarden14N)
	ON_BN_CLICKED(IDC_GARDEN14_S, OnGarden14S)
	ON_BN_CLICKED(IDC_GARDEN15_M, OnGarden15M)
	ON_BN_CLICKED(IDC_GARDEN15_N, OnGarden15N)
	ON_BN_CLICKED(IDC_GARDEN15_S, OnGarden15S)
	ON_BN_CLICKED(IDC_GARDEN16_M, OnGarden16M)
	ON_BN_CLICKED(IDC_GARDEN16_N, OnGarden16N)
	ON_BN_CLICKED(IDC_GARDEN16_S, OnGarden16S)
	ON_BN_CLICKED(IDC_GARDEN17_M, OnGarden17M)
	ON_BN_CLICKED(IDC_GARDEN17_N, OnGarden17N)
	ON_BN_CLICKED(IDC_GARDEN17_S, OnGarden17S)
	ON_BN_CLICKED(IDC_GARDEN18_M, OnGarden18M)
	ON_BN_CLICKED(IDC_GARDEN18_N, OnGarden18N)
	ON_BN_CLICKED(IDC_GARDEN18_S, OnGarden18S)
	ON_BN_CLICKED(IDC_GARDEN19_M, OnGarden19M)
	ON_BN_CLICKED(IDC_GARDEN19_N, OnGarden19N)
	ON_BN_CLICKED(IDC_GARDEN19_S, OnGarden19S)
	ON_BN_CLICKED(IDC_GARDEN20_M, OnGarden20M)
	ON_BN_CLICKED(IDC_GARDEN20_N, OnGarden20N)
	ON_BN_CLICKED(IDC_GARDEN20_S, OnGarden20S)
	ON_BN_CLICKED(IDC_GARDEN21_M, OnGarden21M)
	ON_BN_CLICKED(IDC_GARDEN21_N, OnGarden21N)
	ON_BN_CLICKED(IDC_GARDEN21_S, OnGarden21S)
	ON_BN_CLICKED(IDC_GARDEN22_M, OnGarden22M)
	ON_BN_CLICKED(IDC_GARDEN22_N, OnGarden22N)
	ON_BN_CLICKED(IDC_GARDEN22_S, OnGarden22S)
	ON_BN_CLICKED(IDC_GARDEN23_M, OnGarden23M)
	ON_BN_CLICKED(IDC_GARDEN23_N, OnGarden23N)
	ON_BN_CLICKED(IDC_GARDEN23_S, OnGarden23S)
	ON_BN_CLICKED(IDC_POLE08_M, OnPole08M)
	ON_BN_CLICKED(IDC_POLE08_N, OnPole08N)
	ON_BN_CLICKED(IDC_POLE09_M, OnPole09M)
	ON_BN_CLICKED(IDC_POLE09_N, OnPole09N)
	ON_BN_CLICKED(IDC_POLE10_M, OnPole10M)
	ON_BN_CLICKED(IDC_POLE10_N, OnPole10N)
	ON_BN_CLICKED(IDC_POLE11_M, OnPole11M)
	ON_BN_CLICKED(IDC_POLE11_N, OnPole11N)
	ON_BN_CLICKED(IDC_POLE12_M, OnPole12M)
	ON_BN_CLICKED(IDC_POLE12_N, OnPole12N)
	ON_BN_CLICKED(IDC_POLE13_M, OnPole13M)
	ON_BN_CLICKED(IDC_POLE13_N, OnPole13N)
	ON_BN_CLICKED(IDC_POLE14_M, OnPole14M)
	ON_BN_CLICKED(IDC_POLE14_N, OnPole14N)
	ON_BN_CLICKED(IDC_POLE15_M, OnPole15M)
	ON_BN_CLICKED(IDC_POLE15_N, OnPole15N)
	ON_BN_CLICKED(IDC_POLE16_M, OnPole16M)
	ON_BN_CLICKED(IDC_POLE16_N, OnPole16N)
	ON_BN_CLICKED(IDC_POLE17_M, OnPole17M)
	ON_BN_CLICKED(IDC_POLE17_N, OnPole17N)
	ON_BN_CLICKED(IDC_POLE18_M, OnPole18M)
	ON_BN_CLICKED(IDC_POLE18_N, OnPole18N)
	ON_BN_CLICKED(IDC_POLE19_M, OnPole19M)
	ON_BN_CLICKED(IDC_POLE19_N, OnPole19N)
	ON_BN_CLICKED(IDC_POLE20_M, OnPole20M)
	ON_BN_CLICKED(IDC_POLE20_N, OnPole20N)
	ON_BN_CLICKED(IDC_POLE21_M, OnPole21M)
	ON_BN_CLICKED(IDC_POLE21_N, OnPole21N)
	ON_BN_CLICKED(IDC_POLE22_M, OnPole22M)
	ON_BN_CLICKED(IDC_POLE22_N, OnPole22N)
	ON_BN_CLICKED(IDC_POLE23_M, OnPole23M)
	ON_BN_CLICKED(IDC_POLE23_N, OnPole23N)
	ON_BN_CLICKED(IDC_CIRCLE_M, OnCircleM)
	ON_BN_CLICKED(IDC_CIRCLE_N, OnCircleN)
	ON_BN_CLICKED(IDC_CIRCLE_S, OnCircleS)
	ON_BN_CLICKED(IDC_GARDEN_M, OnGardenM)
	ON_BN_CLICKED(IDC_GARDEN_N, OnGardenN)
	ON_BN_CLICKED(IDC_GARDEN_S, OnGardenS)
	ON_BN_CLICKED(IDC_POLE_M, OnPoleM)
	ON_BN_CLICKED(IDC_POLE_N, OnPoleN)
	ON_BN_CLICKED(IDC_SAVE_TO_FILE, OnSaveToFile)
	ON_BN_CLICKED(IDC_INITIALIZE, OnInitialize)
	ON_COMMAND(ID_OCLOCK, OnOclock)
	ON_COMMAND(IDR_ADJUST, OnAdjustOclock)
	ON_COMMAND(IDM_SHOW_ADJUSTER, OnShowAdjuster)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CQueens3Dlg メッセージ ハンドラ

BOOL CQueens3Dlg::OnInitDialog()
{
	int i;
	BOOL bRet;
	//CDC *pDCadj;
	CString str;

	CDialog::OnInitDialog();

	// "バージョン情報..." メニュー項目をシステム メニューへ追加します。

	// IDM_ABOUTBOX はコマンド メニューの範囲でなければなりません。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// このダイアログ用のアイコンを設定します。フレームワークはアプリケーションのメイン
	// ウィンドウがダイアログでない時は自動的に設定しません。
	SetIcon(m_hIcon, TRUE);			// 大きいアイコンを設定
	SetIcon(m_hIcon, FALSE);		// 小さいアイコンを設定

	//スレッドの設定
	//CWinThread *ct=AfxGetThread();
    //DWORD threadID=ct->m_nThreadID;//::GetCurrentThreadId();
    //HANDLE hObj = 0; 
    //HMODULE hdll = LoadLibrary( "kernel32.dll" ); 
    //if ( hdll ) 
    //{ 
    //    typedef HANDLE (WINAPI* OPENTHREADFCN)( DWORD, BOOL, DWORD ); 
    //    OPENTHREADFCN openThread = reinterpret_cast<OPENTHREADFCN>(GetProcAddress( hdll, "OpenThread" )); 
    //    if ( openThread ) 
	//	{
    //        hObj = openThread( THREAD_ALL_ACCESS, TRUE, threadID); 
	//		if(hObj==0)
	//		{
	//			LPVOID lpMsgBuf;
	//			FormatMessage(
	//				FORMAT_MESSAGE_ALLOCATE_BUFFER |
	//				FORMAT_MESSAGE_FROM_SYSTEM |
	//				FORMAT_MESSAGE_IGNORE_INSERTS,
	//				NULL,
	//				GetLastError(),
	//				MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // 既定の言語
	//				(LPTSTR) &lpMsgBuf,
	//				0,
	//				NULL
	//			);
	//			// lpMsgBuf 内のすべての挿入シーケンスを処理する。
	//			// ...
	//			// 文字列を表示する。
	//			MessageBox((LPCTSTR)lpMsgBuf, "Error", MB_OK | MB_ICONINFORMATION);
	//			// バッファを解放する。
	//			LocalFree(lpMsgBuf);
	//		}
	//		else
	//			TRACE("OnInitDialog:OpenThread handle=0x%x,0x%x\n",ct->m_hThread,hObj);
	//	}
    //    FreeLibrary( hdll ); 
    //} 
	adjuster=new CQueens3adj();
	RECT rect={600,0,1000,60};
	int ret=adjuster->Create(NULL,"時刻設定",WS_OVERLAPPEDWINDOW |WS_VISIBLE,rect,this);
	//pDCadj=adjuster->GetDC();
	//pDCadj->SetTextCharacterExtra(
	//str="（重要）";
	//adjuster->TextOutFont(0,0,str,10,RGB(255,0,0));
	//str="このウィンドウは時刻設定のために常にアクティブにする必要がありますので";
	//adjuster->TextOutFont(40,0,str,10,RGB(0,0,0));
	//str="他のウィンドウを使用したあとは、このウィンドウを選択しておいてください。";
	//adjuster->TextOutFont(40,10,str,10,RGB(0,0,0));
	//adjuster->ReleaseDC(pDCadj);
	//adjuster->ShowWindow(SW_HIDE);
	str="しばらくお待ちください・・・";
	adjuster->TextOutFont(20,10,str,20,RGB(0,0,255));
	//しばらくおまちください
	CWAITDlg * wDlg	= new CWAITDlg;
	wDlg->Create(IDD_WAIT_DIALOG, this);
	wDlg->GetDlgItem(IDC_TITLE)->ShowWindow(SW_SHOW);
	wDlg->CenterWindow(NULL);
	wDlg->ShowWindow(SW_SHOW);
	wDlg->RedrawWindow(NULL, NULL, RDW_UPDATENOW);

	//Create a statusbarctrl
	statusbarctrl = new CStatusBarCtrl;
	bRet = statusbarctrl->
		Create(WS_CHILD|WS_VISIBLE|CBRS_BOTTOM, CRect(0,0,100,30), this, IDD_STATUSBARCTRL);
	
	//if(bRet){
	//	TRACE("StatusBar O.K.\n");
	//}

	pChannelMap = NULL;
	PlayInitSeq();
	OnStartProg();

	m_iDayList = new int[31];
	m_iMonthList = new int[31];
	m_iYearList = new int[31];
	m_bFileExist = new BOOL[31];
	m_iCircle = new int[24];
	m_iPole = new int[24];
	m_iGarden = new int[24];
	SetDefault();
	m_bmpSet=new CBitmap;
	m_bmpSet->LoadBitmap(IDB_BITMAPPRESET);
	m_bmpUnset=new CBitmap;
	m_bmpUnset->LoadBitmap(IDB_BITMAPPREUNSET);


	CTime t = CTime::GetCurrentTime();
	pLB = (CListBox*) GetDlgItem(IDC_WEEK_LIST);

	for(i=0;i<31;i++){
		m_iDayList[i] = t.GetDay();
		m_iMonthList[i] = t.GetMonth();
		m_iYearList[i] = t.GetYear();
		CString s = t.Format( "%y年%m月%d日" );

		char cPath[MAX_PATH];
		char pFileName[14];
		sprintf_s(cPath,MAX_PATH,"\\QUEEN3\\Data");
		sprintf_s(pFileName,14,"%04d%02d%02d.dat", m_iYearList[i], m_iMonthList[i], m_iDayList[i]);
		SetCurrentDirectory(cPath);

		TRACE("%s\n", pFileName);

		CFile f;
		CFileException e;
		char buf[256];//, c;
		if(f.Open(pFileName, f.modeRead, &e) != 0 && f.Read(buf,144) == 144){
				m_bFileExist[i] = TRUE;
				s += "*";
//				TRACE("%s\n", s);
				f.Close();
		}else{
			m_bFileExist[i] = FALSE;
		}

		pLB->InsertString(-1, s);
		t += CTimeSpan( 1, 0, 0, 0 );
	}

	pLB->SetCurSel(0);
	OnSelchangeWeekList();
	ModifyTodayPreset(FALSE);

	if(statusbarctrl)
		statusbarctrl->SetText("設定できます", 0, 0);

	//しばらくおまちくださいをやめる
	wDlg->DestroyWindow();
	delete wDlg;
	view = new Cqueens3View();
	RECT rect2={10,10,800,600};
	ret=view->Create(NULL,"Monitor",WS_OVERLAPPEDWINDOW | WS_VISIBLE,rect2,this);
	//RECT rect={10,10,800,600};
	//int ret=view->Create(NULL,"Monitor",WS_OVERLAPPEDWINDOW | WS_VISIBLE,rect,this);
    //TRACE("ret=%d\n",ret);
	//view->ShowWindow(SW_SHOW);
	view->ShowWindow(SW_HIDE);
	adjuster->SetFocus();
	adjuster->SetActiveWindow();
	
	return TRUE;  // TRUE を返すとコントロールに設定したフォーカスは失われません。
}

void CQueens3Dlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// もしダイアログボックスに最小化ボタンを追加するならば、アイコンを描画する
// コードを以下に記述する必要があります。MFC アプリケーションは document/view
// モデルを使っているので、この処理はフレームワークにより自動的に処理されます。

void CQueens3Dlg::OnPaint() 
{
	int i,j,tr;
	char text[256];
	CDC *pDC;
    adjuster->ActivateFrame();
	if (IsIconic())
	{
		CPaintDC dc(this); // 描画用のデバイス コンテキスト

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// クライアントの矩形領域内の中央
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// アイコンを描画します。
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		if(DisplayFlg)
		{
		    pDC=view->GetDC();
	        if( sensors != NULL && sensors->start)
	        {
//		        if(player->DemoFlg) wsprintf(text,"Demo Mode      ");
//		        else 
					wsprintf(text,VERSION_STRING);
		        pDC->TextOut(850,10,text);
		        for(i=0;i<fileNamesCount;i++)
		        {
					wsprintf(text,"%d:%d:%s"
						,player->seqGroup[i],i
						,fileNamesList[i]);
			        pDC->TextOut(190*((int)(i/20)),90+20*(i%20),text);
		        }
		        for(i=tr=0;i<(int)player->nseq;i++)
		        {
			        if(DebugFlg)
			        {
				        for(j=0;j<(int)player->nTrack[i];j++,tr++)
				        {
					        if((player->trackEnv[tr] & 0x7E000000) == 0
					        && (player->trackEnv[tr] & 0x80000000) !=0)
					        {
						        wsprintf(text," t%d",player->trackTics[tr]);
						        pDC->TextOut(190*((int)(i/20))+150,90+20*(i%20),text);
					        }
				        }
				        if(player->seqEndPoint[i]!=0)
				        {
					        wsprintf(text," E%d",player->seqEndPoint[i]);
					        pDC->TextOut(190*((int)(i/20))+135,90+20*(i%20),text);
				        }
				        if(player->seqStartPoint[i]!=0)
				        {
					        wsprintf(text,"S%d",player->seqStartPoint[i]);
					        pDC->TextOut(190*((int)(i/20))+120,90+20*(i%20),text);
				        }
			        }
			        else
			        {
				        if(player->seqInitial[i]!=0)
				        {
					        wsprintf(text,"Ini%d",player->seqInitial[i]);
					        pDC->TextOut(190*((int)(i/20))+130,90+20*(i%20),text);
				        }
//				        if(player->seqTide[i]!=0)
//				        {
//					        wsprintf(text,"T%d",player->seqTide[i]);
//					        pDC->TextOut(190*((int)(i/30))+130,90+20*(i%30),text);
//				        }
				        if(player->seqTimerCommand[i])
				        {
					        wsprintf(text,"Tm");
					        pDC->TextOut(190*((int)(i/20))+130,90+20*(i%20),text);
				        }
				        if(player->seqSirenCommand[i] > 0)
				        {
					        wsprintf(text,"Sr");
					        pDC->TextOut(190*((int)(i/20))+130,90+20*(i%20),text);
				        }
						wsprintf(text,"%08x",player->seqChannel[i]);
						pDC->TextOut(190*((int)(i/20))+160,90+20*(i%20),text);
			        }
		        }
	        }
	        view->ReleaseDC(pDC);
		}
		CDialog::OnPaint();
	}
}

// システムは、ユーザーが最小化ウィンドウをドラッグしている間、
// カーソルを表示するためにここを呼び出します。
HCURSOR CQueens3Dlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

LRESULT CQueens3Dlg::OnAdjustTime(WPARAM wParam, LPARAM lParam)
{
	CDC* pDC;
	char text[256];
	int status;

    SYSTEMTIME st;
    //AfxMessageBox("OnAdjustTime",MB_OK);
	if(!sensors->TimeAdjustFlg)
	{
		sensors->TimeAdjustFlg = TRUE;
//#ifndef DEMO_VERSION
//		status = DAQ_Clear(1);
//#else
		status = 0;
//#endif
		GetLocalTime(&st);       // gets current time

		//if(st.wHour == (WORD)6) st.wHour = (WORD)7;
		//else if(st.wHour == (WORD)11) st.wHour = (WORD)12;
		//else if(st.wHour == (WORD)18) st.wHour = (WORD)19; //1205

		if(st.wMinute == (WORD)59)
		{
			switch(st.wHour)
			{
			case (WORD)6:
				st.wHour = (WORD)7;
				break;
			case (WORD)7:
				st.wHour = (WORD)8;
				break;
			case (WORD)8:
				st.wHour = (WORD)9;
				break;
			case (WORD)9:
				st.wHour = (WORD)10;
				break;
			case (WORD)10:
				st.wHour = (WORD)11;
				break;
			case (WORD)11:
				st.wHour = (WORD)12;
				break;
			case (WORD)12:
				st.wHour = (WORD)13;
				break;
			case (WORD)13:
				st.wHour = (WORD)14;
				break;
			case (WORD)14:
				st.wHour = (WORD)15;
				break;
			case (WORD)15:
				st.wHour = (WORD)16;
				break;
			case (WORD)16:
				st.wHour = (WORD)17;
				break;
			case (WORD)17:
				st.wHour = (WORD)18;
				break;
			case (WORD)18:
				st.wHour = (WORD)19;
				break;
			case (WORD)19:
				st.wHour = (WORD)20;
				break;
			case (WORD)20:
				st.wHour = (WORD)21;
				break;
			case (WORD)21:
				st.wHour = (WORD)22;
				break;
			case (WORD)22:
				st.wHour = (WORD)23;
				break;
			}
		}

		st.wMinute = (WORD)0;
		st.wSecond = (WORD)0;
		st.wMilliseconds = (WORD)ADJ_MILLISEC;
		SetLocalTime(&st);

		if(DisplayFlg)
		{
		    pDC=view->GetDC();
//		    wsprintf(text,"Time Adjusted;DAQ_Clear:%d",status);
		    wsprintf(text,"Time Adjusted 1");
		    pDC->TextOut(300,10,text);
		    view->ReleaseDC(pDC);
		}
		ReportLog(0x1,0x1);
	}
	return (LRESULT)m_hWnd;
}

void CQueens3Dlg::PlayInitSeq()
{
	char text[MAX_PATH];
	sprintf_s(text,MAX_PATH,"\\QUEEN3\\Init");
	SetCurrentDirectory(text);
	GetMIDIFiles();
//	TRACE("PlayInitSeq()   startSeq(INIT_SEQ)\n");
	startSeq(INIT_SEQ);
	EndSeq(FALSE);
}

void CQueens3Dlg::OnStartProg() 
{
	char text[MAX_PATH];

    if(!start)
	{
#ifdef DEBUG
	//AfxMessageBox("OnStartProg 1", MB_OK);
#endif
	    sprintf_s(text,MAX_PATH,"\\QUEEN3\\Data");
	    SetCurrentDirectory(text);
	    GetMIDIFiles();
#ifdef DEBUG
	//AfxMessageBox("OnStartProg 2", MB_OK);
#endif
//        GetDlgItem(IDC_START)->ShowWindow(SW_HIDE);
//        GetDlgItem(IDC_RESTART)->ShowWindow(SW_SHOW);
//        GetDlgItem(IDC_DEMO)->ShowWindow(SW_HIDE);
//        GetDlgItem(IDB_BITMAPPLAYING)->ShowWindow(SW_SHOW);
		start=TRUE;
//	TRACE("OnStartProg()   startSeq(NORMAL_SEQ)\n");
		startSeq(NORMAL_SEQ);
	}
}


void CQueens3Dlg::OnDemo() 
{
	char text[MAX_PATH];

    if(!start)
	{
	    sprintf_s(text,MAX_PATH,"\\QUEEN3\\Demo");
	    SetCurrentDirectory(text);
	    GetMIDIFiles();
        GetDlgItem(IDC_RESTART)->ShowWindow(SW_SHOW);
        GetDlgItem(IDC_DEMO)->ShowWindow(SW_HIDE);
        GetDlgItem(IDB_BITMAPPLAYING)->ShowWindow(SW_SHOW);
		start=TRUE;
//	TRACE("OnDemo()   startSeq(DEMO_SEQ)\n");
		startSeq(DEMO_SEQ);
	}
}
//////////////////////////////////////////////////

void CQueens3Dlg::startSeq(int DirFlg)
{
    //HANDLE hObj = 0; 
    //HMODULE hdll = LoadLibrary( "kernel32.dll" ); 
    //typedef HANDLE (WINAPI* OPENTHREADFCN)( DWORD, BOOL, DWORD ); 
    //OPENTHREADFCN openThread = reinterpret_cast<OPENTHREADFCN>(GetProcAddress( hdll, "OpenThread" )); 
	//LPVOID lpMsgBuf;
	//CDC* pDC;
	//char text[256];
	int status;
//	UINT ct;
	SYSTEMTIME st;
	//pDC =(CDC*)NULL;
	
//	if(view)
//		pDC = view->GetDC();

//	if(pDC){
//		wsprintf(text,"This is a test.");
//		pDC->TextOut(300,10,text);
//	}

#ifdef DEBUG
	//AfxMessageBox("StartSeq", MB_OK);
#endif
//	AfxMessageBox("in queens3.daq: TimingCount % SampleInterval", MB_OK);

	if( player == NULL)
	{
		player = new CQueens3midi;
		player->DisplayFlg=DisplayFlg;
		if(view) player->View = view;
//		player->FastBoot = FastBoot;
		player->DirFlg = DirFlg;
		player->DebugFlg = DebugFlg;
#ifdef DEBUG
	//AfxMessageBox("StartSeq 1", MB_OK);
#endif
		if(!pChannelMap)
		{
			pChannelMap = new CMap<CString,LPCSTR,int,int>(10);

        	pChannelMap->InitHashTable(37);
		    player->GetChannelInfo(pChannelMap);
	    }
		else player->pChannelMap = pChannelMap;
//		if(pDC){
//			wsprintf(text,"fileNamesCount %d",fileNamesCount);
//			pDC->TextOut(300,10,text);
//		}else{
//			TRACE("%d\n",fileNamesCount);
//		}
#ifdef DEBUG
	//MessageBox("getfileinfo","Message",
	//	MB_ICONEXCLAMATION | MB_OK);
#endif
		player->GetFileInfo(fileNamesCount,fileNamesList);///////

	}
	//
#ifdef DEBUG
	//MessageBox("player. Next Step?","Message",
	//	MB_ICONEXCLAMATION | MB_OK);
#endif
	if( sensors == NULL)
	{
		sensors = new CQueens3timer;
		sensors->adjuster = adjuster;
		sensors->player = player;
		if(view) sensors->view = view;
		else sensors->view=NULL;
		sensors->DisplayFlg = DisplayFlg;
		sensors->copyMemFromPlayer();
		
		if(DirFlg != INIT_SEQ)
		{
/*	        
			flg=((CButton *)GetDlgItem(IDC_CIRCLE))->GetCheck();
		    if(!flg)
		    {
		        sensors->UnsetTimer(1);
		        m_bCircle = FALSE;
		    }
	        flg=((CButton *)GetDlgItem(IDC_POLE))->GetCheck();
		    if(!flg)
		    {
		        sensors->UnsetTimer(2);
		        m_bPole = FALSE;
		    }
	        flg=((CButton *)GetDlgItem(IDC_GARDEN))->GetCheck();
		    if(!flg)
		    {
		        sensors->UnsetTimer(3);
		        m_bGarden = FALSE;
		    }
*/
		}
		
		GetLocalTime(&st);       // gets current time
		sensors->InitSystemDat();
		sensors->m_hWnd=this->m_hWnd;
 //       sensors->start = TRUE;
		sensors->TimerThread = AfxBeginThread(TimerThreadProc, sensors, 
//			THREAD_PRIORITY_NORMAL, 0, 0, NULL);
			THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED, NULL);
		//if ( hdll ) 
		//{ 
		//	if ( openThread ) 
		//	{
		//		hObj = openThread( THREAD_ALL_ACCESS, TRUE, sensors->TimerThread->m_nThreadID); 
		//		if(hObj==0)
		//		{
		//			FormatMessage(
		//				FORMAT_MESSAGE_ALLOCATE_BUFFER |
		//				FORMAT_MESSAGE_FROM_SYSTEM |
		//				FORMAT_MESSAGE_IGNORE_INSERTS,
		//				NULL,
		//				GetLastError(),
		//				MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // 既定の言語
		//				(LPTSTR) &lpMsgBuf,
		//				0,
		//				NULL
		//			);
		//			// lpMsgBuf 内のすべての挿入シーケンスを処理する。
		//			// ...
		//			// 文字列を表示する。
		//			MessageBox((LPCTSTR)lpMsgBuf, "Error", MB_OK | MB_ICONINFORMATION);
		//			// バッファを解放する。
		//			LocalFree(lpMsgBuf);
		//		}
		//		else
		//			TRACE("EndSeq:OpenThread handle=0x%x,0x%x\n",sensors->TimerThread->m_hThread,hObj);
		//	}
		//	FreeLibrary( hdll ); 
		//} 
	}
#ifdef DEBUG
	//MessageBox("sensor. Next Step?","Message",
	//	MB_ICONEXCLAMATION | MB_OK);
#endif
//	StartTime = sensors->StartTime;
//	GetLocalTime(&st); //現在時刻
//	while((ct=(UINT)(st.wHour * 3600000L + st.wMinute * 60000 + st.wSecond * 1000)) != StartTime)
//	{
//		if((StartTime > sensors->ShutDownTime
//		  && ( ct >= StartTime
//		    || ct < sensors->ShutDownTime))
//		|| (StartTime < sensors->ShutDownTime
//		  && ct >= StartTime
//		  && ct < sensors->ShutDownTime)) break;
//		else GetLocalTime(&st);
//	}

	status = 0;
	player->OpenMidiOut();
	player->playinit();
	player->m_hWnd=this->m_hWnd;
	player->MidiThread = AfxBeginThread(MidiThreadProc, player, 
#ifndef DEMO_VERSION
		THREAD_PRIORITY_ABOVE_NORMAL, 0, CREATE_SUSPENDED, NULL);
#else
		THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED, NULL);
//		THREAD_PRIORITY_NORMAL, 0, 0, NULL);
#endif
	//if ( hdll ) 
	//{ 
	//	if ( openThread ) 
	//	{
	//		hObj = openThread( THREAD_ALL_ACCESS, TRUE, player->MidiThread->m_nThreadID); 
	//		if(hObj==0)
	//		{
	//			FormatMessage(
	//				FORMAT_MESSAGE_ALLOCATE_BUFFER |
	//				FORMAT_MESSAGE_FROM_SYSTEM |
	//				FORMAT_MESSAGE_IGNORE_INSERTS,
	//				NULL,
	//				GetLastError(),
	//				MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // 既定の言語
	//				(LPTSTR) &lpMsgBuf,
	//				0,
	//				NULL
	//			);
	//			// lpMsgBuf 内のすべての挿入シーケンスを処理する。
	//			// ...
	//			// 文字列を表示する。
	//			MessageBox((LPCTSTR)lpMsgBuf, "Error", MB_OK | MB_ICONINFORMATION);
	//			// バッファを解放する。
	//			LocalFree(lpMsgBuf);
	//		}
	//		else
	//		{
	//			TRACE("EndSeq:OpenThread handle=0x%x,0x%x\n",player->MidiThread->m_hThread,hObj);
	//		}
	//	}
	//	FreeLibrary( hdll ); 
	//} 

	player->MidiThread->ResumeThread();
	sensors->start = TRUE;
	sensors->TimerThread->ResumeThread();
//	ReleaseDC(pDC);

}

void CQueens3Dlg::GetMIDIFiles()
{
//	SYSTEMTIME st;
	char text[MAX_PATH];
	WIN32_FIND_DATA FindFileData;
	HANDLE h;
	int i;

//	GetLocalTime(&st);
//	sprintf(text,"\\QUEEN3\\Data");
//	SetCurrentDirectory(text);
	if(fileNamesList) delete []fileNamesList;
	strcpy_s(text,"*.mid");
	h = FindFirstFile(text,&FindFileData);
	if(h != INVALID_HANDLE_VALUE)
	{
		fileNamesCount=1;
		while(FindNextFile(h,&FindFileData))
		{
			fileNamesCount++;
		}
	}
	else fileNamesCount=0;
	if(fileNamesCount>0)
	{
		fileNamesList = new LPSTR[fileNamesCount];
		h = FindFirstFile(text,&FindFileData);
		for(i=0;i<fileNamesCount;i++)
		{
			fileNamesList[i]=(LPSTR)new char[MAX_PATH];
			strcpy_s(fileNamesList[i],MAX_PATH,FindFileData.cFileName);
			FindNextFile(h,&FindFileData);
		}
	}
	else fileNamesList = (LPSTR *)NULL;
	return;
}


void CQueens3Dlg::OnOK() 
{

	deleteMem();

    EndSeq(TRUE);
	//if(m_bmpPlay) delete m_bmpPlay;
	//if(m_bmpStop) delete m_bmpStop;
	if(pChannelMap != NULL) delete pChannelMap; 
	//if(pre != NULL) delete pre;

	CDialog::OnOK();
}

void CQueens3Dlg::PlayEnd()
{
	//OnOK();
	PostMessage(IDOK);
}

void CQueens3Dlg::OnDisplay() 
{
	BOOL flg=((CButton *)GetDlgItem(IDC_DISPLAY))->GetCheck();
	if(flg)
	{
		view->ShowWindow(SW_SHOW);
		view->UpdateWindow();
		if(sensors) sensors->view=view;
		DisplayFlg = TRUE;
		if(sensors)
			sensors->DisplayFlg = DisplayFlg;
//		DebugFlg = TRUE;
	}
	else
	{
		view->ShowWindow(SW_HIDE);
		//view=NULL;
		if(sensors) sensors->view=NULL;
		if(player) player->View=NULL;
		DisplayFlg = FALSE;
		if(sensors)		
			sensors->DisplayFlg = DisplayFlg;
//		DebugFlg = FALSE;
	}
}

void CQueens3Dlg::OnRestart() 
{
	EndSeq(TRUE);
    GetDlgItem(IDC_START)->ShowWindow(SW_SHOW);
    GetDlgItem(IDC_RESTART)->ShowWindow(SW_HIDE);
    GetDlgItem(IDB_BITMAPPLAYING)->ShowWindow(SW_HIDE);
    GetDlgItem(IDC_DEMO)->ShowWindow(SW_SHOW);
	start=FALSE;
}

void CQueens3Dlg::EndSeq(BOOL force)
{
	//DWORD ts;
	//MSG msg;
	//HANDLE h;
	int i;
	//CWinThread * ct;
	CQueens3timer *wp;

	//ct = AfxGetThread();
	//ct->SetThreadPriority(THREAD_PRIORITY_HIGHEST);
	if(player != NULL)
	{
		if(player->MidiThread !=NULL)
		{
			if( force ) player->start = FALSE;
			//::GetExitCodeThread((h=player->MidiThread->m_hThread),&ts);
			while(player->MidiThread !=NULL)//ts==STILL_ACTIVE) && 
			{
				Sleep(100);
				//::GetExitCodeThread(h,&ts);
			}
			//::GetMessage(&msg,m_hWnd,1,1);
			//if(msg.message==IDM_MIDI_FINISHED)
			//{
				player->deleteMem();
				player->CloseMidiOut();
				delete player;
				player = (CQueens3midi *)NULL;
				if(sensors != NULL)
				{
					sensors->start = FALSE;
				}
			//}
			//else if(msg.message==IDM_TIMER_FINISHED)
			//{
			//	sensors->start = FALSE;
			//	wp = sensors;
			//	sensors = (CQueens3timer *)NULL;
			//	wp->deleteMem();
			//	wp->TimerThread = NULL;
			//	delete wp;
			//}
			//while(m_bMidiThread && player->MidiThread !=NULL) UpdateData(TRUE);
				//Sleep(100);
		}
	}
	if(sensors != NULL && force)
	{
		sensors->start = FALSE;
	}
	if(sensors != NULL)
	{
		if(sensors->TimerID!=NULL) timeKillEvent(sensors->TimerID);
		if(sensors->TimerThread != NULL)
		{
			//::GetExitCodeThread((h=sensors->TimerThread->m_hThread),&ts);
			//while(sensors->TimerThread !=NULL)//ts==STILL_ACTIVE) && 
			//{
				//Sleep(100);
			//	::GetExitCodeThread(h,&ts);
			//}
			//::GetMessage(&msg,m_hWnd,1,1);
			//if(msg.message==IDM_MIDI_FINISHED)
			//{
			//	player->deleteMem();
			//	player->CloseMidiOut();
			//	delete player;
			//	player = (CQueens3midi *)NULL;
			//}
			//else if(msg.message==IDM_TIMER_FINISHED)
			//{
				sensors->start = FALSE;
				wp = sensors;
				sensors = (CQueens3timer *)NULL;
				//wp->deleteMem();
				wp->TimerThread = NULL;
				//Sleep(2000);
				delete wp;
			//}
			//while(m_bTimerThread && sensors->TimerThread !=NULL)
			//	UpdateData(TRUE);
		}
	}

	for(i=0;i<fileNamesCount;i++)
	{
		delete [](fileNamesList[i]);
	}
	if(fileNamesList) delete [] fileNamesList;
	fileNamesList = (LPSTR *)NULL;
	fileNamesCount = 0;

	if(view)
	{
//		view->ShowWindow(SW_HIDE);
	    //view = (Cqueens3View *)NULL;
		DisplayFlg = FALSE;
	    DebugFlg = FALSE;
	    ((CButton *)GetDlgItem(IDC_DISPLAY))->SetCheck(0);
	}
}

//void CQUEENDlg::OnGotoPresetDlg() 
//{
//	pre=new CQUEENPRESET;
//	pre->Create(IDD_QUEEN_PRESET,this);
//	pre->ShowWindow(SW_SHOW);
//	pre->player = player;
//	pre->sensors = sensors;
//	pre->ModifyTodayPreset(TRUE);
//	pre->dlg = (CDialog *)this;
//	pre->DlgSet();
//	pre->SetFocus();
//}

void CQueens3Dlg::ClearYesterdayData()
{
	CTime t = CTime::GetCurrentTime();
	t -= CTimeSpan( 1, 0, 0, 0 );
	int d = t.GetDay();
	int m = t.GetMonth();
	int y = t.GetYear();

	char cPath[MAX_PATH];
	char* pFileName;
	pFileName = new char[14];
	sprintf_s(cPath,MAX_PATH,"\\QUEEN3\\Data");
	SetCurrentDirectory(cPath);
	sprintf_s(pFileName,14,"%04d%02d%02d.dat", y, m, d);
//	TRACE("%s\n", pFileName);

	CFile f;
//	CFileException e;
//	f.Open(pFileName, f.modeCreate | f.shareDenyWrite, &e);
//	f.Close();

	delete pFileName;
}

void CQueens3Dlg::ReadPresetData()
{
}

void CQueens3Dlg::deleteMem()
{
	delete statusbarctrl;
	delete []m_iDayList;
	delete []m_iMonthList;
	delete []m_iYearList;
	delete []m_bFileExist;
//	delete []m_bCircleM;
//	delete []m_bCircleN;
//	delete []m_bCircleS;
//	delete []m_bPoleM;
//	delete []m_bPoleN;
//	delete []m_bGardenM;
//	delete []m_bGardenN;
//	delete []m_bGardenS;
	delete []m_iCircle;
	delete []m_iPole;
	delete []m_iGarden;
	delete m_bmpSet;
    delete m_bmpUnset;
	delete view;
	delete adjuster;
}

void CQueens3Dlg::OnSelchangeWeekList() 
{
	// TODO: この位置にコントロール通知ハンドラ用のコードを追加してください
//	TRACE("%s\n",m_strWeek);
	int d;
	d = pLB->GetCurSel();
	char cPath[MAX_PATH];
	char* pFileName;
	pFileName = new char[14];
	sprintf_s(cPath,MAX_PATH,"\\QUEEN3\\Data");
	sprintf_s(pFileName,14,"%04d%02d%02d.dat", m_iYearList[d], m_iMonthList[d], m_iDayList[d]);
	SetCurrentDirectory(cPath);

	TRACE("%s\n", pFileName);

	CFile f;
	CFileException e;
	f.Open(pFileName, f.modeCreate | f.modeNoTruncate | f.modeRead | f.shareDenyWrite, &e);

	char buf[256];//, c;
	int i;
	if(f.Read(buf,144) == 144){
		TRACE("OnSelchangeWeekList:Data exist\n");
		TRACE("%s\n", buf);
		if(statusbarctrl)
			statusbarctrl->SetText("この日の設定データを読み込みました", 0, 0);

		int p = 0;
		for(i=0;i<24;i++,p+=2){
//			TRACE("%d ",m_iCircle[i]);
//			sscanf(buf + p, "%d:", m_iCircle[i]);
			m_iCircle[i] = (int)(buf[p] - '0');
//			TRACE("%d ",m_iCircle[i]);
		}
		for(i=0;i<24;i++,p+=2){
//			sscanf(buf + p, "%d:", m_iPole[i]);
			m_iPole[i] = (int)(buf[p] - '0');
		}
//		TRACE("pole\n");
		for(i=0;i<24;i++,p+=2){
//			sscanf(buf + p, "%d:", m_iGarden[i]);
			m_iGarden[i] = (int)(buf[p] - '0');
		}
//		TRACE("garden\n");
	}else{
		TRACE("No Data\n");
		if(statusbarctrl)
			statusbarctrl->SetText("この日のデータは設定されていません", 0, 0);
		SetDefault();
//		for(i=0;i<24;i++){
//			m_iCircle[i] = TIMERSUB;
//			m_iPole[i] = TIMERMAIN;
//			m_iGarden[i] = TIMERSUB;
//		}
	}
	f.Close();

	delete pFileName;
/*
	int d;
	d = pLB->GetCurSel();
	if(m_bFileExist){
		char cPath[MAX_PATH];
		char* pFileName;
		pFileName = new char[14];
		sprintf(cPath,"\\QUEEN3\\Data");
		sprintf(pFileName,"%04d%02d%02d.dat", m_iYearList[d], m_iMonthList[d], m_iDayList[d]);
		SetCurrentDirectory(cPath);
		
		TRACE("%s\n", pFileName);
		
		CFile f;
		CFileException e;
		f.Open(pFileName, f.modeRead | f.shareDenyWrite, &e);

		char buf[256];//, c;
		int i;
		f.Read(buf,144);
		TRACE("Data exist\n");
		TRACE("%s\n", buf);
		if(statusbarctrl)
			statusbarctrl->SetText("この日の設定データを読み込みました", 0, 0);

		int p = 0;
		for(i=0;i<24;i++,p+=2){
//			TRACE("%d ",m_iCircle[i]);
//			sscanf(buf + p, "%d:", m_iCircle[i]);
			m_iCircle[i] = (int)(buf[p] - '0');
//			TRACE("%d ",m_iCircle[i]);
		}
		for(i=0;i<24;i++,p+=2){
//			sscanf(buf + p, "%d:", m_iPole[i]);
			m_iPole[i] = (int)(buf[p] - '0');
		}
//		TRACE("pole\n");
		for(i=0;i<24;i++,p+=2){
//			sscanf(buf + p, "%d:", m_iGarden[i]);
			m_iGarden[i] = (int)(buf[p] - '0');
		}
//		TRACE("garden\n");
		f.Close();
		delete pFileName;
	}else{
		TRACE("No Data\n");
		if(statusbarctrl)
			statusbarctrl->SetText("この日のデータは設定されていません", 0, 0);
		SetDefault();
	}
 */
	ViewPreset();
//	for(i=8;i<24;i++){
//		CircleSet(m_iCircle[i], i);
//	}
//	for(i=8;i<24;i++){
//		PoleSet(m_iPole[i], i);
//	}
//	for(i=8;i<24;i++){
//		GardenSet(m_iGarden[i], i);
//	}

}

void CQueens3Dlg::SetDefault()
{
	int i;

	for(i=0;i<24;i++){
		m_iCircle[i] = TIMERSUB;
		m_iPole[i] = TIMERMAIN;
		m_iGarden[i] = TIMERSUB;
	}
	m_iCircle[12] = TIMERMAIN;
	m_iCircle[15] = TIMERMAIN;
	m_iCircle[19] = TIMERMAIN;
	m_iGarden[12] = TIMERMAIN;
	m_iGarden[15] = TIMERMAIN;
	m_iGarden[19] = TIMERMAIN;
}

void CQueens3Dlg::ViewPreset()
{
	int i;

	for(i=8;i<24;i++){
		CircleSet(m_iCircle[i], i);
	}
	for(i=8;i<24;i++){
		PoleSet(m_iPole[i], i);
	}
	for(i=8;i<24;i++){
		GardenSet(m_iGarden[i], i);
	}
}

void CQueens3Dlg::CircleSet(int set, int time)
{
	int d;
    SYSTEMTIME st;

	CButton *pmain = (CButton *)GetDlgItem(m_iCircleMID[time]);
	CButton *pstop = (CButton *)GetDlgItem(m_iCircleNID[time]);
	CButton *psub  = (CButton *)GetDlgItem(m_iCircleSID[time]);

	if(m_iCircle[time] == TIMERSTOP)
	{
		pstop->SetBitmap((HBITMAP)*m_bmpSet);
		pmain->SetBitmap((HBITMAP)*m_bmpUnset);
		psub->SetBitmap((HBITMAP)*m_bmpUnset);
	} else if(m_iCircle[time]==TIMERMAIN){
		pstop->SetBitmap((HBITMAP)*m_bmpUnset);
		pmain->SetBitmap((HBITMAP)*m_bmpSet);
		psub->SetBitmap((HBITMAP)*m_bmpUnset);
	} else if(m_iCircle[time]==TIMERSUB) {
		pstop->SetBitmap((HBITMAP)*m_bmpUnset);
		pmain->SetBitmap((HBITMAP)*m_bmpUnset);
		psub->SetBitmap((HBITMAP)*m_bmpSet);
	}

	GetLocalTime(&st);
	d = pLB->GetCurSel();
	if(m_iMonthList[d]==(int)st.wMonth && m_iDayList[d]==(int)st.wDay
		&& time <= (int)(st.wHour+1))
	{
		if(statusbarctrl)
			statusbarctrl->SetText("この時間は設定できません", 0, 0);
		return;
	}
	else
	{
		if(statusbarctrl)
			statusbarctrl->SetText("", 0, 0);
	}

	if(set==TIMERSTOP) {
		m_iCircle[time] = TIMERSTOP;
		pstop->SetBitmap((HBITMAP)*m_bmpSet);
		pmain->SetBitmap((HBITMAP)*m_bmpUnset);
		psub->SetBitmap((HBITMAP)*m_bmpUnset);
	} else if(set==TIMERMAIN){
		m_iCircle[time] = TIMERMAIN;
		pstop->SetBitmap((HBITMAP)*m_bmpUnset);
		pmain->SetBitmap((HBITMAP)*m_bmpSet);
		psub->SetBitmap((HBITMAP)*m_bmpUnset);
	} else if(set==TIMERSUB) {
		m_iCircle[time] = TIMERSUB;
		pstop->SetBitmap((HBITMAP)*m_bmpUnset);
		pmain->SetBitmap((HBITMAP)*m_bmpUnset);
		psub->SetBitmap((HBITMAP)*m_bmpSet);
	}
}

void CQueens3Dlg::PoleSet(int set, int time)
{
	int d;
    SYSTEMTIME st;

	CButton *pmain = (CButton *)GetDlgItem(m_iPoleMID[time]);
	CButton *pstop = (CButton *)GetDlgItem(m_iPoleNID[time]);

	if(m_iPole[time]==TIMERSTOP) {
		pstop->SetBitmap((HBITMAP)*m_bmpSet);
		pmain->SetBitmap((HBITMAP)*m_bmpUnset);
	} else if(m_iPole[time]==TIMERMAIN){
		pstop->SetBitmap((HBITMAP)*m_bmpUnset);
		pmain->SetBitmap((HBITMAP)*m_bmpSet);
	} 

	GetLocalTime(&st);
	d = pLB->GetCurSel();
	if(m_iMonthList[d]==(int)st.wMonth && m_iDayList[d]==(int)st.wDay
		&& time <= (int)(st.wHour+1))
	{
		if(statusbarctrl)
			statusbarctrl->SetText("この時間は設定できません", 0, 0);
		return;
	}
	else
	{
		if(statusbarctrl)
			statusbarctrl->SetText("", 0, 0);
	}

	if(set==TIMERSTOP) {
		m_iPole[time] = TIMERSTOP;
		pstop->SetBitmap((HBITMAP)*m_bmpSet);
		pmain->SetBitmap((HBITMAP)*m_bmpUnset);
	} else if(set==TIMERMAIN){
		m_iPole[time] = TIMERMAIN;
		pstop->SetBitmap((HBITMAP)*m_bmpUnset);
		pmain->SetBitmap((HBITMAP)*m_bmpSet);
	} 
}

void CQueens3Dlg::GardenSet(int set, int time)
{
	int d;
    SYSTEMTIME st;

	CButton *pmain = (CButton *)GetDlgItem(m_iGardenMID[time]);
	CButton *pstop = (CButton *)GetDlgItem(m_iGardenNID[time]);
	CButton *psub  = (CButton *)GetDlgItem(m_iGardenSID[time]);

	if(m_iGarden[time]==TIMERSTOP) {
		pstop->SetBitmap((HBITMAP)*m_bmpSet);
		pmain->SetBitmap((HBITMAP)*m_bmpUnset);
		psub->SetBitmap((HBITMAP)*m_bmpUnset);
	} else if(m_iGarden[time]==TIMERMAIN){
		pstop->SetBitmap((HBITMAP)*m_bmpUnset);
		pmain->SetBitmap((HBITMAP)*m_bmpSet);
		psub->SetBitmap((HBITMAP)*m_bmpUnset);
	} else if(m_iGarden[time]==TIMERSUB) {
		pstop->SetBitmap((HBITMAP)*m_bmpUnset);
		pmain->SetBitmap((HBITMAP)*m_bmpUnset);
		psub->SetBitmap((HBITMAP)*m_bmpSet);
	}

	GetLocalTime(&st);
	d = pLB->GetCurSel();
	if(m_iMonthList[d]==(int)st.wMonth && m_iDayList[d]==(int)st.wDay
		&& time <= (int)(st.wHour+1))
	{
		if(statusbarctrl)
			statusbarctrl->SetText("この時間は設定できません", 0, 0);
		return;
	}
	else
	{
		if(statusbarctrl)
			statusbarctrl->SetText("", 0, 0);
	}

	if(set==TIMERSTOP) {
		m_iGarden[time] = TIMERSTOP;
		pstop->SetBitmap((HBITMAP)*m_bmpSet);
		pmain->SetBitmap((HBITMAP)*m_bmpUnset);
		psub->SetBitmap((HBITMAP)*m_bmpUnset);
	} else if(set==TIMERMAIN){
		m_iGarden[time] = TIMERMAIN;
		pstop->SetBitmap((HBITMAP)*m_bmpUnset);
		pmain->SetBitmap((HBITMAP)*m_bmpSet);
		psub->SetBitmap((HBITMAP)*m_bmpUnset);
	} else if(set==TIMERSUB) {
		m_iGarden[time] = TIMERSUB;
		pstop->SetBitmap((HBITMAP)*m_bmpUnset);
		pmain->SetBitmap((HBITMAP)*m_bmpUnset);
		psub->SetBitmap((HBITMAP)*m_bmpSet);
	}
}

void CQueens3Dlg::OnCircle08M() 
{
	CircleSet(TIMERMAIN, 8);
}

void CQueens3Dlg::OnCircle08N() 
{
	CircleSet(TIMERSTOP, 8);
}

void CQueens3Dlg::OnCircle08S() 
{
	CircleSet(TIMERSUB, 8);
}

void CQueens3Dlg::OnCircle09M() 
{
	CircleSet(TIMERMAIN, 9);
}

void CQueens3Dlg::OnCircle09N() 
{
	CircleSet(TIMERSTOP, 9);
}

void CQueens3Dlg::OnCircle09S() 
{
	CircleSet(TIMERSUB, 9);
}

void CQueens3Dlg::OnCircle10M() 
{
	CircleSet(TIMERMAIN, 10);
}

void CQueens3Dlg::OnCircle10N() 
{
	CircleSet(TIMERSTOP, 10);
}

void CQueens3Dlg::OnCircle10S() 
{
	CircleSet(TIMERSUB, 10);
}

void CQueens3Dlg::OnCircle11M() 
{
	CircleSet(TIMERMAIN, 11);
}

void CQueens3Dlg::OnCircle11N() 
{
	CircleSet(TIMERSTOP, 11);
}

void CQueens3Dlg::OnCircle11S() 
{
	CircleSet(TIMERSUB, 11);
}

void CQueens3Dlg::OnCircle12M() 
{
	CircleSet(TIMERMAIN, 12);
}

void CQueens3Dlg::OnCircle12N() 
{
	CircleSet(TIMERSTOP, 12);
}

void CQueens3Dlg::OnCircle12S() 
{
	CircleSet(TIMERSUB, 12);
}

void CQueens3Dlg::OnCircle13M() 
{
	CircleSet(TIMERMAIN, 13);
}

void CQueens3Dlg::OnCircle13N() 
{
	CircleSet(TIMERSTOP, 13);
}

void CQueens3Dlg::OnCircle13S() 
{
	CircleSet(TIMERSUB, 13);
}

void CQueens3Dlg::OnCircle14M() 
{
	CircleSet(TIMERMAIN, 14);
}

void CQueens3Dlg::OnCircle14N() 
{
	CircleSet(TIMERSTOP, 14);
}

void CQueens3Dlg::OnCircle14S() 
{
	CircleSet(TIMERSUB, 14);
}

void CQueens3Dlg::OnCircle15M() 
{
	CircleSet(TIMERMAIN, 15);
}

void CQueens3Dlg::OnCircle15N() 
{
	CircleSet(TIMERSTOP, 15);
}

void CQueens3Dlg::OnCircle15S() 
{
	CircleSet(TIMERSUB, 15);
}

void CQueens3Dlg::OnCircle16M() 
{
	CircleSet(TIMERMAIN, 16);
}

void CQueens3Dlg::OnCircle16N() 
{
	CircleSet(TIMERSTOP, 16);
}

void CQueens3Dlg::OnCircle16S() 
{
	CircleSet(TIMERSUB, 16);
}

void CQueens3Dlg::OnCircle17M() 
{
	CircleSet(TIMERMAIN, 17);
}

void CQueens3Dlg::OnCircle17N() 
{
	CircleSet(TIMERSTOP, 17);
}

void CQueens3Dlg::OnCircle17S() 
{
	CircleSet(TIMERSUB, 17);
}

void CQueens3Dlg::OnCircle18M() 
{
	CircleSet(TIMERMAIN, 18);
}

void CQueens3Dlg::OnCircle18N() 
{
	CircleSet(TIMERSTOP, 18);
}

void CQueens3Dlg::OnCircle18S() 
{
	CircleSet(TIMERSUB, 18);
}

void CQueens3Dlg::OnCircle19M() 
{
	CircleSet(TIMERMAIN, 19);
}

void CQueens3Dlg::OnCircle19N() 
{
	CircleSet(TIMERSTOP, 19);
}

void CQueens3Dlg::OnCircle19S() 
{
	CircleSet(TIMERSUB, 19);
}

void CQueens3Dlg::OnCircle20M() 
{
	CircleSet(TIMERMAIN, 20);
}

void CQueens3Dlg::OnCircle20N() 
{
	CircleSet(TIMERSTOP, 20);
}

void CQueens3Dlg::OnCircle20S() 
{
	CircleSet(TIMERSUB, 20);
}

void CQueens3Dlg::OnCircle21M() 
{
	CircleSet(TIMERMAIN, 21);
}

void CQueens3Dlg::OnCircle21N() 
{
	CircleSet(TIMERSTOP, 21);
}

void CQueens3Dlg::OnCircle21S() 
{
	CircleSet(TIMERSUB, 21);
}

void CQueens3Dlg::OnCircle22M() 
{
	CircleSet(TIMERMAIN, 22);
}

void CQueens3Dlg::OnCircle22N() 
{
	CircleSet(TIMERSTOP, 22);
}

void CQueens3Dlg::OnCircle22S() 
{
	CircleSet(TIMERSUB, 22);
}

void CQueens3Dlg::OnCircle23M() 
{
	CircleSet(TIMERMAIN, 23);
}

void CQueens3Dlg::OnCircle23N() 
{
	CircleSet(TIMERSTOP, 23);
}

void CQueens3Dlg::OnCircle23S() 
{
	CircleSet(TIMERSUB, 23);
}

void CQueens3Dlg::OnGarden08M() 
{
	GardenSet(TIMERMAIN, 8);
}

void CQueens3Dlg::OnGarden08N() 
{
	GardenSet(TIMERSTOP, 8);
}

void CQueens3Dlg::OnGarden08S() 
{
	GardenSet(TIMERSUB, 8);
}

void CQueens3Dlg::OnGarden09M() 
{
	GardenSet(TIMERMAIN, 9);
}

void CQueens3Dlg::OnGarden09N() 
{
	GardenSet(TIMERSTOP, 9);
}

void CQueens3Dlg::OnGarden09S() 
{
	GardenSet(TIMERSUB, 9);
}

void CQueens3Dlg::OnGarden10M() 
{
	GardenSet(TIMERMAIN, 10);
}

void CQueens3Dlg::OnGarden10N() 
{
	GardenSet(TIMERSTOP, 10);
}

void CQueens3Dlg::OnGarden10S() 
{
	GardenSet(TIMERSUB, 10);
}

void CQueens3Dlg::OnGarden11M() 
{
	GardenSet(TIMERMAIN, 11);
}

void CQueens3Dlg::OnGarden11N() 
{
	GardenSet(TIMERSTOP, 11);
}

void CQueens3Dlg::OnGarden11S() 
{
	GardenSet(TIMERSUB, 11);
}

void CQueens3Dlg::OnGarden12M() 
{
	GardenSet(TIMERMAIN, 12);
}

void CQueens3Dlg::OnGarden12N() 
{
	GardenSet(TIMERSTOP, 12);
}

void CQueens3Dlg::OnGarden12S() 
{
	GardenSet(TIMERSUB, 12);
}

void CQueens3Dlg::OnGarden13M() 
{
	GardenSet(TIMERMAIN, 13);
}

void CQueens3Dlg::OnGarden13N() 
{
	GardenSet(TIMERSTOP, 13);
}

void CQueens3Dlg::OnGarden13S() 
{
	GardenSet(TIMERSUB, 13);
}

void CQueens3Dlg::OnGarden14M() 
{
	GardenSet(TIMERMAIN, 14);
}

void CQueens3Dlg::OnGarden14N() 
{
	GardenSet(TIMERSTOP, 14);
}

void CQueens3Dlg::OnGarden14S() 
{
	GardenSet(TIMERSUB, 14);
}

void CQueens3Dlg::OnGarden15M() 
{
	GardenSet(TIMERMAIN, 15);
}

void CQueens3Dlg::OnGarden15N() 
{
	GardenSet(TIMERSTOP, 15);
}

void CQueens3Dlg::OnGarden15S() 
{
	GardenSet(TIMERSUB, 15);
}

void CQueens3Dlg::OnGarden16M() 
{
	GardenSet(TIMERMAIN, 16);
}

void CQueens3Dlg::OnGarden16N() 
{
	GardenSet(TIMERSTOP, 16);
}

void CQueens3Dlg::OnGarden16S() 
{
	GardenSet(TIMERSUB, 16);
}

void CQueens3Dlg::OnGarden17M() 
{
	GardenSet(TIMERMAIN, 17);
}

void CQueens3Dlg::OnGarden17N() 
{
	GardenSet(TIMERSTOP, 17);
}

void CQueens3Dlg::OnGarden17S() 
{
	GardenSet(TIMERSUB, 17);
}

void CQueens3Dlg::OnGarden18M() 
{
	GardenSet(TIMERMAIN, 18);
}

void CQueens3Dlg::OnGarden18N() 
{
	GardenSet(TIMERSTOP, 18);
}

void CQueens3Dlg::OnGarden18S() 
{
	GardenSet(TIMERSUB, 18);
}

void CQueens3Dlg::OnGarden19M() 
{
	GardenSet(TIMERMAIN, 19);
}

void CQueens3Dlg::OnGarden19N() 
{
	GardenSet(TIMERSTOP, 19);
}

void CQueens3Dlg::OnGarden19S() 
{
	GardenSet(TIMERSUB, 19);
}

void CQueens3Dlg::OnGarden20M() 
{
	GardenSet(TIMERMAIN, 20);
}

void CQueens3Dlg::OnGarden20N() 
{
	GardenSet(TIMERSTOP, 20);
}

void CQueens3Dlg::OnGarden20S() 
{
	GardenSet(TIMERSUB, 20);
}

void CQueens3Dlg::OnGarden21M() 
{
	GardenSet(TIMERMAIN, 21);
}

void CQueens3Dlg::OnGarden21N() 
{
	GardenSet(TIMERSTOP, 21);
}

void CQueens3Dlg::OnGarden21S() 
{
	GardenSet(TIMERSUB, 21);
}

void CQueens3Dlg::OnGarden22M() 
{
	GardenSet(TIMERMAIN, 22);
}

void CQueens3Dlg::OnGarden22N() 
{
	GardenSet(TIMERSTOP, 22);
}

void CQueens3Dlg::OnGarden22S() 
{
	GardenSet(TIMERSUB, 22);
}

void CQueens3Dlg::OnGarden23M() 
{
	GardenSet(TIMERMAIN, 23);
}

void CQueens3Dlg::OnGarden23N() 
{
	GardenSet(TIMERSTOP, 23);
}

void CQueens3Dlg::OnGarden23S() 
{
	GardenSet(TIMERSUB, 23);
}

void CQueens3Dlg::OnPole08M() 
{
	PoleSet(TIMERMAIN, 8);
}

void CQueens3Dlg::OnPole08N() 
{
	PoleSet(TIMERSTOP, 8);
}

void CQueens3Dlg::OnPole09M() 
{
	PoleSet(TIMERMAIN, 9);
}

void CQueens3Dlg::OnPole09N() 
{
	PoleSet(TIMERSTOP, 9);
}

void CQueens3Dlg::OnPole10M() 
{
	PoleSet(TIMERMAIN, 10);
}

void CQueens3Dlg::OnPole10N() 
{
	PoleSet(TIMERSTOP, 10);
}

void CQueens3Dlg::OnPole11M() 
{
	PoleSet(TIMERMAIN, 11);
}

void CQueens3Dlg::OnPole11N() 
{
	PoleSet(TIMERSTOP, 11);
}

void CQueens3Dlg::OnPole12M() 
{
	PoleSet(TIMERMAIN, 12);
}

void CQueens3Dlg::OnPole12N() 
{
	PoleSet(TIMERSTOP, 12);
}

void CQueens3Dlg::OnPole13M() 
{
	PoleSet(TIMERMAIN, 13);
}

void CQueens3Dlg::OnPole13N() 
{
	PoleSet(TIMERSTOP, 13);
}

void CQueens3Dlg::OnPole14M() 
{
	PoleSet(TIMERMAIN, 14);
}

void CQueens3Dlg::OnPole14N() 
{
	PoleSet(TIMERSTOP, 14);
}

void CQueens3Dlg::OnPole15M() 
{
	PoleSet(TIMERMAIN, 15);
}

void CQueens3Dlg::OnPole15N() 
{
	PoleSet(TIMERSTOP, 15);
}

void CQueens3Dlg::OnPole16M() 
{
	PoleSet(TIMERMAIN, 16);
}

void CQueens3Dlg::OnPole16N() 
{
	PoleSet(TIMERSTOP, 16);
}

void CQueens3Dlg::OnPole17M() 
{
	PoleSet(TIMERMAIN, 17);
}

void CQueens3Dlg::OnPole17N() 
{
	PoleSet(TIMERSTOP, 17);
}

void CQueens3Dlg::OnPole18M() 
{
	PoleSet(TIMERMAIN, 18);
}

void CQueens3Dlg::OnPole18N() 
{
	PoleSet(TIMERSTOP, 18);
}

void CQueens3Dlg::OnPole19M() 
{
	PoleSet(TIMERMAIN, 19);
}

void CQueens3Dlg::OnPole19N() 
{
	PoleSet(TIMERSTOP, 19);
}

void CQueens3Dlg::OnPole20M() 
{
	PoleSet(TIMERMAIN, 20);
}

void CQueens3Dlg::OnPole20N() 
{
	PoleSet(TIMERSTOP, 20);
}

void CQueens3Dlg::OnPole21M() 
{
	PoleSet(TIMERMAIN, 21);
}

void CQueens3Dlg::OnPole21N() 
{
	PoleSet(TIMERSTOP, 21);
}

void CQueens3Dlg::OnPole22M() 
{
	PoleSet(TIMERMAIN, 22);
}

void CQueens3Dlg::OnPole22N() 
{
	PoleSet(TIMERSTOP, 22);
}

void CQueens3Dlg::OnPole23M() 
{
	PoleSet(TIMERMAIN, 23);
}

void CQueens3Dlg::OnPole23N() 
{
	PoleSet(TIMERSTOP, 23);
}

void CQueens3Dlg::OnCircleM() 
{
	int i;
	int d;
    SYSTEMTIME st;

	GetLocalTime(&st);
	d = pLB->GetCurSel();
	if(m_iMonthList[d]==(int)st.wMonth && m_iDayList[d]==(int)st.wDay)
	{
		if(statusbarctrl)
			statusbarctrl->SetText("この操作は本日分に対して使用できません", 0, 0);
		return;
	}
	else
	{
		if(statusbarctrl)
			statusbarctrl->SetText("", 0, 0);
	}

	for(i=8;i<24;i++){
		CircleSet(TIMERMAIN, i);
	}
}

void CQueens3Dlg::OnCircleN() 
{
	int i;
	int d;
    SYSTEMTIME st;

	GetLocalTime(&st);
	d = pLB->GetCurSel();
	if(m_iMonthList[d]==(int)st.wMonth && m_iDayList[d]==(int)st.wDay)
	{
		if(statusbarctrl)
			statusbarctrl->SetText("この操作は本日分に対して使用できません", 0, 0);
		return;
	}
	else
	{
		if(statusbarctrl)
			statusbarctrl->SetText("", 0, 0);
	}

	for(i=8;i<24;i++){
		CircleSet(TIMERSTOP, i);
	}
}

void CQueens3Dlg::OnCircleS() 
{
	int i;
	int d;
    SYSTEMTIME st;

	GetLocalTime(&st);
	d = pLB->GetCurSel();
	if(m_iMonthList[d]==(int)st.wMonth && m_iDayList[d]==(int)st.wDay)
	{
		if(statusbarctrl)
			statusbarctrl->SetText("この操作は本日分に対して使用できません", 0, 0);
		return;
	}
	else
	{
		if(statusbarctrl)
			statusbarctrl->SetText("", 0, 0);
	}

	for(i=8;i<24;i++){
		CircleSet(TIMERSUB, i);
	}
}

void CQueens3Dlg::OnGardenM() 
{
	int i;
	int d;
    SYSTEMTIME st;

	GetLocalTime(&st);
	d = pLB->GetCurSel();
	if(m_iMonthList[d]==(int)st.wMonth && m_iDayList[d]==(int)st.wDay)
	{
		if(statusbarctrl)
			statusbarctrl->SetText("この操作は本日分に対して使用できません", 0, 0);
		return;
	}
	else
	{
		if(statusbarctrl)
			statusbarctrl->SetText("", 0, 0);
	}

	for(i=8;i<24;i++){
		GardenSet(TIMERMAIN, i);
	}
}

void CQueens3Dlg::OnGardenN() 
{
	int i;
	int d;
    SYSTEMTIME st;

	GetLocalTime(&st);
	d = pLB->GetCurSel();
	if(m_iMonthList[d]==(int)st.wMonth && m_iDayList[d]==(int)st.wDay)
	{
		if(statusbarctrl)
			statusbarctrl->SetText("この操作は本日分に対して使用できません", 0, 0);
		return;
	}
	else
	{
		if(statusbarctrl)
			statusbarctrl->SetText("", 0, 0);
	}

	for(i=8;i<24;i++){
		GardenSet(TIMERSTOP, i);
	}
}

void CQueens3Dlg::OnGardenS() 
{
	int i;
	int d;
    SYSTEMTIME st;

	GetLocalTime(&st);
	d = pLB->GetCurSel();
	if(m_iMonthList[d]==(int)st.wMonth && m_iDayList[d]==(int)st.wDay)
	{
		if(statusbarctrl)
			statusbarctrl->SetText("この操作は本日分に対して使用できません", 0, 0);
		return;
	}
	else
	{
		if(statusbarctrl)
			statusbarctrl->SetText("", 0, 0);
	}

	for(i=8;i<24;i++){
		GardenSet(TIMERSUB, i);
	}
}

void CQueens3Dlg::OnPoleM() 
{
	int i;
	int d;
    SYSTEMTIME st;

	GetLocalTime(&st);
	d = pLB->GetCurSel();
	if(m_iMonthList[d]==(int)st.wMonth && m_iDayList[d]==(int)st.wDay)
	{
		if(statusbarctrl)
			statusbarctrl->SetText("この操作は本日分に対して使用できません", 0, 0);
		return;
	}
	else
	{
		if(statusbarctrl)
			statusbarctrl->SetText("", 0, 0);
	}

	for(i=8;i<24;i++){
		PoleSet(TIMERMAIN, i);
	}
}

void CQueens3Dlg::OnPoleN() 
{
	int i;
	int d;
    SYSTEMTIME st;

	GetLocalTime(&st);
	d = pLB->GetCurSel();
	if(m_iMonthList[d]==(int)st.wMonth && m_iDayList[d]==(int)st.wDay)
	{
		if(statusbarctrl)
			statusbarctrl->SetText("この操作は本日分に対して使用できません", 0, 0);
		return;
	}
	else
	{
		if(statusbarctrl)
			statusbarctrl->SetText("", 0, 0);
	}

	for(i=8;i<24;i++){
		PoleSet(TIMERSTOP, i);
	}
}

void CQueens3Dlg::OnSaveToFile() 
{
	int d;
	d = pLB->GetCurSel();

	if(d==0){
		ModifyTodayPreset(FALSE);
	}

	if(statusbarctrl)
		statusbarctrl->SetText("設定を記憶しています", 0, 0);

	char cPath[MAX_PATH];
	char* pFileName;
	pFileName = new char[14];
	sprintf_s(cPath,MAX_PATH,"\\QUEEN3\\Data");
	sprintf_s(pFileName,14,"%04d%02d%02d.dat", m_iYearList[d], m_iMonthList[d], m_iDayList[d]);
	SetCurrentDirectory(cPath);

	TRACE("%s\n", pFileName);

	CFile f;
	CFileException e;
	f.Open(pFileName, f.modeCreate | f.modeNoTruncate | f.modeWrite | f.shareDenyWrite, &e);

	char buf[256];
	int i, p = 0;
	for(i=0;i<24;i++){
		p += sprintf_s(buf + p, 256 - p, "%d:", m_iCircle[i]);
	}
//	p += sprintf(buf + p, "\n");
	for(i=0;i<24;i++){
		p += sprintf_s(buf + p, 256 - p, "%d:", m_iPole[i]);
	}
//	p += sprintf_s(buf + p, 256 - p, "\n");
	for(i=0;i<24;i++){
		p += sprintf_s(buf + p, 256 - p, "%d:", m_iGarden[i]);
	}
//	p += sprintf(buf + p, "\n");

	f.Write( buf, 144);
	f.Close();

	delete pFileName;

	if(!m_bFileExist[d]){
		CString s;
		pLB->GetText(d, s);
		s += "*";
		pLB->DeleteString(d);
		pLB->InsertString(d, s);
		m_bFileExist[d] = TRUE;
	}
	if(pLB->GetCurSel() < 0){
		pLB->SetCurSel(d);
	}
	if(statusbarctrl)
		statusbarctrl->SetText("この設定は記憶されました", 0, 0);

}

void CQueens3Dlg::OnInitialize() 
{
	SetDefault();
	ViewPreset();
}

void CQueens3Dlg::ModifyTodayPreset(BOOL load)
{
	int i;
	BOOL st = TRUE;

	for(i=8;i<24;i++){
		if(!sensors->ModifyEntryTable(1, i, m_iCircle[i])||
			!sensors->ModifyEntryTable(2, i, m_iPole[i])||
			!sensors->ModifyEntryTable(3, i, m_iGarden[i]))
		{
			if(!load){
//				TRACE("ModifyTodayPreset Erorr!\n");
				st = FALSE;
			}
		}
	}
	if(load && statusbarctrl)
		statusbarctrl->SetText("設定データを読み込みました", 0, 0);
}

//void Cqueens3Dlg::DlgSet(){
//	qdlg = (CQUEENDlg *)dlg;
//}

void CQueens3Dlg::OnOclock() 
{
	CDC* pDC;
	char text[256];
//	int status;

    SYSTEMTIME st;

	MessageBox("test oclock");
	if(!sensors->TimeAdjustFlg)
	{
		sensors->TimeAdjustFlg = TRUE;
		GetLocalTime(&st);       // gets current time

		if(st.wMinute == (WORD)59)
		{
			switch(st.wHour)
			{
			case (WORD)6:
				st.wHour = (WORD)7;
				break;
			case (WORD)7:
				st.wHour = (WORD)8;
				break;
			case (WORD)8:
				st.wHour = (WORD)9;
				break;
			case (WORD)9:
				st.wHour = (WORD)10;
				break;
			case (WORD)10:
				st.wHour = (WORD)11;
				break;
			case (WORD)11:
				st.wHour = (WORD)12;
				break;
			case (WORD)12:
				st.wHour = (WORD)13;
				break;
			case (WORD)13:
				st.wHour = (WORD)14;
				break;
			case (WORD)14:
				st.wHour = (WORD)15;
				break;
			case (WORD)15:
				st.wHour = (WORD)16;
				break;
			case (WORD)16:
				st.wHour = (WORD)17;
				break;
			case (WORD)17:
				st.wHour = (WORD)18;
				break;
			case (WORD)18:
				st.wHour = (WORD)19;
				break;
			case (WORD)19:
				st.wHour = (WORD)20;
				break;
			case (WORD)20:
				st.wHour = (WORD)21;
				break;
			case (WORD)21:
				st.wHour = (WORD)22;
				break;
			case (WORD)22:
				st.wHour = (WORD)23;
				break;
			}
		}

		st.wMinute = (WORD)0;
		st.wSecond = (WORD)0;
		st.wMilliseconds = (WORD)ADJ_MILLISEC;
		SetLocalTime(&st);

		if(DisplayFlg)
		{
		    pDC=view->GetDC();
//		    wsprintf(text,"Time Adjusted;DAQ_Clear:%d",status);
		    wsprintf(text,"Time Adjusted 2");
		    pDC->TextOut(300,10,text);
		    view->ReleaseDC(pDC);
		}
		ReportLog(0x2,0x1);
	}
}

void CQueens3Dlg::OnAdjustOclock() 
{
	AdjustOclock();
}

void CQueens3Dlg::OnShowAdjuster()
{
	adjuster->ShowWindow(SW_SHOW);
}

void CQueens3Dlg::AdjustOclock()
{
	CDC* pDC;
	char text[256];
	int status;

    SYSTEMTIME st;
    //AfxMessageBox("AdjustOclock",MB_OK);
	if(!sensors->TimeAdjustFlg)
	{
		sensors->TimeAdjustFlg = TRUE;
		status = 0;
		GetLocalTime(&st);       // gets current time

		//if(st.wHour == (WORD)6) st.wHour = (WORD)7;
		//else if(st.wHour == (WORD)11) st.wHour = (WORD)12;
		//else if(st.wHour == (WORD)18) st.wHour = (WORD)19; //1205

		if(st.wMinute > (WORD)29)
		{
			switch(st.wHour)
			{
			case (WORD)6:
				st.wHour = (WORD)7;
				break;
			case (WORD)7:
				st.wHour = (WORD)8;
				break;
			case (WORD)8:
				st.wHour = (WORD)9;
				break;
			case (WORD)9:
				st.wHour = (WORD)10;
				break;
			case (WORD)10:
				st.wHour = (WORD)11;
				break;
			case (WORD)11:
				st.wHour = (WORD)12;
				break;
			case (WORD)12:
				st.wHour = (WORD)13;
				break;
			case (WORD)13:
				st.wHour = (WORD)14;
				break;
			case (WORD)14:
				st.wHour = (WORD)15;
				break;
			case (WORD)15:
				st.wHour = (WORD)16;
				break;
			case (WORD)16:
				st.wHour = (WORD)17;
				break;
			case (WORD)17:
				st.wHour = (WORD)18;
				break;
			case (WORD)18:
				st.wHour = (WORD)19;
				break;
			case (WORD)19:
				st.wHour = (WORD)20;
				break;
			case (WORD)20:
				st.wHour = (WORD)21;
				break;
			case (WORD)21:
				st.wHour = (WORD)22;
				break;
			case (WORD)22:
				st.wHour = (WORD)23;
				break;
			}
		}
		//else
        st.wMinute = (WORD)0;
		st.wSecond = (WORD)0;
		st.wMilliseconds = (WORD)ADJ_MILLISEC;
		SetLocalTime(&st);

		if(DisplayFlg)
		{
		    pDC=view->GetDC();
//		    wsprintf(text,"Time Adjusted;DAQ_Clear:%d",status);
		    wsprintf(text,"Time Adjusted 3");
		    pDC->TextOut(300,10,text);
		    view->ReleaseDC(pDC);
		}
		ReportLog(0x3,0x1);
	}
}

void CQueens3Dlg::ExitApp()
{
	deleteMem();
	if(pChannelMap != NULL) delete pChannelMap; 
	delete this;
	exit(0);
}

void CQueens3Dlg::ReportLog(
    DWORD dwEventID,   // event identifier
    WORD wCategory)    // event category
{
    HANDLE h; 

    // Get a handle to the event log.
 
    h = RegisterEventSource(NULL,  // use local computer 
            (LPCSTR)L"Queens4");           // event source name 
    if (h == NULL) 
    {
        printf("Could not register the event source."); 
        return;
    }

    // Report the event.
 
    if (!ReportEvent(h,           // event log handle 
            EVENTLOG_INFORMATION_TYPE,  // event type 
            wCategory,            // event category  
            dwEventID,            // event identifier 
            NULL,                 // no user security identifier 
            0,             // number of substitution strings 
            0,                    // no data 
            NULL,                // pointer to strings 
            NULL))                // no data 
    {
        printf("Could not report the event."); 
    }
 
    DeregisterEventSource(h); 
    return;
}
