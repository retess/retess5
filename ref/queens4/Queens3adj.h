#if !defined(AFX_QUEENS3ADJ_H__19637600_9EEB_4C86_A7DC_069959DCC70F__INCLUDED_)
#define AFX_QUEENS3ADJ_H__19637600_9EEB_4C86_A7DC_069959DCC70F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Queens3adj.h : ヘッダー ファイル
//

/////////////////////////////////////////////////////////////////////////////
// CQueens3adj フレーム

class CQueens3adj : public CFrameWnd
{
	DECLARE_DYNCREATE(CQueens3adj)
protected:

// アトリビュート
public:
	bool m_bKey1Up;

// オペレーション
public:
	void TextOutFont(int x,int y,CString& str,int point,COLORREF color);
	CQueens3adj();           // 動的生成に使用されるプロテクト コンストラクタ。
	virtual ~CQueens3adj();

// オーバーライド
	// ClassWizard は仮想関数のオーバーライドを生成します。
	//{{AFX_VIRTUAL(CQueens3adj)
	//}}AFX_VIRTUAL

// インプリメンテーション
protected:

	// 生成されたメッセージ マップ関数
	//{{AFX_MSG(CQueens3adj)
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnPaint();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ は前行の直前に追加の宣言を挿入します。

#endif // !defined(AFX_QUEENS3ADJ_H__19637600_9EEB_4C86_A7DC_069959DCC70F__INCLUDED_)
