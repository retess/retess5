// queens3.cpp : アプリケーション用クラスの定義を行います。
//

#include "stdafx.h"
#include "queens3.h"
#include "queens3Dlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

LPSTR * fileNamesList;
int fileNamesCount;

const UINT incTable[32] = {
		INC_MIDI_CHANNEL_1  ,
		INC_MIDI_CHANNEL_2  ,
		INC_MIDI_CHANNEL_3  ,
		INC_MIDI_CHANNEL_4  ,
		INC_MIDI_CHANNEL_5  ,
		INC_MIDI_CHANNEL_6  ,
		INC_MIDI_CHANNEL_7  ,
		INC_MIDI_CHANNEL_8  ,
		INC_MIDI_CHANNEL_9  ,
		INC_MIDI_CHANNEL_10 ,
		INC_MIDI_CHANNEL_11 ,
		INC_MIDI_CHANNEL_12 ,
		INC_MIDI_CHANNEL_13 ,
		INC_MIDI_CHANNEL_14 ,
		INC_MIDI_CHANNEL_15 ,
		INC_MIDI_CHANNEL_16 ,
		INC_MIDI_CHANNEL_17 ,
		INC_MIDI_CHANNEL_18 ,
		INC_MIDI_CHANNEL_19 ,
		INC_MIDI_CHANNEL_20 ,
		INC_MIDI_CHANNEL_21 ,
		INC_MIDI_CHANNEL_22 ,
		INC_MIDI_CHANNEL_23 ,
		INC_MIDI_CHANNEL_24 ,
		INC_MIDI_CHANNEL_25 ,
		INC_MIDI_CHANNEL_26 ,
		INC_MIDI_CHANNEL_27 ,
		INC_MIDI_CHANNEL_28 ,
		INC_MIDI_CHANNEL_29 ,
		INC_MIDI_CHANNEL_30 ,
		INC_MIDI_CHANNEL_31 ,
		INC_MIDI_CHANNEL_32 
	};

const UINT excTable[32] = {
		EXC_MIDI_CHANNEL_1  ,
		EXC_MIDI_CHANNEL_2  ,
		EXC_MIDI_CHANNEL_3  ,
		EXC_MIDI_CHANNEL_4  ,
		EXC_MIDI_CHANNEL_5  ,
		EXC_MIDI_CHANNEL_6  ,
		EXC_MIDI_CHANNEL_7  ,
		EXC_MIDI_CHANNEL_8  ,
		EXC_MIDI_CHANNEL_9  ,
		EXC_MIDI_CHANNEL_10 ,
		EXC_MIDI_CHANNEL_11 ,
		EXC_MIDI_CHANNEL_12 ,
		EXC_MIDI_CHANNEL_13 ,
		EXC_MIDI_CHANNEL_14 ,
		EXC_MIDI_CHANNEL_15 ,
		EXC_MIDI_CHANNEL_16 ,
		EXC_MIDI_CHANNEL_17 ,
		EXC_MIDI_CHANNEL_18 ,
		EXC_MIDI_CHANNEL_19 ,
		EXC_MIDI_CHANNEL_20 ,
		EXC_MIDI_CHANNEL_21 ,
		EXC_MIDI_CHANNEL_22 ,
		EXC_MIDI_CHANNEL_23 ,
		EXC_MIDI_CHANNEL_24 ,
		EXC_MIDI_CHANNEL_25 ,
		EXC_MIDI_CHANNEL_26 ,
		EXC_MIDI_CHANNEL_27 ,
		EXC_MIDI_CHANNEL_28 ,
		EXC_MIDI_CHANNEL_29 ,
		EXC_MIDI_CHANNEL_30 ,
		EXC_MIDI_CHANNEL_31 ,
		EXC_MIDI_CHANNEL_32 
	};

/////////////////////////////////////////////////////////////////////////////
// CQueens3App

BEGIN_MESSAGE_MAP(CQueens3App, CWinApp)
	//{{AFX_MSG_MAP(CQueens3App)
	//}}AFX_MSG_MAP
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CQueens3App クラスの構築

CQueens3App::CQueens3App()
{
	// TODO: この位置に構築用のコードを追加してください。
	// ここに InitInstance 中の重要な初期化処理をすべて記述してください。
}

/////////////////////////////////////////////////////////////////////////////
// 唯一の CQueens3App オブジェクト

CQueens3App theApp;

/////////////////////////////////////////////////////////////////////////////
// CQueens3App クラスの初期化

BOOL CQueens3App::InitInstance()
{
	AfxEnableControlContainer();

	// 標準的な初期化処理
	// もしこれらの機能を使用せず、実行ファイルのサイズを小さくしたけ
	//  れば以下の特定の初期化ルーチンの中から不必要なものを削除して
	//  ください。

//#ifdef _AFXDLL
//	Enable3dControls();			// 共有 DLL 内で MFC を使う場合はここをコールしてください。
//#else
//	Enable3dControlsStatic();	// MFC と静的にリンクする場合はここをコールしてください。
//#endif

	CQueens3Dlg dlg;
	m_pMainWnd = &dlg;
	int nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: ダイアログが <OK> で消された時のコードを
		//       記述してください。
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: ダイアログが <ｷｬﾝｾﾙ> で消された時のコードを
		//       記述してください。
	}

	// ダイアログが閉じられてからアプリケーションのメッセージ ポンプを開始するよりは、
	// アプリケーションを終了するために FALSE を返してください。
	return FALSE;
}

