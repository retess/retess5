#ifndef _TIMERCONTROL_H_
#define _TIMERCONTROL_H_

#define CIRCLE_MAIN 0
#define CIRCLE_SUB 1
#define POLE_MAIN 2
#define GARDEN_MAIN 3
#define GARDEN_SUB 4

struct TimerEntry {
	BOOL flg;
	int track;
	int seq;
};

#endif