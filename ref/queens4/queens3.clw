; CLW ファイルは MFC ClassWizard の情報を含んでいます。

[General Info]
Version=1
LastClass=CQueens3adj
LastTemplate=CFrameWnd
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "queens3.h"

ClassCount=9
Class1=CQueens3App
Class2=CQueens3Dlg
Class3=CAboutDlg

ResourceCount=4
Resource1=IDD_ABOUTBOX
Resource2=IDR_MAINFRAME
Resource3=IDD_QUEENS3_DIALOG
Class4=CQueens3timer
Class5=CQueens3midi
Class6=Cqueens3View
Class7=Queens3View
Class8=queens3View
Class9=CQueens3adj
Resource4=IDD_WAIT_DIALOG

[CLS:CQueens3App]
Type=0
HeaderFile=queens3.h
ImplementationFile=queens3.cpp
Filter=N
BaseClass=CWinApp
VirtualFilter=AC
LastObject=CQueens3App

[CLS:CQueens3Dlg]
Type=0
HeaderFile=queens3Dlg.h
ImplementationFile=queens3Dlg.cpp
Filter=D
LastObject=CQueens3Dlg
BaseClass=CDialog
VirtualFilter=dWC

[CLS:CAboutDlg]
Type=0
HeaderFile=queens3Dlg.h
ImplementationFile=queens3Dlg.cpp
Filter=D
LastObject=CAboutDlg

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_QUEENS3_DIALOG]
Type=1
Class=CQueens3Dlg
ControlCount=166
Control1=IDOK,button,1342242817
Control2=IDC_ALARM,button,1342308359
Control3=IDC_STATIC1,static,1342308353
Control4=IDC_STATIC2,static,1342308353
Control5=IDC_STATIC3,static,1342308353
Control6=IDC_CIRCLE_M,button,1342242816
Control7=IDC_CIRCLE_S,button,1342242816
Control8=IDC_CIRCLE_N,button,1342242816
Control9=IDC_POLE_M,button,1342242816
Control10=IDC_POLE_N,button,1342242816
Control11=IDC_GARDEN_M,button,1342242816
Control12=IDC_GARDEN_S,button,1342242816
Control13=IDC_GARDEN_N,button,1342242816
Control14=IDC_STATIC,static,1342308354
Control15=IDC_STATIC,static,1342308354
Control16=IDC_STATIC,static,1342308354
Control17=IDC_STATIC,static,1342308354
Control18=IDC_STATIC,static,1342308354
Control19=IDC_STATIC,static,1342308354
Control20=IDC_STATIC,static,1342308354
Control21=IDC_STATIC,static,1342308354
Control22=IDC_STATIC,static,1342308354
Control23=IDC_STATIC,static,1342308354
Control24=IDC_STATIC,static,1342308354
Control25=IDC_STATIC,static,1342308354
Control26=IDC_STATIC,static,1342308354
Control27=IDC_STATIC,static,1342308354
Control28=IDC_STATIC,static,1342308354
Control29=IDC_STATIC,static,1342308354
Control30=IDC_CIRCLE08_M,button,1342242944
Control31=IDC_CIRCLE08_S,button,1342242944
Control32=IDC_CIRCLE08_N,button,1342242944
Control33=IDC_POLE08_M,button,1342242944
Control34=IDC_POLE08_N,button,1342242944
Control35=IDC_GARDEN08_M,button,1342242944
Control36=IDC_GARDEN08_S,button,1342242944
Control37=IDC_GARDEN08_N,button,1342242944
Control38=IDC_CIRCLE09_M,button,1342242944
Control39=IDC_CIRCLE09_S,button,1342242944
Control40=IDC_CIRCLE09_N,button,1342242944
Control41=IDC_POLE09_M,button,1342242944
Control42=IDC_POLE09_N,button,1342242944
Control43=IDC_GARDEN09_M,button,1342242944
Control44=IDC_GARDEN09_S,button,1342242944
Control45=IDC_GARDEN09_N,button,1342242944
Control46=IDC_CIRCLE10_M,button,1342242944
Control47=IDC_CIRCLE10_S,button,1342242944
Control48=IDC_CIRCLE10_N,button,1342242944
Control49=IDC_POLE10_M,button,1342242944
Control50=IDC_POLE10_N,button,1342242944
Control51=IDC_GARDEN10_M,button,1342242944
Control52=IDC_GARDEN10_S,button,1342242944
Control53=IDC_GARDEN10_N,button,1342242944
Control54=IDC_CIRCLE11_M,button,1342242944
Control55=IDC_CIRCLE11_S,button,1342242944
Control56=IDC_CIRCLE11_N,button,1342242944
Control57=IDC_POLE11_M,button,1342242944
Control58=IDC_POLE11_N,button,1342242944
Control59=IDC_GARDEN11_M,button,1342242944
Control60=IDC_GARDEN11_S,button,1342242944
Control61=IDC_GARDEN11_N,button,1342242944
Control62=IDC_CIRCLE12_M,button,1342242944
Control63=IDC_CIRCLE12_S,button,1342242944
Control64=IDC_CIRCLE12_N,button,1342242944
Control65=IDC_POLE12_M,button,1342242944
Control66=IDC_POLE12_N,button,1342242944
Control67=IDC_GARDEN12_M,button,1342242944
Control68=IDC_GARDEN12_S,button,1342242944
Control69=IDC_GARDEN12_N,button,1342242944
Control70=IDC_CIRCLE13_M,button,1342242944
Control71=IDC_CIRCLE13_S,button,1342242944
Control72=IDC_CIRCLE13_N,button,1342242944
Control73=IDC_POLE13_M,button,1342242944
Control74=IDC_POLE13_N,button,1342242944
Control75=IDC_GARDEN13_M,button,1342242944
Control76=IDC_GARDEN13_S,button,1342242944
Control77=IDC_GARDEN13_N,button,1342242944
Control78=IDC_CIRCLE14_M,button,1342242944
Control79=IDC_CIRCLE14_S,button,1342242944
Control80=IDC_CIRCLE14_N,button,1342242944
Control81=IDC_POLE14_M,button,1342242944
Control82=IDC_POLE14_N,button,1342242944
Control83=IDC_GARDEN14_M,button,1342242944
Control84=IDC_GARDEN14_S,button,1342242944
Control85=IDC_GARDEN14_N,button,1342242944
Control86=IDC_CIRCLE15_M,button,1342242944
Control87=IDC_CIRCLE15_S,button,1342242944
Control88=IDC_CIRCLE15_N,button,1342242944
Control89=IDC_POLE15_M,button,1342242944
Control90=IDC_POLE15_N,button,1342242944
Control91=IDC_GARDEN15_M,button,1342242944
Control92=IDC_GARDEN15_S,button,1342242944
Control93=IDC_GARDEN15_N,button,1342242944
Control94=IDC_CIRCLE16_M,button,1342242944
Control95=IDC_CIRCLE16_S,button,1342242944
Control96=IDC_CIRCLE16_N,button,1342242944
Control97=IDC_POLE16_M,button,1342242944
Control98=IDC_POLE16_N,button,1342242944
Control99=IDC_GARDEN16_M,button,1342242944
Control100=IDC_GARDEN16_S,button,1342242944
Control101=IDC_GARDEN16_N,button,1342242944
Control102=IDC_CIRCLE17_M,button,1342242944
Control103=IDC_CIRCLE17_S,button,1342242944
Control104=IDC_CIRCLE17_N,button,1342242944
Control105=IDC_POLE17_M,button,1342242944
Control106=IDC_POLE17_N,button,1342242944
Control107=IDC_GARDEN17_M,button,1342242944
Control108=IDC_GARDEN17_S,button,1342242944
Control109=IDC_GARDEN17_N,button,1342242944
Control110=IDC_CIRCLE18_M,button,1342242944
Control111=IDC_CIRCLE18_S,button,1342242944
Control112=IDC_CIRCLE18_N,button,1342242944
Control113=IDC_POLE18_M,button,1342242944
Control114=IDC_POLE18_N,button,1342242944
Control115=IDC_GARDEN18_M,button,1342242944
Control116=IDC_GARDEN18_S,button,1342242944
Control117=IDC_GARDEN18_N,button,1342242944
Control118=IDC_CIRCLE19_M,button,1342242944
Control119=IDC_CIRCLE19_S,button,1342242944
Control120=IDC_CIRCLE19_N,button,1342242944
Control121=IDC_POLE19_M,button,1342242944
Control122=IDC_POLE19_N,button,1342242944
Control123=IDC_GARDEN19_M,button,1342242944
Control124=IDC_GARDEN19_S,button,1342242944
Control125=IDC_GARDEN19_N,button,1342242944
Control126=IDC_CIRCLE20_M,button,1342242944
Control127=IDC_CIRCLE20_S,button,1342242944
Control128=IDC_CIRCLE20_N,button,1342242944
Control129=IDC_POLE20_M,button,1342242944
Control130=IDC_POLE20_N,button,1342242944
Control131=IDC_GARDEN20_M,button,1342242944
Control132=IDC_GARDEN20_S,button,1342242944
Control133=IDC_GARDEN20_N,button,1342242944
Control134=IDC_CIRCLE21_M,button,1342242944
Control135=IDC_CIRCLE21_S,button,1342242944
Control136=IDC_CIRCLE21_N,button,1342242944
Control137=IDC_POLE21_M,button,1342242944
Control138=IDC_POLE21_N,button,1342242944
Control139=IDC_GARDEN21_M,button,1342242944
Control140=IDC_GARDEN21_S,button,1342242944
Control141=IDC_GARDEN21_N,button,1342242944
Control142=IDC_CIRCLE22_M,button,1342242944
Control143=IDC_CIRCLE22_S,button,1342242944
Control144=IDC_CIRCLE22_N,button,1342242944
Control145=IDC_POLE22_M,button,1342242944
Control146=IDC_POLE22_N,button,1342242944
Control147=IDC_GARDEN22_M,button,1342242944
Control148=IDC_GARDEN22_S,button,1342242944
Control149=IDC_GARDEN22_N,button,1342242944
Control150=IDC_CIRCLE23_M,button,1342242944
Control151=IDC_CIRCLE23_S,button,1342242944
Control152=IDC_CIRCLE23_N,button,1342242944
Control153=IDC_POLE23_M,button,1342242944
Control154=IDC_POLE23_N,button,1342242944
Control155=IDC_GARDEN23_M,button,1342242944
Control156=IDC_GARDEN23_S,button,1342242944
Control157=IDC_GARDEN23_N,button,1342242944
Control158=IDC_STATIC,button,1342177287
Control159=IDC_STATIC,button,1342177287
Control160=IDC_STATIC,button,1342177287
Control161=IDC_STATIC,button,1342177287
Control162=IDC_WEEK_LIST,listbox,1352728835
Control163=IDC_STATIC,static,1342308352
Control164=IDC_INITIALIZE,button,1342242816
Control165=IDC_SAVE_TO_FILE,button,1342242816
Control166=IDC_DISPLAY,button,1342242819

[DLG:IDD_WAIT_DIALOG]
Type=1
Class=?
ControlCount=2
Control1=IDC_TITLE,static,1342308353
Control2=IDC_WAIT2,static,1342308352

[CLS:CQueens3timer]
Type=0
HeaderFile=Queens3timer.h
ImplementationFile=Queens3timer.cpp
BaseClass=CWinThread
Filter=N
LastObject=CQueens3timer

[CLS:CQueens3midi]
Type=0
HeaderFile=Queens3midi.h
ImplementationFile=Queens3midi.cpp
BaseClass=CWinThread
Filter=N
LastObject=CQueens3midi

[CLS:Cqueens3View]
Type=0
HeaderFile=queens3View.h
ImplementationFile=queens3View.cpp
BaseClass=CView
Filter=C
VirtualFilter=VWC
LastObject=Cqueens3View

[CLS:CQueens3adj]
Type=0
HeaderFile=Queens3adj.h
ImplementationFile=Queens3adj.cpp
BaseClass=CFrameWnd
Filter=T
VirtualFilter=fWC
LastObject=CQueens3adj

