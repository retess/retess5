// queens3Dlg.h : ヘッダー ファイル
//

#if !defined(AFX_QUEENS3DLG_H__A994E5F3_C44D_49D2_AD17_3EC72956DC4B__INCLUDED_)
#define AFX_QUEENS3DLG_H__A994E5F3_C44D_49D2_AD17_3EC72956DC4B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <string.h>
#include <stdio.h>
#include "queens3midi.h"
#include "queens3timer.h"
#include "queens3view.h"

#define TIMERSTOP 0
#define TIMERMAIN 1
#define TIMERSUB  2

/////////////////////////////////////////////////////////////////////////////
// CQueens3Dlg ダイアログ

class CQueens3Dlg : public CDialog
{
// 構築
public:
	CQueens3Dlg(CWnd* pParent = NULL);	// 標準のコンストラクタ

// ダイアログ データ
	//{{AFX_DATA(CQueens3Dlg)
	enum { IDD = IDD_QUEENS3_DIALOG };
	CString	m_strWeek;
	//}}AFX_DATA

	// ClassWizard は仮想関数のオーバーライドを生成します。
	//{{AFX_VIRTUAL(CQueens3Dlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV のサポート
	//}}AFX_VIRTUAL

// アトリビュート
public:
	BOOL start;
	BOOL	DisplayFlg;
	BOOL	DebugFlg;
	CBitmap * m_bmpSet;
	CBitmap * m_bmpUnset;
	int * m_iDayList;
	int * m_iMonthList;
	int * m_iYearList;
	BOOL * m_bFileExist;
	CListBox* pLB;
	int * m_iCircle;
	int * m_iPole;
	int * m_iGarden;
	CQueens3midi * player;
	CQueens3timer * sensors;
	CStatusBar * statusbar;
	CStatusBarCtrl * statusbarctrl;
	Cqueens3View * view;
	UINT StartTime;

	// インプリメンテーション
public:
	void ExitApp();
	void AdjustOclock();
	CQueens3adj * adjuster;
    void OnStartProg();
	void OnDemo();
	void OnRestart();
	void startSeq(int DirFlg);
	void GetMIDIFiles();
	void PlayEnd();
	void EndSeq(BOOL force);
	void PlayInitSeq();
	void ModifyTodayPreset(BOOL load);
	void ReportLog(DWORD dwEventID,WORD wCategory);

protected:
	HICON m_hIcon;
	void ViewPreset();
	void SetDefault();
	void CircleSet(int set, int time);
	void PoleSet(int set, int time);
	void GardenSet(int set, int time);
	void deleteMem();
	void ClearYesterdayData();
	void ReadPresetData();

	// 生成されたメッセージ マップ関数
	//{{AFX_MSG(CQueens3Dlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	virtual void OnOK();
	afx_msg void OnDisplay();
	afx_msg void OnSelchangeWeekList();
	afx_msg void OnCircle08N();
	afx_msg void OnCircle08S();
	afx_msg void OnCircle08M();
	afx_msg void OnCircle09M();
	afx_msg void OnCircle09N();
	afx_msg void OnCircle09S();
	afx_msg void OnCircle10M();
	afx_msg void OnCircle10N();
	afx_msg void OnCircle10S();
	afx_msg void OnCircle11M();
	afx_msg void OnCircle11N();
	afx_msg void OnCircle11S();
	afx_msg void OnCircle12M();
	afx_msg void OnCircle12N();
	afx_msg void OnCircle12S();
	afx_msg void OnCircle13M();
	afx_msg void OnCircle13N();
	afx_msg void OnCircle13S();
	afx_msg void OnCircle14M();
	afx_msg void OnCircle14N();
	afx_msg void OnCircle14S();
	afx_msg void OnCircle15M();
	afx_msg void OnCircle15N();
	afx_msg void OnCircle15S();
	afx_msg void OnCircle16M();
	afx_msg void OnCircle16N();
	afx_msg void OnCircle16S();
	afx_msg void OnCircle17M();
	afx_msg void OnCircle17N();
	afx_msg void OnCircle17S();
	afx_msg void OnCircle18M();
	afx_msg void OnCircle18N();
	afx_msg void OnCircle18S();
	afx_msg void OnCircle19M();
	afx_msg void OnCircle19N();
	afx_msg void OnCircle19S();
	afx_msg void OnCircle20M();
	afx_msg void OnCircle20N();
	afx_msg void OnCircle20S();
	afx_msg void OnCircle21M();
	afx_msg void OnCircle21N();
	afx_msg void OnCircle21S();
	afx_msg void OnCircle22M();
	afx_msg void OnCircle22N();
	afx_msg void OnCircle22S();
	afx_msg void OnCircle23M();
	afx_msg void OnCircle23N();
	afx_msg void OnCircle23S();
	afx_msg void OnGarden08M();
	afx_msg void OnGarden08N();
	afx_msg void OnGarden08S();
	afx_msg void OnGarden09M();
	afx_msg void OnGarden09N();
	afx_msg void OnGarden09S();
	afx_msg void OnGarden10M();
	afx_msg void OnGarden10N();
	afx_msg void OnGarden10S();
	afx_msg void OnGarden11M();
	afx_msg void OnGarden11N();
	afx_msg void OnGarden11S();
	afx_msg void OnGarden12M();
	afx_msg void OnGarden12N();
	afx_msg void OnGarden12S();
	afx_msg void OnGarden13M();
	afx_msg void OnGarden13N();
	afx_msg void OnGarden13S();
	afx_msg void OnGarden14M();
	afx_msg void OnGarden14N();
	afx_msg void OnGarden14S();
	afx_msg void OnGarden15M();
	afx_msg void OnGarden15N();
	afx_msg void OnGarden15S();
	afx_msg void OnGarden16M();
	afx_msg void OnGarden16N();
	afx_msg void OnGarden16S();
	afx_msg void OnGarden17M();
	afx_msg void OnGarden17N();
	afx_msg void OnGarden17S();
	afx_msg void OnGarden18M();
	afx_msg void OnGarden18N();
	afx_msg void OnGarden18S();
	afx_msg void OnGarden19M();
	afx_msg void OnGarden19N();
	afx_msg void OnGarden19S();
	afx_msg void OnGarden20M();
	afx_msg void OnGarden20N();
	afx_msg void OnGarden20S();
	afx_msg void OnGarden21M();
	afx_msg void OnGarden21N();
	afx_msg void OnGarden21S();
	afx_msg void OnGarden22M();
	afx_msg void OnGarden22N();
	afx_msg void OnGarden22S();
	afx_msg void OnGarden23M();
	afx_msg void OnGarden23N();
	afx_msg void OnGarden23S();
	afx_msg void OnPole08M();
	afx_msg void OnPole08N();
	afx_msg void OnPole09M();
	afx_msg void OnPole09N();
	afx_msg void OnPole10M();
	afx_msg void OnPole10N();
	afx_msg void OnPole11M();
	afx_msg void OnPole11N();
	afx_msg void OnPole12M();
	afx_msg void OnPole12N();
	afx_msg void OnPole13M();
	afx_msg void OnPole13N();
	afx_msg void OnPole14M();
	afx_msg void OnPole14N();
	afx_msg void OnPole15M();
	afx_msg void OnPole15N();
	afx_msg void OnPole16M();
	afx_msg void OnPole16N();
	afx_msg void OnPole17M();
	afx_msg void OnPole17N();
	afx_msg void OnPole18M();
	afx_msg void OnPole18N();
	afx_msg void OnPole19M();
	afx_msg void OnPole19N();
	afx_msg void OnPole20M();
	afx_msg void OnPole20N();
	afx_msg void OnPole21M();
	afx_msg void OnPole21N();
	afx_msg void OnPole22M();
	afx_msg void OnPole22N();
	afx_msg void OnPole23M();
	afx_msg void OnPole23N();
	afx_msg void OnCircleM();
	afx_msg void OnCircleN();
	afx_msg void OnCircleS();
	afx_msg void OnGardenM();
	afx_msg void OnGardenN();
	afx_msg void OnGardenS();
	afx_msg void OnPoleM();
	afx_msg void OnPoleN();
	afx_msg void OnSaveToFile();
	afx_msg void OnInitialize();
	afx_msg void OnOclock();
	afx_msg void OnAdjustOclock();
	afx_msg void OnShowAdjuster();
	//}}AFX_MSG
	afx_msg LRESULT OnAdjustTime(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ は前行の直前に追加の宣言を挿入します。

#endif // !defined(AFX_QUEENS3DLG_H__A994E5F3_C44D_49D2_AD17_3EC72956DC4B__INCLUDED_)
