#if !defined(AFX_QUEENS3MIDI_H__357AD345_E245_41B1_87BC_50ED2C2A111E__INCLUDED_)
#define AFX_QUEENS3MIDI_H__357AD345_E245_41B1_87BC_50ED2C2A111E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Queens3midi.h : ヘッダー ファイル
//



#define	STRINGSIZE	128		/* how big is a string? */
#include <wtypes.h>
#include <mmsystem.h>
#include <afxmt.h>
#include <afxtempl.h>
#include "queens3View.h"

#include "timercontrol.h"

UINT MidiThreadProc( LPVOID pParam );

/////////////////////////////////////////////////////////////////////////////
// CQueens3midi スレッド

class CQueens3midi : public CWinThread
{
	DECLARE_DYNCREATE(CQueens3midi)
public:
	CQueens3midi();           // 動的生成に使用されるプロテクト コンストラクタ
	virtual ~CQueens3midi();

// アトリビュート
public:
	HWND m_hWnd;
	int GardenMain;
	int CircleMain;
	unsigned char * trackStatus;
    CMap<CString,LPCSTR,int,int> * pChannelMap;/////
	int DirFlg;
//	BOOL DemoFlg,*CheckLoopFlg;
	BOOL *CheckLoopFlg;
//	BOOL repressFadeFlg;
//	int * seqTide;
	int currentUpSeq;
//	int * rouletteTable;
//	int MCWeight;
//	BOOL TempMode;
	QUEEN_play_time * seqPlayTime;
//	UINT * seqDegree;
	UINT FIPMIDIChannel;
	UINT FOPMIDIChannel;
//	int SensorMIDIChannel[16];
	UINT currentTrack;
	LPSTR * seqFOPData;
	LPSTR * seqFIPData;
	LPSTR * seqStartData;
	LPSTR * seqEndData;
	UINT * seqStartPoint;
	UINT * seqEndPoint;
	BOOL * seqFOPCommand;
	BOOL * seqFIPCommand;
	BOOL * seqEndCommand;
	BOOL * seqStartCommand;
	int currentSeq;
//	UINT *seqWeight;
	UINT *seqSensor;
	UINT *trackEnv;
	int *seqOutPort;/////
	UINT *seqChannel;/////
	int * seqGroup;/////
	int *seqMid;/////
	int *seqPid;/////
//	int *seqPriority;/////
//	UINT ActiveMIDIChannel;
	int StartMIDIChannel;
	int EndMIDIChannel;
	int * seqSirenCommand;
	BOOL * seqTimerCommand;
	BOOL DebugFlg;
	UINT startTime;
	SYSTEMTIME startSysTime;
	UINT currentTime;
//	BOOL FastBoot;
	int * seqInitial;
	BOOL seqLock2;
	BOOL seqLock1;
	UINT * vUpSeq;
	UINT cUpSeq;
	CWinThread * MidiThread;
	BOOL loopFlg;
	BOOL * trackEndFlg;
	UINT * trackTics;
	BOOL start;
	Cqueens3View * View;
	UINT * trackStartTime;
	float * Tempo;
	LPSTR * pTopTrack;
	LPSTR * pBottomTrack;
	UINT * division;
	UINT * nTrack;
	LPSTR * pTrack;
	UINT nseq;
	UINT * trackSize;
	LPSTR pbuffer;
//	HMIDIOUT hMidiout;
	HMIDIOUT hMidiout[16];
	TimerEntry (* pTimerEntry)[5];

// オペレーション
public:
	BOOL DisplayFlg;
//	void GetCurrentTimeFromMIDIProcess(SYSTEMTIME *st);
	void ModifyCurrentSeq2(int type,int seq);
	void ModifyCurrentSeq1(int type,int seq);
	void StopPart(int part);
	BOOL GetTimeFromString(char *msg,SYSTEMTIME * st);
	void ShowMIDIMessage(unsigned char c,unsigned char *rp);
	void playinit();
	void playResume();
	void deleteMem();
	void GetChannelInfo(CMap<CString,LPCSTR,int,int> *pcm);/////
	int GetSeqGroup(int currentSeq);/////
	void GetFileInfo(UINT nfiles, LPSTR *path);
	UINT sendMIDILong(LPSTR msg,UINT mlength);
	UINT OpenMidiOut();
	int OpenMidiOutSelect(int wMid, int wPid);//select Midi Port
	UINT CloseMidiOut();
	UINT sendMIDIEvent( unsigned char bStatus, 
						unsigned char bData1, unsigned char bData2);
	unsigned long Get24(LPSTR p);
	UINT Get16(LPSTR p);
	void AddMeta1(LPSTR msg,unsigned int type);
	void AddMeta2(LPSTR msg,unsigned int type);
	unsigned long SkipDeltaT(unsigned char **rp,long *size);
	unsigned long SkipVari(unsigned char **rp,long *size);
	UINT Skip8(unsigned char **rp,long *size);
	UINT Skip16(unsigned char **rp,long *size);
	unsigned long Skip32(unsigned char **rp,long *size);
	unsigned long ReadDeltaT(unsigned char **rp,long *size, unsigned char **wp);
	unsigned long ReadVari(unsigned char **rp,long *size,unsigned char **wp);
	unsigned long Read32(unsigned char **rp,long *size,unsigned char **wp);
	UINT Read16(unsigned char **rp,long *size,unsigned char **wp);
	UINT Read8(unsigned char **rp,long *size,unsigned char **wp);
	UINT MidiProc(); 
	void ReportLog(DWORD dwEventID,WORD wCategory);

// オーバーライド
	// ClassWizard は仮想関数のオーバーライドを生成します。
	//{{AFX_VIRTUAL(CQueens3midi)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// インプリメンテーション
protected:
	int CurrentPort;//select current Midi Port

	// 生成されたメッセージ マップ関数
	//{{AFX_MSG(CQueens3midi)
		// メモ - ClassWizard はこの位置にメンバ関数を追加または削除します。
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
public:
	void CloseMidiOut2(void);
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ は前行の直前に追加の宣言を挿入します。

#endif // !defined(AFX_QUEENS3MIDI_H__357AD345_E245_41B1_87BC_50ED2C2A111E__INCLUDED_)
