#ifndef queens3MID_H
#define queens3MID_H
// queens3mid.h : ヘッダー ファイル
//

/////////////////////////////////////////////////////////////////////////////
// Cqueens3mid ウィンドウ
#define	STRINGSIZE	128		/* how big is a string? */
#include <wtypes.h>
#include <mmsystem.h>
#include <afxmt.h>
#include <afxtempl.h>
#include "queens3View.h"

#include "timercontrol.h"

UINT MidiThreadProc( LPVOID pParam );

class Cqueens3mid : public CWnd
{
	DECLARE_DYNAMIC(Cqueens3mid)
// コンストラクション
public:
	int GardenMain;
	int CircleMain;
	unsigned char * trackStatus;
    CMap<CString,LPCSTR,int,int> * pChannelMap;/////
	int DirFlg;
//	BOOL DemoFlg,*CheckLoopFlg;
	BOOL *CheckLoopFlg;
//	BOOL repressFadeFlg;
//	int * seqTide;
//	void GetCurrentTimeFromMIDIProcess(SYSTEMTIME *st);
	void ModifyCurrentSeq2(int type,int seq);
	void ModifyCurrentSeq1(int type,int seq);
	void StopPart(int part);
	BOOL GetTimeFromString(char *msg,SYSTEMTIME * st);
//	void SensorToMIDIChannel(int ach,int sensorStep);
//	void setNextActiveTrack(int up,UINT ct);

// アトリビュート
public:
	int currentUpSeq;
//	int * rouletteTable;
//	int MCWeight;
//	BOOL TempMode;
	QUEEN_play_time * seqPlayTime;
//	UINT * seqDegree;
	UINT FIPMIDIChannel;
	UINT FOPMIDIChannel;
//	int SensorMIDIChannel[16];
	UINT currentTrack;
	LPSTR * seqFOPData;
	LPSTR * seqFIPData;
	LPSTR * seqStartData;
	LPSTR * seqEndData;
	UINT * seqStartPoint;
	UINT * seqEndPoint;
	BOOL * seqFOPCommand;
	BOOL * seqFIPCommand;
	BOOL * seqEndCommand;
	BOOL * seqStartCommand;
	int currentSeq;
//	UINT *seqWeight;
	UINT *seqSensor;
	UINT *trackEnv;
	int *seqOutPort;/////
	UINT *seqChannel;/////
	int * seqGroup;/////
	int *seqMid;/////
	int *seqPid;/////
//	int *seqPriority;/////
//	UINT ActiveMIDIChannel;
	int StartMIDIChannel;
	int EndMIDIChannel;
	int * seqSirenCommand;
	BOOL * seqTimerCommand;
	BOOL DebugFlg;
	UINT startTime;
	SYSTEMTIME startSysTime;
	UINT currentTime;
//	BOOL FastBoot;
	int * seqInitial;
	BOOL seqLock2;
	BOOL seqLock1;
	UINT * vUpSeq;
	UINT cUpSeq;
	CWinThread * MidiThread;
	BOOL loopFlg;
	BOOL * trackEndFlg;
	UINT * trackTics;
	BOOL start;
	Cqueens3View * View;
	UINT * trackStartTime;
	float * Tempo;
	LPSTR * pTopTrack;
	LPSTR * pBottomTrack;
	UINT * division;
	UINT * nTrack;
	LPSTR * pTrack;
	UINT nseq;
	UINT * trackSize;
	LPSTR pbuffer;
//	HMIDIOUT hMidiout;
	HMIDIOUT hMidiout[16];
//	UINT wCounter[MAX_WEIGHT];
//	CMap<CString,LPCSTR,long,long> * pChannelMap;/////
	TimerEntry (* pTimerEntry)[5];

protected:
	int CurrentPort;//select current Midi Port

// オペレーション
public:
	void ShowMIDIMessage(unsigned char c,unsigned char *rp);
	void playinit();
	void playResume();
	void deleteMem();
	void GetChannelInfo(CMap<CString,LPCSTR,int,int> *pcm);/////
	int GetSeqGroup(int currentSeq);/////
	void GetFileInfo(UINT nfiles, LPSTR *path);
	Cqueens3mid();
	UINT sendMIDILong(LPSTR msg,UINT mlength);
	UINT OpenMidiOut();
	int OpenMidiOutSelect(int wMid, int wPid);//select Midi Port
	UINT CloseMidiOut();
	UINT sendMIDIEvent( unsigned char bStatus, 
						unsigned char bData1, unsigned char bData2);
	unsigned long Get24(LPSTR p);
	UINT Get16(LPSTR p);
	void AddMeta1(LPSTR msg,unsigned int type);
	void AddMeta2(LPSTR msg,unsigned int type);
	unsigned long SkipDeltaT(unsigned char **rp,long *size);
	unsigned long SkipVari(unsigned char **rp,long *size);
	UINT Skip8(unsigned char **rp,long *size);
	UINT Skip16(unsigned char **rp,long *size);
	unsigned long Skip32(unsigned char **rp,long *size);
	unsigned long ReadDeltaT(unsigned char **rp,long *size, unsigned char **wp);
	unsigned long ReadVari(unsigned char **rp,long *size,unsigned char **wp);
	unsigned long Read32(unsigned char **rp,long *size,unsigned char **wp);
	UINT Read16(unsigned char **rp,long *size,unsigned char **wp);
	UINT Read8(unsigned char **rp,long *size,unsigned char **wp);
	UINT MidiProc(); 

// オーバーライド
	// ClassWizard は仮想関数を生成しオーバーライドします。
	//{{AFX_VIRTUAL(Cqueens3mid)
	//}}AFX_VIRTUAL

// インプリメンテーション
public:
	virtual ~Cqueens3mid();

	// 生成されたメッセージ マップ関数
protected:
	//{{AFX_MSG(Cqueens3mid)
	//afx_msg void OnTestSiren();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
#endif
/////////////////////////////////////////////////////////////////////////////
