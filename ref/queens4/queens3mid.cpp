// QUEENmid.cpp : インプリメンテーション ファイル
//
#include "stdafx.h"
#include "queens3.h"
#include "queens3mid.h"
#include "resource.h"
#include <stdlib.h>
#include <mmsystem.h>
#include <ctype.h>
#include <string.h>
#include <time.h>

#include <mmreg.h>//the manufacturer and product identifiers
#include <afxtempl.h>
#include <math.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define WIND_SEQ 3
#define RAIN_SEQ 1

#define NUM_COMMAND 4

#define START_DATA    "STA"
#define END_DATA      "END"
#define FADE_IN_DATA  "FIP"
#define FADE_OUT_DATA "FOP"

#define START_DATA_COMMAND    0
#define END_DATA_COMMAND      1
#define FADE_IN_DATA_COMMAND  2
#define FADE_OUT_DATA_COMMAND 3

#define NUM_TIMER 2

#define TIMER_START   "TSTA"
#define TIMER_END     "TEND"

#define TIMER_START_COMMAND   0
#define TIMER_END_COMMAND     1

static const int chantype[] = {
		0, 0, 0, 0, 0, 0, 0, 0,
		2, 2, 2, 2, 1, 1, 2, 0
	};

/////////////////////////////////////////////////////////////////////////////
// CQUEENmid

IMPLEMENT_DYNAMIC(Cqueens3mid,CWnd)
Cqueens3mid::Cqueens3mid()
{
//	int i;

	nseq=cUpSeq=0;
	pbuffer = (LPSTR)NULL;
	seqPlayTime = (QUEEN_play_time *)NULL;
	seqInitial = (int *)NULL;
	seqOutPort = (int *)NULL;/////
	seqChannel = (UINT *)NULL;/////
	seqGroup = (int *)NULL;/////
	seqMid = seqPid = (int *)NULL;/////
//	seqPriority = (int *)NULL;/////
	pTopTrack = pTrack = pBottomTrack = (LPSTR *)NULL;
	trackSize =	nTrack = trackStartTime = trackTics = vUpSeq = (UINT *)NULL;
	trackEndFlg = (BOOL *)NULL;
	trackStatus = (unsigned char *)NULL;
	trackEnv = (UINT *)NULL;
	seqStartCommand = seqEndCommand = seqFIPCommand = seqFOPCommand = (BOOL *)NULL;
	seqTimerCommand = (BOOL *)NULL;
	seqSirenCommand = (int *)NULL;
	seqStartPoint = seqEndPoint = division = (UINT *)NULL;
	seqStartData = seqEndData = seqFIPData = seqFOPData = (LPSTR *)NULL;
	Tempo = (float *)NULL;
	start = loopFlg = seqLock1 = seqLock2 = FALSE;
	currentUpSeq = 0;
	StartMIDIChannel = EndMIDIChannel = FIPMIDIChannel = FOPMIDIChannel = 0;
	pChannelMap = (CMap<CString,LPCSTR,int,int> *)NULL;
	pTimerEntry = (TimerEntry (*)[5])NULL;

	CurrentPort=0;
}

Cqueens3mid::~Cqueens3mid()
{
	if(MidiThread != NULL) MidiThread = NULL;
}


BEGIN_MESSAGE_MAP(Cqueens3mid, CWnd)
	//{{AFX_MSG_MAP(Cqueens3mid)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Cqueens3mid メッセージ ハンドラ

void Cqueens3mid::GetChannelInfo(CMap<CString,LPCSTR,int,int> *pcm)
{
	FILE *fp;
	int Mid, Pid;
	int Group[16];
	int i, j, k;

	char cBuf[256];
	char cTmp[5];
	short sCount;

	char cChannel[16];

	char text[MAX_PATH];

	sprintf(text, "\\QUEEN3\\Init");
	SetCurrentDirectory(text);

#ifdef DEBUG
	AfxMessageBox("GetChannelInfo", MB_OK);
#endif
	if((fp = fopen("Channel.dat","r"))==NULL){
		TRACE("Channel.dat dose not exist.\n");
		return ;
	}

	while(fgets(cBuf,255,fp)){
		cBuf[255]=NULL;
		i=j=k=sCount=0;
		if(cBuf[0]==EOF||cBuf[0]==NULL)
			break;
//		TRACE("%s",cBuf);

		for(i=0;(i<256)&&(sCount<18);i++){
			if(cBuf[i]==':'){
				if(sCount==0){
					cTmp[j] = NULL;
					Mid = atoi(cTmp);
					sCount ++;
					j = 0;
				}else if(sCount==1){
					cTmp[j] = NULL;	
					Pid = atoi(cTmp);
					sCount ++;
					j = 0;
				}else{
					cTmp[j] = NULL;	
					Group[k++] = atoi(cTmp);
					sCount ++;
					j = 0;
				}
			}else if(cBuf[i]=='\n'||cBuf[i]=='\0'||cBuf[i]==EOF){
				cTmp[j] = NULL;	
				Group[k++] = atoi(cTmp);
				sCount ++;
				j = 0;
				break;
			}else{
				cTmp[j++]=cBuf[i];
			}
		}

#ifdef DEBUG
	AfxMessageBox("GetChannelInfo'SetAt", MB_OK);
#endif
		for(i=0;i<16;i++){
			sprintf(cChannel,"%d:%d:%d",Mid,Pid,i);//i = channel - 1;
			pcm->SetAt( cChannel, Group[i]);
		}
		TRACE("Mid:%d Pid:%d\n",Mid,Pid);
		for(i=0;i<16;i++)
			TRACE("Channel%d: %d  ", i, Group[i]);
		TRACE("\n");
	}
	fclose(fp);
    pChannelMap = pcm;
}

int Cqueens3mid::GetSeqGroup(int cs)
{
	int i,flg,sc,Group = 0;
	UINT ch;
	char cChannel[15];

	for(i=flg=0;i<32;i++)
	{
		if((ch = seqChannel[cs] & incTable[i]) != 0)
		{
			sc = i % 16;
			flg = 1;
			break;
		}
	}
	if(flg)
	{
	    sprintf(cChannel,"%d:%d:%d"
		    ,(int)seqMid[cs]
		    ,(int)seqPid[cs]
		    ,sc);

	    if(!pChannelMap->Lookup(cChannel, Group))
		    TRACE("No Value\n");

	//TRACE("%s %d\n",cChannel,Group);
	}
	return Group;
}
	
void Cqueens3mid::GetFileInfo(UINT nfiles, LPSTR *path)
{
	FILE *fp;
	long bsize = 0;
	unsigned char *rp,*wp,*dp,*rbase,c,c1,c2,status;
	LPSTR *ptrack,*ptoptrack,*pbottomtrack,cp;
	long fsize,size;
	unsigned char b[STRINGSIZE + 1];
	char text[256];
	UINT tracks,format,tr,trk,cdiv,lookfor,tc;
	float ctempo;
	UINT *ptsize,i,j,ntrk = 0;
	int syscont,running,needed,type,stwh,part;
	BOOL *startCommand,startFlg;
	BOOL *endCommand,endFlg;
   
	nseq = nfiles;
	vUpSeq = new UINT[nfiles];
	nTrack = new UINT[nfiles];
	division = new UINT[nfiles];
	Tempo = new float[nfiles];
	seqInitial = new int[nfiles];
	seqStartCommand = new BOOL[nfiles];
	seqEndCommand = new BOOL[nfiles];
	seqFIPCommand = new BOOL[nfiles];
	seqFOPCommand = new BOOL[nfiles];
	seqTimerCommand = new BOOL[nfiles];
	seqSirenCommand = new int[nfiles];
	seqStartPoint = new UINT[nfiles];
	seqEndPoint = new UINT[nfiles];
	seqStartData = new LPSTR[nfiles];
	seqEndData = new LPSTR[nfiles];
	seqFIPData = new LPSTR[nfiles];
	seqFOPData = new LPSTR[nfiles];
	seqOutPort = new int[nfiles];//<0: use default port
	seqChannel = new UINT[nfiles];//channel
	seqGroup = new int[nfiles];//channel group
	seqMid = new int[nfiles];//manufacturer id
	seqPid = new int[nfiles];//product id
//	seqPriority = new int[nfiles];//priority

	startCommand = new BOOL[nfiles];
	endCommand = new BOOL[nfiles];

	CircleMain = GardenMain = nfiles;
	pTimerEntry = new TimerEntry[24][5];
	for(i=0;i<24;i++){
		if(i==11 || i==14 || i==18)
		{
			pTimerEntry[i][CIRCLE_MAIN].flg = TRUE;
			pTimerEntry[i][CIRCLE_SUB].flg = FALSE;
			pTimerEntry[i][GARDEN_MAIN].flg = TRUE;
			pTimerEntry[i][GARDEN_SUB].flg = FALSE;
		}
		else
		{
			pTimerEntry[i][CIRCLE_MAIN].flg = FALSE;
			pTimerEntry[i][CIRCLE_SUB].flg = TRUE;
			pTimerEntry[i][GARDEN_MAIN].flg = FALSE;
			pTimerEntry[i][GARDEN_SUB].flg = TRUE;
		}
		pTimerEntry[i][POLE_MAIN].flg = TRUE;
		for(j=0;j<5;j++){
			pTimerEntry[i][j].track = -1;
		}
	}

	int aa = -1;
#ifdef DEBUG	
	AfxMessageBox("GetFileInfo", MB_OK);
#endif

	char cPath[MAX_PATH];

	if(DirFlg == 0) sprintf(cPath,"\\QUEEN3\\Data");
	else if(DirFlg == 1)sprintf(cPath,"\\QUEEN3\\Demo");
	else sprintf(cPath,"\\QUEEN3\\Init");
	SetCurrentDirectory(cPath);
//////////
#ifdef DEBUG
	AfxMessageBox("pass 1", MB_OK);
#endif

	cdiv = 480;
	ctempo = (float)90.0;

	for(j=0;j<nfiles;j++)
	{
		vUpSeq[j] = (UINT)0;
		seqOutPort[j] = -1;//
		seqChannel[j] = 0;//
		seqGroup[j] = 0x00000000;//
		seqInitial[j] = 0;
		seqStartCommand[j] = seqEndCommand[j] = FALSE;
		seqFIPCommand[j] = seqFOPCommand[j] = FALSE;
		seqTimerCommand[j] = FALSE;
		seqSirenCommand[j] = 0;
		seqStartPoint[j] = seqEndPoint[j] = (UINT)0;
		seqStartData[j] = seqEndData[j] = seqFIPData[j] = seqFOPData[j] = (LPSTR)NULL;
		startCommand[j] = endCommand[j] = TRUE;

		division[j] = cdiv;
		Tempo[j] = ctempo;

        lstrcpy((char *)b,path[j]);

		if((fp=fopen((char *)b,"rb"))==NULL) 
		{
			fclose(fp);
			wsprintf(text, "ファイル%sがオープンできませんでした",b);
			AfxMessageBox(text, MB_OK);
			return; // abnormal
		}
		else
		{
			fseek(fp,0L,SEEK_END);
			fsize = ftell(fp);
			fseek(fp,0L,SEEK_SET);
			bsize += fsize;
		}
		if(fread(b,1,4,fp) != 4) 
		{
			fclose(fp);
			wsprintf(text, "ファイル%sの大きさが足りませんでした",b);
			AfxMessageBox(text, MB_OK);
			return; // abnormal
		}

		if(memcmp(b,"MThd",4)) 
		{
			fclose(fp);
			wsprintf(text, "ファイル%sのMThdが見つかりませんでした",b);
			AfxMessageBox(text, MB_OK);
			return; // abnormal
		}

		fread(b,1,4,fp);
		rp = b;
        size=(long)Skip32(&rp,NULL);
		fread(b,1,2,fp);
		rp = b;
		format = Skip16(&rp,NULL);

		fread(b,1,2,fp);
		rp = b;
		tracks = Skip16(&rp,NULL);
		nTrack[j] = tracks;
		ntrk += tracks;

		fread(b,1,2,fp);
		rp = b;
		division[j] = cdiv = Skip16(&rp,NULL);
		fclose(fp);
	}//end for j

	seqPlayTime = new QUEEN_play_time[ntrk];
	trackEnv = new UINT[ntrk];
	pTopTrack = ptoptrack = new LPSTR[ntrk];
	pBottomTrack = pbottomtrack = new LPSTR[ntrk];
	pTrack = ptrack = new LPSTR[ntrk];
	trackSize = ptsize = new UINT[ntrk];
	trackStartTime = new UINT[ntrk];
	trackTics = new UINT[ntrk];
	trackEndFlg = new BOOL[ntrk];
	trackStatus = new unsigned char[ntrk];
	rbase = rp = new unsigned char[(UINT)bsize];
	pbuffer = (LPSTR)rbase;

	for(j=0,tr=trk=0;j<nfiles;tr+=nTrack[j],j++)
	{
#ifdef DEBUG
	AfxMessageBox("pass 2-1", MB_OK);
#endif
		currentSeq = j;
        lstrcpy((char *)b,path[j]);

		fp=fopen((char *)b,"rb");
		fseek(fp,0L,SEEK_END);
		fsize = ftell(fp);
		fseek(fp,12L,SEEK_SET);
		fread(rp,1,(size_t)(fsize-12L),fp);
		fclose(fp);

		cdiv = Skip16(&rp,NULL);
#ifdef DEBUG
	AfxMessageBox("pass 2-2", MB_OK);
#endif
		for(trk=tr;trk<tr+nTrack[j];trk++) 
		{
			seqPlayTime[trk].type = TIMER_UNDEF;
			seqPlayTime[trk].status = TIMER_WAIT;
			trackEnv[trk] = 0;
			trackTics[trk] = 0;
		}
		for(trk=tr;trk<tr+nTrack[j];trk++) 
		{
#ifdef DEBUG
	AfxMessageBox("pass 2-3", MB_OK);
#endif
			currentTrack = trk;
			if(fsize - 14 < 4 || memcmp(rp,"MTrk",4)) 
			{
				wsprintf(text, "ファイル%sの%d番目のトラックでMTrkが見つかりませんでした",b,tr+1);
				AfxMessageBox(text, MB_OK);
				return; // normal
			}
			else rp+=4;
			
			fsize = (long)Skip32(&rp,&size);
			size = fsize;
			*ptsize++ = (UINT)size;
			*ptrack++ = *ptoptrack++ = (LPSTR)rp;
			*pbottomtrack++ = (LPSTR)(rp + (UINT)size - 1);
			wp = rp;
			syscont = status = running = 0;
#ifdef DEBUG
	AfxMessageBox("pass 2-4", MB_OK);
#endif
			while(rp < wp + (UINT)fsize)
			{
				trackTics[trk] += SkipDeltaT(&rp,&size); //イベント開始tick
				dp = rp;                      //開始処理・終了処理用ポインタ
				c=(unsigned char)Skip8(&rp,&size);   //先頭MIDIコード

				if(syscont && c != 0xf7) break;
				if((c & 0x80)==0x0) 
				{
					if(!status) break;
					else running=1;
				}
				else 
				{
					status = c;
					running = 0;
				}

				needed=chantype[(status>>4) & 0x0f];


				if(needed) 
				{		
					if(status){
						seqChannel[j] |= (UINT)incTable[(status & 0x0f)];//channel;
//						TRACE("%08x\n",seqChannel[j]);
					}

					if(running) c1 = c;
					else c1 = Skip8(&rp,&size);
					if(needed > 1) c2=Skip8(&rp,&size);
					else c2=0x0;
					//START処理データ時
					if(seqStartCommand[j] && startCommand[j])
					{
						tc = trackTics[trk];
						startCommand[j] = FALSE;
						cp = (LPSTR)dp;
					}
					//END処理データ時
					if(seqEndCommand[j] && endCommand[j])
					{
						tc = trackTics[trk];
						endCommand[j] = FALSE;
						cp = (LPSTR)dp;
					}
				}
				else
				{
					switch(c) 
					{
					case 0xff:
						type=Skip8(&rp,&size);
						lookfor = SkipVari(&rp,&size);
						lookfor = size - lookfor;
						b[0]=0;
						i=0;
						while(size > (long)lookfor) 
						{
							c=Skip8(&rp,&size);
							if(i < STRINGSIZE) b[i++]=c;
						}
						b[i]=0;
						AddMeta1((LPSTR)b,type);///AddMeta1
						if((trackEnv[trk] & START_COMMAND_TRACK)!=0)
						{
							seqStartPoint[j] = tc;
							seqStartData[j] = cp;
						}
						else if((trackEnv[trk] & END_COMMAND_TRACK)!=0)
						{
							seqEndPoint[j] = tc;
							seqEndData[j] = cp;
						}
						break;
					case 0xf0:
						lookfor = SkipDeltaT(&rp,&size);
						lookfor = size - lookfor;
						while(size > (long)lookfor) c=Skip8(&rp,&size);
						if(c!=0xf7) syscont = 1;
						break;
					case 0xf7:
						lookfor = SkipVari(&rp,&size);
						lookfor = size - lookfor;
						while(size > (long)lookfor) c=Skip8(&rp,&size);
						if(syscont && c ==0xf7) syscont=0;
						break;
					default:
						size = -1L;
						rp++;
						size--;
						break;
					}
				}
			}
			rp = wp + (UINT)fsize;

		} // tr < tracks

	} // j < nfiles

#ifdef DEBUG
	AfxMessageBox("pass 3", MB_OK);
#endif
	for(j=0,trk=0;j<nfiles;j++)
	{
		if(seqSirenCommand[j] > 0)
		{
		    if(seqGroup[j]==1
				&& (CircleMain == nseq || seqSirenCommand[CircleMain] > seqSirenCommand[j]))
			{//circle
				CircleMain=j;
			}else if(seqGroup[j]==3
				&& (GardenMain == nseq || seqSirenCommand[GardenMain] > seqSirenCommand[j]))
			{//garden
				GardenMain=j;
			}
		}
	}
	for(j=0,trk=0;j<nfiles;j++)
	{
		for(tr=0;tr<nTrack[j];tr++,trk++) 
		{
			if(seqSirenCommand[j] > 0
			&& (trackEnv[trk] & TIMER_START_TRACK) != 0)
			{
				//make timer entry table
				///seqSirenCommand ?
				//main or sub ?
				part = -1;
				if(seqGroup[j]==1){//circle
					if(j==CircleMain) part = CIRCLE_MAIN;
					else part=CIRCLE_SUB;
					//sub ??
				}else if(seqGroup[j]==2){//pole
					part = POLE_MAIN;
				}else if(seqGroup[j]==3){//garden
					if(j==GardenMain) part = GARDEN_MAIN;
					else part=GARDEN_SUB;
					//sub??
				}else{
					TRACE("No Group\n");
				}
				if(part >=0){
					stwh=seqPlayTime[trk].rt.wHour;
					pTimerEntry[stwh][part].track = trk;
					pTimerEntry[stwh][part].seq = j;
				}
			}
		}
	}
	for(j=0,trk=0;j<nfiles;j++)
	{
		currentSeq = j;
		seqChannel[j] = seqChannel[j] << (16 * seqOutPort[j]);
		for(tr=0;tr<nTrack[j];tr++,trk++) 
		{
			currentTrack = trk;
			startFlg = endFlg = TRUE;
			trackTics[trk] = 0;
			pTrack[trk] = pTopTrack[trk];
			rp = (unsigned char *)pTrack[trk];
			fsize = trackSize[trk];
			syscont = status = running = 0;
			while(rp < (unsigned char *)(pTrack[trk] + (UINT)fsize))
			{
				wp = rp;
				trackTics[trk] += SkipDeltaT(&rp,&size); //イベント開始tick
				c=(unsigned char)Skip8(&rp,&size);   //先頭MIDIコード

				if(syscont && c != 0xf7) break;
				if((c & 0x80)==0x0) 
				{
					if(!status) break;
					else running=1;
				}
				else 
				{
					status = c;
					running = 0;
				}

				needed=chantype[(status>>4) & 0x0f];

				if(needed) 
				{		
					if(running) c1 = c;
					else c1 = Skip8(&rp,&size);
					if(needed > 1) c2=Skip8(&rp,&size);
					else c2=0x0;
				}
				else
				{
					switch(c) 
					{
					case 0xff:
						type=Skip8(&rp,&size);
						lookfor = SkipVari(&rp,&size);
						lookfor = size - lookfor;
						b[0]=0;
						i=0;
						while(size > (long)lookfor) 
						{
							c=Skip8(&rp,&size);
							if(i < STRINGSIZE) b[i++]=c;
						}
						b[i]=0;
						break;
					case 0xf0:
						lookfor = SkipDeltaT(&rp,&size);
						lookfor = size - lookfor;
						while(size > (long)lookfor) c=Skip8(&rp,&size);
						if(c!=0xf7) syscont = 1;
						break;
					case 0xf7:
						lookfor = SkipVari(&rp,&size);
						lookfor = size - lookfor;
						while(size > (long)lookfor) c=Skip8(&rp,&size);
						if(syscont && c ==0xf7) syscont=0;
						break;
					default:
						size = -1L;
						rp++;
						size--;
						break;
					}
				}
			}
			rp = wp + (UINT)fsize;
			trackSize[trk] = (UINT)(pBottomTrack[trk] - pTopTrack[trk] + 1);

		} // tr < tracks

	} // j < nfiles

	delete []startCommand;
	delete []endCommand;
	return; // normal

}

void Cqueens3mid::playinit()
{
	UINT i,j,k,trk;

	currentTime = startTime = (UINT)timeGetTime();
	GetLocalTime(&startSysTime);
	if(DebugFlg)
	{
		cUpSeq = 1;
		vUpSeq[0]=nseq;
	}
	else
	{
		for(i=0;i<nseq;i++) //
		{
			if(seqInitial[i]==1)
			{
				vUpSeq[0]=i;
				cUpSeq=1;
				break;
			}
		}
	}
	for(i=trk=0;i<nseq;i++)
	{
		for(j=0;j<nTrack[i];j++,trk++)
		{
			pTrack[trk] = pTopTrack[trk];
			trackTics[trk] = 0;
			trackEndFlg[trk] = TRUE;
			trackStatus[trk] = 0;
			if((trackEnv[trk] & (ACTIVE_TRACK | FIP_COMMAND_TRACK | END_COMMAND_TRACK))!=0
				&& (trackEnv[trk] & FOP_COMMAND_TRACK)==0)
			{
				for(k=0;k<cUpSeq;k++)
					if(i == vUpSeq[k])
				{
					trackStartTime[trk] = currentTime;
					trackEndFlg[trk] = FALSE;
					if((trackEnv[trk] & LOOP)!=0) loopFlg = TRUE;
					break;
				}
			}
		}
	}
	start = TRUE;
	return;

}

void Cqueens3mid::playResume()
{
	UINT i,j,trk;

	currentTime = (UINT)timeGetTime();
	for(i=trk=0;i<nseq;i++)
		for(j=0;j<nTrack[i];j++,trk++)
		{
			trackTics[trk] = 0;
			trackStartTime[trk] = currentTime;
			trackEndFlg[trk] = FALSE;
		}

	start = TRUE;
	return;

}

unsigned long Cqueens3mid::Skip32(unsigned char **rp,long *size)
{
	unsigned long wk;
	if(size != NULL) *size-=4;
	wk = (unsigned long)(*(*rp)++) << 24;
	wk += (unsigned long)(*(*rp)++) << 16;
	wk += (unsigned long)(*(*rp)++) << 8;
	wk += (unsigned long)(*(*rp)++) ;
	//return(((unsigned long)(*(*rp)++) << 24) +
	//       ((unsigned long)(*(*rp)++) << 16) +
	//       ((unsigned long)(*(*rp)++) << 8) +
	//       ((unsigned long)(*(*rp)++) ));
	return(wk);
}

UINT Cqueens3mid::Skip16(unsigned char **rp,long *size)
{
	UINT wk;
	if(size != NULL) *size-=2;
	wk = (UINT)(*(*rp)++) << 8;
	wk += (UINT)(*(*rp)++) ;
	//return(((UINT)(*(*rp)++) << 8 ) + (UINT)(*(*rp)++));
	return(wk);
}

UINT Cqueens3mid::Skip8(unsigned char **rp,long *size)
{
	UINT wk;
	if(size != NULL) *size-=1;
    wk = (UINT)(*(*rp)++);
    //return((UINT)(*(*rp)++));
    return(wk);
}

unsigned long Cqueens3mid::SkipVari(unsigned char **rp,long *size)
{
	unsigned long value;
	unsigned char c;

	c = *(*rp)++;
	if(size != NULL) *size-=1;
	value = (unsigned long)c & 0xffL;
	if(c & 0x80) {
		value &= 0x7fL;
		do {
			c = *(*rp)++;
			if(size != NULL) *size-=1;
			value=(value << 7)+(unsigned long)(c & 0x7f);
		} while(c & 0x80);
	}
	return(value);

}

UINT Cqueens3mid::Read8(unsigned char **rp,long *size, unsigned char **wp)
{
	UINT wk;
	if(size != NULL) *size-=1;
	wk = (UINT)(*(*wp)++ = *(*rp)++) & 0xff;
    //    return((UINT)(*(*wp)++ = *(*rp)++) & 0xff);
    return(wk);
}

UINT Cqueens3mid::Read16(unsigned char **rp,long *size, unsigned char **wp)
{
	UINT wk;
	if(size != NULL) *size-=2;
	wk = (unsigned long)(*(*wp)++ = *(*rp)++) << 8;
	wk += (unsigned long)(*(*wp)++ = *(*rp)++) ;
	//return((((UINT)((*(*wp)++ = *(*rp)++)) <<8) 
	//	+ (UINT)(*(*wp)++ = *(*rp)++)));
	return(wk);
}

unsigned long Cqueens3mid::Read32(unsigned char **rp,long *size, unsigned char **wp)
{
	unsigned long wk;
	if(size != NULL) *size-=4;
	wk = (unsigned long)(*(*wp)++ = *(*rp)++) << 24;
	wk += (unsigned long)(*(*wp)++ = *(*rp)++) << 16;
	wk += (unsigned long)(*(*wp)++ = *(*rp)++) << 8;
	wk += (unsigned long)(*(*wp)++ = *(*rp)++) ;
//	return(((unsigned long)(*(*wp)++ = *(*rp)++) << 24) +
//	       ((unsigned long)(*(*wp)++ = *(*rp)++) << 16) +
//	       ((unsigned long)(*(*wp)++ = *(*rp)++) << 8) +
//	       ((unsigned long)(*(*wp)++ = *(*rp)++) ));
	return(wk);
}


unsigned long Cqueens3mid::SkipDeltaT(unsigned char **rp,long *size)
{
	unsigned long lret;

	lret = SkipVari(rp,size);
	if (lret==0L)
	{
	
	}
	else
	{
	}
	return(lret);

}

unsigned long Cqueens3mid::ReadDeltaT(unsigned char **rp,long *size, unsigned char **wp)
{
	unsigned long lret;

	lret = ReadVari(rp,size,wp);
	if (lret==0L)
	{
	
	}
	else
	{
	}
	return(lret);
}
unsigned long Cqueens3mid::ReadVari(unsigned char **rp,long *size, unsigned char **wp)
{
	unsigned long value;
	unsigned char c;

	c = *(*wp)++ = *(*rp)++;
	if(size != NULL) *size-=1;
	value = (unsigned long)c & 0xffL;
	if(c & 0x80) {
		value &= 0x7fL;
		do {
			c = *(*wp)++ = *(*rp)++;
			if(size != NULL) *size-=1;
			value=(value << 7)+(unsigned long)(c & 0x7f);
		} while(c & 0x80);
	}
	return(value);
}

BOOL Cqueens3mid::GetTimeFromString(char *msg,SYSTEMTIME *st)
{
	int i,j,k,h,m,s,n,c,d;
	BOOL flg,hflg,mflg,sflg,nflg,cflg,ret;
	char b[STRINGSIZE+1],tm[4];

	if(strcspn(msg,"0123456789:.* ")==0)
	{
		ret = TRUE;
		for(i=j=0;i<(int)strlen(msg);i++)
		{
			if(msg[i]!=' ') b[j++]=msg[i];
		}
		k=(int)strlen(b);
		d=0;
		for(i=0;i<k;i++)
		{
			if(i>2) break;
			if(b[i]=='*')
			{
				for(j=0;j<i;j++)
				{
					tm[j]=b[j];
				}
				tm[i]=NULL;
				d=atoi(tm);
				for(j=0;j<k;j++)
				{
					b[j]=b[j+i+1];
				}
				break;
			}
		}

		b[j]=0x00;
				//許される文字列
				// 9
				// 99
				// 9.9
				// 9:9
				// 9.99
				// 99.9
				// 9:99
				// 99:9
				// 9.999
				// 99.99
				// 9:9.9
				// 9:9:9
				// 99:99
				// 99.999
				// 9:9.99
				// 9:99.9
				// 99:9.9
				// 9:9:99
				// 9:99:9
				// 99:9:9
				// 9:9.999
				// 9:99.99
				// 99:9.99
				// 99:99.9
				// 9:9:9.9
				// 9:99:99
				// 99:9:99
				// 99:99:9
				// 9:99.999
				// 99:9.999
				// 99:99.99
				// 9:9:9.99
				// 9:9:99.9
				// 9:99:9.9
				// 99:9:9.9
				// 99:99:99
				// 99:99.999
				// 9:9:9.999
				// 9:9:99.99
				// 9:99:9.99
				// 99:9:9.99
				// 9:99:99.9
				// 99:9:99.9
				// 99:99:9.9
				// 9:9:99.999
				// 9:99:9.999
				// 99:9:9.999
				// 9:99:99.99
				// 99:9:99.99
				// 99:99:9.99
				// 99:99:99.9
				// 9:99:99.999
				// 99:9:99.999
				// 99:99:9.999
				// 99:99:99.99
				// 99:99:99.999
		tm[0]=NULL;
		h=m=s=n=j=c=0;
		hflg=mflg=sflg=nflg=cflg=flg=FALSE;
		for(i=0;i<(int)strlen(b);i++)
		{
			if(isdigit(b[i]))
			{
				if(sflg)
				{
					if(tm[0]!=NULL)
					{
						if(tm[1]!=NULL)
						{
							tm[2]=b[i];
							tm[3]=NULL;
							n=atoi(tm);
							nflg=TRUE;
						}
						else
						{
							tm[1]=b[i];
							tm[2]=NULL;
						}
					}
					else
					{
						tm[0]=b[i];
						tm[1]=NULL;
					}
				}
				else if(mflg)
				{
					if(tm[0]!=NULL)
					{
						tm[1]=b[i];
						tm[2]=NULL;
						s=atoi(tm);
						sflg=TRUE;
						tm[0]=NULL;
					}
					else
					{
						tm[0]=b[i];
						tm[1]=NULL;
					}
				}
				else if(hflg)
				{
					if(tm[0]!=NULL)
					{
						tm[1]=b[i];
						tm[2]=NULL;
						m=atoi(tm);
						mflg=TRUE;
						tm[0]=NULL;
					}
					else
					{
						tm[0]=b[i];
						tm[1]=NULL;
					}
				}
				else
				{
					if(tm[0]!=NULL)
					{
						tm[1]=b[i];
						tm[2]=NULL;
						j=atoi(tm);
						flg=TRUE;
						tm[0]=NULL;
					}
					else
					{
						tm[0]=b[i];
						tm[1]=NULL;
					}
				}
			}
			else if(b[i]==':')
			{
				if(cflg) //２つ目のコロン
				{
					h=c;
					hflg=mflg=TRUE;
					if(flg)
					{
						m=j;
						flg=FALSE;
					}
					else if(tm[0]!=NULL)
					{
						m=atoi(tm);
						tm[0]=NULL;
					}
					else m=0;//これはありえない
				}
				else //１つ目のコロン
				{
					cflg=TRUE;
					if(flg)
					{
						c=j;
						flg=FALSE;
						if(tm[0]!=NULL)//これはありえない
						{
							j=atoi(tm);
							flg=TRUE; //jに数字が入っている
							tm[0]=NULL;
						}
					}
					else if(tm[0]!=NULL)
					{
						c=atoi(tm);
						tm[0]=NULL;
					}
					else c=0;//これはありえない
				}		
			}
			else if(b[i]=='.')
			{
				if(!sflg)
				{
					sflg=TRUE;
					if(cflg)
					{
						if(!mflg)
						{
							m=c;
							mflg=TRUE;
						}
						if(flg)
						{
							s=j;
							flg=FALSE;
						}
						else if(tm[0]!=NULL)
						{
							s=atoi(tm);
							tm[0]=NULL;
						}
						else s=0;//これはありえない
					}
					else if(tm[0]!=NULL)
					{
						if(flg) //これはありえない
						{
							m=j;
							mflg=TRUE;
							flg=FALSE;
						}
						s=atoi(tm);
						tm[0]=NULL;
					}
					else if(flg)
					{
						flg=FALSE;
						s=j;
					}
					else s=0;//これはありえない
				}
			}//else if
		}//for
		if(tm[0]!=NULL)
		{
			if(sflg) //ミリ秒の左詰
			{
				if(tm[1]!=NULL) k=1;
				else if(tm[2]!=NULL) k=2;
				for(;k<3;k++) tm[k]='0';
				tm[3]=NULL;
			}
			j=atoi(tm);
			flg=TRUE;
			tm[0]=NULL;
		}
		if(cflg && !mflg && flg)
		{
			h=c;
			m=j;
			hflg=mflg=TRUE;
			flg=FALSE;
		}
		else if(flg)
		{
			if(sflg)
			{
				nflg=TRUE;
				n=j;
			}
			else if(mflg)
			{
				s=j;
				sflg=TRUE;
			}
			else if(hflg)
			{
				mflg=TRUE;
				m=j;
			}
			else
			{
				h=j;
				hflg=TRUE;
			}
		}
		st->wDay=(WORD)d;
		st->wHour=(WORD)h;
		st->wMinute=(WORD)m;
		st->wSecond=(WORD)s;
		st->wMilliseconds=(WORD)n;
	}
	else ret = FALSE;

	return(ret);
}

void Cqueens3mid::AddMeta1(LPSTR msg,unsigned int type)
{

	UINT i,j,tr,p,te,k;
	BOOL flg;
	char timers[3],jihou[3],initseq[3];
//	char mnfcid[4], prodid[4],priority[3];
	char mnfcid[4], prodid[4];
	char b[STRINGSIZE+1],*wp;
	SYSTEMTIME st;
	int part;//for timer table
	static char *envString[NUM_ENV]={
		ENV_ONE_SHOT        ,
		ENV_LOOP            ,
		ENV_TEMPERATURE_MODE,
		ENV_CONTINUE_PLAY   ,
		ENV_START_OPTION    ,
		ENV_FADE_IN_OPTION  ,
		ENV_END_OPTION      ,
		ENV_FADE_OUT_OPTION ,
		ENV_TO_END_OPTION   ,
		ENV_NO_RAIN         ,
		ENV_TIDE_MODE
	};

	static UINT envValue[NUM_ENV]={
		ONE_SHOT         ,
		LOOP             ,
		TEMPERATURE_MODE ,
		CONTINUE_PLAY    ,
		START_OPTION     ,
		FADE_IN_OPTION   ,
		END_OPTION       ,
		FADE_OUT_OPTION  ,
		TO_END_OPTION    ,
		NO_RAIN          ,
		TIDE_MODE
	};

	static char *commandString[NUM_COMMAND]={
		START_DATA    ,
		END_DATA      ,
		FADE_IN_DATA  ,
		FADE_OUT_DATA ,
	};

	static char *timerString[NUM_TIMER]={
		TIMER_START   ,
		TIMER_END     
	};
		
	switch(type) {
		case 0x00:
			//wsprintf(b,"SEQUENCE NUMBER:\t %u\n",Get16(msg));
		case 0x20:
			//wsprintf(b,"CHANNEL PREFIX:\t %u\n",Get16(msg));
			//sendMIDILong(msg,5);
			break;
		case 0x01:	
			//wsprintf(b,"TEXT:\t\t\t %s\n",(LPSTR)msg);
 		case 0x02:	
			//wsprintf(b,"COPYRIGHT:\t %s\n",(LPSTR)msg);
			break;
  		case 0x03:
			for(i=0,flg=FALSE;i<strlen(msg);i++)
			{
				if((b[i]=msg[i])=='/' && i < strlen(msg)-1 && isdigit((int)msg[i+1])) 
				{
					b[i]=NULL;
					for(j=1;j<strlen(msg)-i;j++)
					{
						if(isdigit((int)msg[i+j]))
						{
							b[i+j]=msg[i+j];
						}
						else
						{
							b[i+j]=NULL;
							p = atoi(&b[i+1]);
							wp = &msg[i+j];
							flg = TRUE;
							break;
						}
					}
					break;
				}
			}
			if(flg)
			{
				for(i=0,flg=FALSE;i<NUM_TIMER;i++)
				{
					if(strstr(wp,timerString[i]))
					{
						switch(i)
						{
						case TIMER_START_COMMAND:
							te = TIMER_START_TRACK;
							break;
						case TIMER_END_COMMAND:
							te = TIMER_END_TRACK;
							break;
						}
						flg=TRUE;
						break;
					}
				}
				if(flg && seqPlayTime[currentTrack].type == TIMER_UNDEF)
				{
					if(GetTimeFromString(b,&st))
					{
						seqPlayTime[currentTrack].type = TIMER_DEFINED;
						seqPlayTime[currentTrack].pair = p;
						seqPlayTime[currentTrack].rt.wDay=st.wDay;
						seqPlayTime[currentTrack].rt.wHour=st.wHour;
						seqPlayTime[currentTrack].rt.wMinute=st.wMinute;
						seqPlayTime[currentTrack].rt.wSecond=st.wSecond;
						seqPlayTime[currentTrack].rt.wMilliseconds=st.wMilliseconds;
						seqPlayTime[currentTrack].time=(UINT)(st.wHour * 3600000L 
							+ st.wMinute * 60000L + st.wSecond * 1000L + st.wMilliseconds);
						trackEnv[currentTrack] |= te;
					}
				}
			}
			else
			{
				for(i=0;i<NUM_ENV;i++)
				{
					if(strstr(msg,envString[i]))
					{
						trackEnv[currentTrack] |= (ACTIVE_TRACK | envValue[i]);
					}
				}
				if((trackEnv[currentTrack] & TEMPERATURE_MODE)!=0)
				{
					for(i=tr=0;i<(UINT)currentSeq;i++)
						tr+=nTrack[i];
					for(i=tr;i<tr+nTrack[currentSeq];i++)
						if((trackEnv[i] & ACTIVE_TRACK)==0
						&& (trackEnv[i] & (FIP_COMMAND_TRACK | FOP_COMMAND_TRACK))!=0 )
							trackEnv[i] |= TEMPERATURE_MODE;
				}
				for(i=0;i<NUM_COMMAND;i++)
				{
					if(strstr(msg,commandString[i]))
					{
						switch(i)
						{
						case START_DATA_COMMAND:
							seqStartCommand[currentSeq] = TRUE;
							trackEnv[currentTrack] |= START_COMMAND_TRACK;
							break;
						case END_DATA_COMMAND:
							seqEndCommand[currentSeq] = TRUE;
							trackEnv[currentTrack] |= END_COMMAND_TRACK;
							break;
						case FADE_IN_DATA_COMMAND:
							seqFIPCommand[currentSeq] = TRUE;
							trackEnv[currentTrack] |= FIP_COMMAND_TRACK;
							seqFIPData[currentSeq] = pTopTrack[currentTrack];
							for(j=tr=0;j<(UINT)currentSeq;j++)
								tr+=nTrack[j];
							for(j=tr;j<tr+nTrack[currentSeq];j++)
								if((trackEnv[j] & ACTIVE_TRACK)!=0
								&& (trackEnv[j] & TEMPERATURE_MODE)!=0)
								{
									trackEnv[currentTrack] |= TEMPERATURE_MODE;
									break;
								}
							break;
						case FADE_OUT_DATA_COMMAND:
							seqFOPCommand[currentSeq] = TRUE;
							trackEnv[currentTrack] |= FOP_COMMAND_TRACK;
							seqFOPData[currentSeq] = pTopTrack[currentTrack];
							for(j=tr=0;j<(UINT)currentSeq;j++)
								tr+=nTrack[j];
							for(j=tr;j<tr+nTrack[currentSeq];j++)
								if((trackEnv[j] & ACTIVE_TRACK)!=0
								&& (trackEnv[j] & TEMPERATURE_MODE)!=0)
								{
									trackEnv[currentTrack] |= TEMPERATURE_MODE;
									break;
								}
							break;
						}
					}
				}
			}
			//wsprintf(b,"SEQUENCE:\t\t %s\n",(LPSTR)msg);
			//pDC->TextOut(0,90,b);
			break;
		case 0x04:
			timers[0]=NULL;
			jihou[0]=NULL;
			initseq[0]=NULL;
			mnfcid[0]=NULL;
			prodid[0]=NULL;
//			priority[0]=NULL;
			for(i=j=0,flg=FALSE;i<strlen(msg);i++) //コメント部分の読み飛ばし
			{
				if(flg)
				{
					if(msg[i]=='/') flg=FALSE;
				}
				else if(msg[i]=='/') flg=TRUE;
				else b[j++]=msg[i];
			}
			b[j]=0x00;
			for(i=0;i<(int)strlen(msg);i++)
			{
				if(b[i]=='T' && isdigit((int)b[i+1]))
				{
					j=0;
					while(isdigit((int)b[i+j+1]) && j<2)
					{
						timers[j]=b[i+j+1];
						j++;
					}
					timers[j]=NULL;
				}
				else if(b[i]=='J' && isdigit((int)b[i+1]))
				{
					j=0;
					while(isdigit((int)b[i+j+1]) && j<2)
					{
						jihou[j]=b[i+j+1];
						j++;
					}
					jihou[j]=NULL;
				}
				else if(b[i]=='I' && isdigit((int)b[i+1]))
				{
					j=0;
					while(isdigit((int)b[i+j+1]) && j<2)
					{
						initseq[j]=b[i+j+1];
						j++;
					}
					initseq[j]=NULL;
				}
				else if(b[i]=='C')
				{
					j=0;
					while(isdigit((int)b[i+j+1]) && j<3)
					{
						mnfcid[j]=b[i+j+1];
						j++;
					}
					mnfcid[j]=NULL;
				}
				else if(b[i]=='P')
				{
					j=0;
					while(isdigit((int)b[i+j+1]) && j<3)
					{
						prodid[j]=b[i+j+1];
						j++;
					}
					prodid[j]=NULL;
				}
//				else if(b[i]=='L')
//				{
//					j=0;
//					while(isdigit((int)b[i+j+1]) && j<2)
//					{
//						priority[j]=b[i+j+1];
//						j++;
//					}
//					priority[j]=NULL;
//				}
			}
			if(strlen(timers))
			{
				seqTimerCommand[currentSeq]=TRUE;
				trackEnv[currentTrack] |= ACTIVE_TRACK;
			}
			if(strlen(jihou))
			{
				trackEnv[currentTrack] |= ACTIVE_TRACK;
				k=atoi(jihou);
				seqSirenCommand[currentSeq]=k;
			}
			if(strlen(initseq))
			{
				seqInitial[currentSeq]=atoi(initseq);
				trackEnv[currentTrack] |= ACTIVE_TRACK;
			}
			if(strlen(mnfcid)&&strlen(prodid))
			{
				seqMid[currentSeq]=atoi(mnfcid);
				seqPid[currentSeq]=atoi(prodid);
				seqOutPort[currentSeq]=
					OpenMidiOutSelect(seqMid[currentSeq], seqPid[currentSeq]);

				seqGroup[currentSeq] = GetSeqGroup(currentSeq);
				//set seqGroup
				//TRACE("%d 0x%8x\n",currentSeq, seqGroup[currentSeq]);
			}
//			if(strlen(priority))
//			{
//				seqPriority[currentSeq]=atoi(priority);
//			}else if(seqSirenCommand[currentSeq] > 0){
//				seqPriority[currentSeq]=(int)10;
//			}else{
//				seqPriority[currentSeq]=(int)20;
//			}
			//wsprintf(b,"INSTRUMENT:\t\t %s\n",(LPSTR)msg);
			//pDC->TextOut(0,120,b);
			break;
		case 0x05:
			//wsprintf(b,"LYRIC:\t %s\n",(LPSTR)msg);
		case 0x06:
			//wsprintf(b,"MARKER:\t %s\n",(LPSTR)msg);
		case 0x07:
			//wsprintf(b,"CUE POINT:\t %s\n",(LPSTR)msg);
		case 0x2f:
			//wsprintf(b,"END OF TRACK\n");
			//ShowMIDIMessage(0xFF,(unsigned char *)"\x2F\x00");
			break;
		case 0x51:
			//wsprintf(b,"TEMPO:\t\t\t %lu\n",Get24(msg));
			//sendMIDILong(msg,6);
			Tempo[currentSeq] = (float)(Get24(msg)/1000.0);
			break;
		case 0x54:
			//sendMIDILong(msg,8);
			break;
		case 0x58:
			//wsprintf(b,"TIME SIGNATURE:\t\t %u/%u\n",msg[1],msg[0]);
			break;
		case 0x59:
			 //sprintf(b,"KEY SIGNATURE:\t\t %u - %s\n",msg[0],mm[msg[1] & 0x0001]);
			 break;
		case 0x7f:
			break;
	}
        //Addline(p,b);
}

void Cqueens3mid::AddMeta2(LPSTR msg,unsigned int type)
{
	//char b[2*STRINGSIZE+1];
	//CDC* pDC;

	//pDC = View->GetDC();
    //char mm[2][6]={"MAJOR","MINOR"};


	switch(type) {
		case 0x00:
			//wsprintf(b,"SEQUENCE NUMBER:\t %u\n",Get16(msg));
		case 0x20:
			//wsprintf(b,"CHANNEL PREFIX:\t %u\n",Get16(msg));
			//sendMIDILong(msg,5);
			break;
		case 0x01:	
			//wsprintf(b,"TEXT:\t\t\t %s\n",(LPSTR)msg);
 		case 0x02:	
			//wsprintf(b,"COPYRIGHT:\t %s\n",(LPSTR)msg);
  		case 0x03:

			break;
		case 0x04:
			//seqCond[currentSeq] = msg;
			//wsprintf(b,"INSTRUMENT:\t\t %s\n",(LPSTR)msg);
			//pDC->TextOut(0,120,b);
			break;
		case 0x05:
			//wsprintf(b,"LYRIC:\t %s\n",(LPSTR)msg);
		case 0x06:
			//wsprintf(b,"MARKER:\t %s\n",(LPSTR)msg);
		case 0x07:
			//wsprintf(b,"CUE POINT:\t %s\n",(LPSTR)msg);
		case 0x2f:
			//wsprintf(b,"END OF TRACK\n");
			//ShowMIDIMessage(0xFF,(unsigned char *)"\x2F\x00");
			break;
		case 0x51:
			//wsprintf(b,"TEMPO:\t\t\t %lu\n",Get24(msg));
			//sendMIDILong(msg,6);
			//Tempo[currentSeq] = (float)(Get24(msg)/1000.0);
			break;
		case 0x54:
			//sendMIDILong(msg,8);
			break;
		case 0x58:
			//wsprintf(b,"TIME SIGNATURE:\t\t %u/%u\n",msg[1],msg[0]);
			break;
		case 0x59:
			 //sprintf(b,"KEY SIGNATURE:\t\t %u - %s\n",msg[0],mm[msg[1] & 0x0001]);
			 break;
		case 0x7f:
			break;
	}
        //Addline(p,b);
}

UINT Cqueens3mid::Get16(LPSTR p)
{
	return(((p[0] & 0x00ff) << 8) + (p[1] & 0x00ff));
}

unsigned long Cqueens3mid::Get24(LPSTR p)
{
	return(
		(((unsigned long)(p[0] & 0x00ff)) << 16) +
		(((unsigned long)(p[1] & 0x00ff)) << 8) +
		 ((unsigned long)(p[2] & 0x00ff)));
}

UINT Cqueens3mid::sendMIDIEvent(unsigned char bStatus, 
						   unsigned char bData1, unsigned char bData2)
{
	UINT uiRet;

    union { 
        DWORD dwData; 
        BYTE bData[4]; 
    } u; 
 
    u.bData[0] = bStatus;  // MIDI status byte 
    u.bData[1] = bData1;   // first MIDI data byte 
    u.bData[2] = bData2;   // second MIDI data byte 
    u.bData[3] = 0; 
 
    // Send the message. 

	/*			*/
	/*			*/
	/*			*/
#ifndef DEBUG_PRESET
	if(CurrentPort==DEFAULT_MIDI_OUT_PORT){
		uiRet = midiOutShortMsg(hMidiout[ROLAND_SMPU_MIDI_OUT_A], u.dwData);
		uiRet = midiOutShortMsg(hMidiout[ROLAND_SMPU_MIDI_OUT_B], u.dwData); 
	}else{
		uiRet = midiOutShortMsg(hMidiout[CurrentPort], u.dwData);
	}
	/*			*/
	/*			*/
	/*			*/
	return uiRet;
#else
	return 0;
#endif
} 

UINT Cqueens3mid::sendMIDILong(LPSTR msg,UINT mlength)
{
	MIDIHDR MidiHdr;
	MMRESULT mmresult=0;
	MidiHdr.lpData = msg;
	MidiHdr.dwFlags = 0;
	MidiHdr.dwBufferLength = (DWORD)mlength;;
	/*			*/
	/*			*/
	/*			*/
#ifndef DEBUG_PRESET
	if(CurrentPort==DEFAULT_MIDI_OUT_PORT){
		if((mmresult = midiOutPrepareHeader(hMidiout[ROLAND_SMPU_MIDI_OUT_A]
											,&MidiHdr,sizeof(MidiHdr)))
											== MMSYSERR_NOERROR)
			mmresult = midiOutLongMsg(hMidiout[ROLAND_SMPU_MIDI_OUT_A]
										,&MidiHdr,sizeof(MidiHdr));
		if((mmresult = midiOutPrepareHeader(hMidiout[ROLAND_SMPU_MIDI_OUT_B]
											,&MidiHdr,sizeof(MidiHdr)))
											== MMSYSERR_NOERROR)
			mmresult = midiOutLongMsg(hMidiout[ROLAND_SMPU_MIDI_OUT_B]
										,&MidiHdr,sizeof(MidiHdr));
	}else{
		if((mmresult = midiOutPrepareHeader(hMidiout[CurrentPort],&MidiHdr,sizeof(MidiHdr)))
			== MMSYSERR_NOERROR)
		mmresult = midiOutLongMsg(hMidiout[CurrentPort],&MidiHdr,sizeof(MidiHdr));
	}
#endif
	/*			*/
	/*			*/
	/*			*/
	return((UINT)mmresult);

}

void Cqueens3mid::ShowMIDIMessage(unsigned char c,unsigned char *rp)
{
	CDC *pDC;
	char text[256];

	pDC = View->GetDC();
	wsprintf(text,"send message %x %x %x",(BYTE)c,(BYTE)*rp,(BYTE)*(rp+1));
	pDC->TextOut(0,60,text);

}

UINT Cqueens3mid::OpenMidiOut()
//MM_ROLAND_SMPU_MIDIOUTA
//MM_ROLAND_SMPU_MIDIOUTB
{
	UINT wDeviceID,count;
	MMRESULT result;
	char text[MAXERRORLENGTH+1];
	MIDIOUTCAPS* caps;
	UINT i;
	long j;

	count = midiOutGetNumDevs();
	if(!count) return(MIDIERR_NODEVICE);
	else{
		caps = new MIDIOUTCAPS[count];
	}

//	wDeviceID = MIDI_MAPPER;
//	midiOutGetDevCaps(wDeviceID,&caps[0],sizeof(caps[0]));
//	if((result = midiOutOpen(&hMidiout[0], wDeviceID,
//		(DWORD)NULL, (DWORD)NULL, CALLBACK_NULL)) != MMSYSERR_NOERROR)
//	{
//		midiOutGetErrorText(result,(LPSTR)text,MAXERRORLENGTH); 
//		AfxMessageBox(text, MB_OK);
//	}

	for(i=0;i<count;i++){
		wDeviceID = i;
		midiOutGetDevCaps(wDeviceID,&caps[i],sizeof(caps[i]));

#ifndef DEBUG_PRESET
		j = OpenMidiOutSelect(caps[i].wMid, caps[i].wPid);
		if(j >= 0){
			if((result = midiOutOpen(&hMidiout[j], wDeviceID,
						(DWORD)NULL, (DWORD)NULL, CALLBACK_NULL)) != MMSYSERR_NOERROR)
			{
				midiOutGetErrorText(result,(LPSTR)text,MAXERRORLENGTH); 
				AfxMessageBox(text, MB_OK);
			}
		}
#endif
	}

	delete caps;

	return(0);
}

int Cqueens3mid::OpenMidiOutSelect(int wMid, int wPid)
{
	if(wMid == (int)MM_ROLAND){
		if(wPid == (int)MM_ROLAND_SMPU_MIDIOUTA){
			return ROLAND_SMPU_MIDI_OUT_A;//0
		}else if(wPid == (int)MM_ROLAND_SMPU_MIDIOUTB){
			return ROLAND_SMPU_MIDI_OUT_B;//1
		}else{
			return -1;
		}
	}else{
		return -1;
	}
	return -1;
}

UINT Cqueens3mid::CloseMidiOut()
{
	MMRESULT result;
	char text[MAXERRORLENGTH+1];
	int i;

	/*			*/
	/*			*/
	/*			*/
#ifndef DEBUG_PRESET
	for(i=0;i<2;i++){
		if((result = midiOutReset(hMidiout[i])) != MMSYSERR_NOERROR)
		{
			midiOutGetErrorText(result,(LPSTR)text,MAXERRORLENGTH); 
			AfxMessageBox(text, MB_OK);
		}
	
		if((result = midiOutClose(hMidiout[i])) != MMSYSERR_NOERROR)
		{
			midiOutGetErrorText(result,(LPSTR)text,MAXERRORLENGTH); 
			AfxMessageBox(text, MB_OK);
		}
	}
#endif
	/*			*/
	/*			*/
	/*			*/

	return(0);
}

UINT Cqueens3mid::MidiProc() 
{
	UINT st;
	//SYSTEMTIME tm;
	long size;
	unsigned char *rp,*pos,status,c,c1,c2,cc;
	char b[STRINGSIZE+1];
	UINT i,j,k,k2,kmin,tr,ntr,trk,lookfor,tics;
	//UNIT ds;
    BOOL dflg,allDflg,allSeqEndFlg,allTrackEndFlg,wflg,xflg,aflg,pflg;
	BOOL CheckTempFlg,saveFlg;
	int syscont,running,needed,type;
	BOOL Note[32][128];
	float ftics,fMinWait,fwk;
	int sPort;

	srand((unsigned)time(NULL));

	CheckLoopFlg = new BOOL[nseq];
	for(i=0;i<nseq;i++) CheckLoopFlg[i]=FALSE;

	allSeqEndFlg = allTrackEndFlg = allDflg = CheckTempFlg = FALSE;
	for(i=0;i<32;i++)
	{
		for(j=0;j<128;j++)
		{
			Note[i][j]=FALSE; //ノートオン・オフ管理用配列のリセット
		}
	}

	fMinWait = 10.0;
	while( start) //スレッドの存在
	{
		::Sleep((DWORD)fMinWait);
		fMinWait=1000.0;
		currentTime = (UINT)timeGetTime(); //現在時刻
		//TRACE("midiProc:currentTime ");
		if ( allSeqEndFlg && !allDflg)       //全曲終了
		{
				if(loopFlg)
				for(j=ntr=0,xflg=wflg=FALSE;j<nseq;ntr+=nTrack[j],j++)
				{
					for(k=0;k<cUpSeq;k++)
					{
						if(j==vUpSeq[k])
						{
							xflg=TRUE;
							break;//for
						}
					}///end for(k=0;k<cUpSeq;k++)
					if(xflg)
					{
						xflg=FALSE;
						for(k=ntr;k<ntr+nTrack[j];k++)
						{
							if((trackEnv[k] & ACTIVE_TRACK)!=0
							&& trackEndFlg[k])
							{
								if((trackEnv[k] & END_COMMAND_TRACK)!= 0)
								{
									xflg=TRUE;
								}
							    if( (trackEnv[k] & LOOP)!= 0)
							    {
								    wflg=TRUE;
								}
							}
						}///end for(k=ntr;k<ntr+nTrack[j];k++)
						if(wflg && xflg)
						{
							for(k=ntr;k<ntr+nTrack[j];k++)
							{
								if((trackEnv[k] & ACTIVE_TRACK)!=0
								&& trackEndFlg[k]
							    && (  (trackEnv[k] & LOOP)!= 0
							       || (trackEnv[k] & END_COMMAND_TRACK)!= 0))
								{
									trackStartTime[k] = currentTime;//開始時刻
								    trackStatus[k] = 0;
								    trackEndFlg[k] =  FALSE;//１曲未終了
								}
							}///end for(k=ntr;k<ntr+nTrack[j];k++)
						}///end if(wflg)
					}///end if(xflg)
				}///end if(loopFlg)
				if(	!loopFlg) break;//while
//			}////end if( CheckTempFlg),else
		}
		//全曲終了

		allSeqEndFlg = allDflg = TRUE;
		//TRACE("midiProc:for seq ");
		for(j=ntr=tr=0;j<nseq;tr+=nTrack[j],j++)
		{
			if(CheckLoopFlg[j])
			{
				CheckLoopFlg[j] = FALSE;
				if(loopFlg)
				{
					xflg=wflg=FALSE;
					for(k=0;k<cUpSeq;k++)
					{
						if(j==vUpSeq[k])
						{
							xflg=TRUE;
							break;
						}
					}
					if(xflg)
					{
						xflg=FALSE;
						for(k=tr;k<tr+nTrack[j];k++)
						{
							if((trackEnv[k] & ACTIVE_TRACK)!=0
							&& trackEndFlg[k])
							{
							    if( (trackEnv[k] & LOOP)!= 0)
							    {
								    xflg=TRUE;
							    }
							    if( (trackEnv[k] & END_COMMAND_TRACK)!= 0)
							    {
								    wflg=TRUE;
							    }
						    }
						}
						if(wflg && xflg)
						{
							for(k=tr;k<tr+nTrack[j];k++)
							{
								if((trackEnv[k] & ACTIVE_TRACK)!=0
							    && ( (trackEnv[k] & LOOP)!= 0
								   ||(trackEnv[k] & END_COMMAND_TRACK)!= 0)
								&& trackEndFlg[k])
								{
								    trackStatus[k] = 0;
									trackStartTime[k] = currentTime;//開始時刻
									trackEndFlg[k] = FALSE;//１曲未終了
								}
							}///end for(k=tr;k<tr+nTrack[j];k++)
						}///end if(wflg)
					}///end if(xflg)
				}///end if(loopFlg)
			}///end if(CheckLoopFlg[j])
			dflg = TRUE;                       // not output file
			aflg = FALSE;                      // siren
			for(k=0;k<cUpSeq;k++)
			{
				if(j == vUpSeq[k])
				{
					dflg = FALSE;    //演奏曲
				}
				if(seqSirenCommand[vUpSeq[k]] > 0)
				{
					aflg=TRUE;
				}
			}
			allDflg &= dflg;
			allTrackEndFlg = TRUE;
			for(ntr=tr;ntr<tr+nTrack[j];++ntr) 
			{
				allTrackEndFlg &= trackEndFlg[ntr];//１曲終了＝１曲分トラック終了
				if(trackEndFlg[ntr]) continue; //終了トラックは処理しない

  				if((st = trackStartTime[ntr]) > currentTime ) st = trackStartTime[ntr] = currentTime;      //開始時刻
			
				syscont=running=0;
				status = trackStatus[ntr];
				size = 	(long)(trackSize[ntr] - (pTrack[ntr] - pTopTrack[ntr]));
				if (size < 1L)        //ここには来ないはず
				{
					pTrack[ntr] = pTopTrack[ntr];
					size = (long)trackSize[ntr];
					st = trackStartTime[ntr] = currentTime;
					trackTics[ntr] = 0;
					trackStatus[ntr]=0;
				}

				rp=(unsigned char *)pTrack[ntr];//データ読み出し位置
				pflg=FALSE;
				while(TRUE) 
				{
					pos = rp;
					tics = SkipDeltaT(&rp,&size); //イベント開始tick
					if((trackEnv[ntr] & COMMAND_TRACK)==0
					&&	pos==(unsigned char *)pTopTrack[ntr])
					{
						if(tics < seqStartPoint[j])
						{
							trackTics[ntr] += tics;      //処理済みtics
							saveFlg=dflg;
							dflg=pflg=TRUE;
						}
					}
					else if(pflg)
					{
						if(tics+trackTics[ntr] < seqStartPoint[j])
						{
							trackTics[ntr] += tics;
						}
						else
						{
							pflg = FALSE;
							dflg=saveFlg;
						}
					}///end else if(pflg)
					if( !pflg && tics > 0 )
					{
						if((trackEnv[ntr] & COMMAND_TRACK)!=0) ftics = (float)(tics+trackTics[ntr]);
						else ftics = (float)(tics+trackTics[ntr]-seqStartPoint[j]);
						if( Tempo[j] > 0.0 
						&& (fwk= ftics / (float)division[j]	* Tempo[j] - (float)(currentTime - st)) > 0.0) //イベント開始時刻でない
						{
							pTrack[ntr] = (LPSTR)pos;    //読み出し位置を記憶して
							trackStatus[ntr] = status;
							if(fwk > 0.0 && fwk < fMinWait) fMinWait=fwk;
							break;                       //次のトラック処理へ
						}
					    else
						{
							trackTics[ntr] += tics;      //処理済みtics
						}
					}

					c=(unsigned char)Skip8(&rp,&size);   //先頭MIDIコード

					if(syscont && c != 0xf7) 
					{
						//AfxMessageBox("Bad file - expected continuation of system exclusive", MB_OK);
						TRACE("Bad file - expected continuation of system exclusive\n");
						size = -1L;
						trackEndFlg[ntr] = TRUE;
						break;
					}

					if((c & 0x80)==0x0) 
					{
						if(!status) 
						{
							//AfxMessageBox("Bad file - unexpected running status", MB_OK);
						    TRACE("Bad file - unexpected running status file=%d track=%d pos=%x\n",j,ntr-tr,pos-(unsigned char *)pTopTrack[ntr]);
							size = -1L;
							trackEndFlg[ntr] = TRUE;
							break;
						}
	//					if(j==0)
	//						TRACE("midiProc:status=%x,c=%x\n",status,c);
						running=1;
					}///end if((c & 0x80)==0x0) 
					else 
					{
						status = c;
						running = 0;
					}

					needed=chantype[(status>>4) & 0x0f];

					if(needed) 
					{		
						if(running) c1 = c;
						else 
						{
							c1 = Skip8(&rp,&size);
						}

						if(needed > 1)
						{
							c2=Skip8(&rp,&size);
						}
						else c2=0x0;
			//			if(j==0)
			//				TRACE("midiProc:status=%x,c1=%x,c2=%x\n",status,c1,c2);
						if(!dflg)
						{
								CurrentPort = seqOutPort[j];
							if ((trackEnv[ntr] & FIP_COMMAND_TRACK)!=0)
							{
			//			if(j==0)
			//				TRACE("midiProc:FIP\n");
								if(seqFIPCommand[j])
								{
									sendMIDIEvent(status,c1,c2);
								}
							}///end FIP_COMMAND_TRACK
							else if ((trackEnv[ntr] & FOP_COMMAND_TRACK)!=0)
							{
				//		if(j==0)
				//			TRACE("midiProc:FOP\n");
								if( (FOPMIDIChannel & incTable[(status & 0x0f)*CurrentPort])!=0
								&& seqFOPCommand[j])
						//			)
								{
									sendMIDIEvent(status,c1,c2);
									if(aflg)
									{
										for(k=0;k<32;k++)
										{
											sPort=CurrentPort;
											CurrentPort=(int)(k/16);
											cc=(unsigned char)(0x80 | (k % 16));
											for(k2=0;k2<128;k2++)
											{
												if(Note[k][k2])
												{
											//send note off
													sendMIDIEvent(cc,(unsigned char)k2,(unsigned char)0x40);
													Note[k][k2]=FALSE;
												}
											}
											CurrentPort=sPort;
										}
									}
								}
							}///end FOP_COMMAND_TRACK
							else if( (trackEnv[ntr] & START_COMMAND_TRACK)!=0)
							{
				//		if(j==0)
				//			TRACE("midiProc:START\n");
								sendMIDIEvent(status,c1,c2);
							}///end START_COMMAND_TRACK
							else if( (trackEnv[ntr] & END_COMMAND_TRACK)!=0)
							{
				//		if(j==0)
				//			TRACE("midiProc:END\n");
								for(k=tr,wflg=FALSE,xflg=TRUE;k<tr+nTrack[j];k++)
								{
									if(seqSirenCommand[j] > 0)
									{
										size=0;
										break;
									}///end if(seqSirenCommand[j] > 0)
									else if((trackEnv[k] & LOOP)!=0)
									{
										TRACE("midiProc:loop end %d\n",j);
										trackEndFlg[ntr] = TRUE;
										pTrack[ntr] = pTopTrack[ntr];
										trackTics[ntr] = 0;
										//
										CheckLoopFlg[j] = TRUE;
									}///end else if((trackEnv[k] & LOOP)!=0)
									if((trackEnv[k] & END_OPTION)!=0)
									{
										wflg=TRUE;
									}
								}///end for(k=tr,wflg=FALSE,xflg=TRUE;k<tr+nTrack[j];k++)
								if(xflg && wflg && !CheckLoopFlg[j]
	//								&& !CheckTempFlg
									)
								{
									sendMIDIEvent(status,c1,c2);
								}
							}///end END_COMMAND_TRACK
							else 
							{
								if((status & 0xf0)==0x90) //note on
						//		&& c2!=0x0)
								{
									Note[(status & 0x0f)*CurrentPort][(c1 & 0x7f)]=TRUE;
								}///end note on
								else if((status & 0xf0)==0x80) //note off
								{
									Note[(status & 0x0f)*CurrentPort][(c1 & 0x7f)]=FALSE;
								}///end note off
									sendMIDIEvent(status,c1,c2);
							}///end else if ((ActiveMIDIChannel & incTable[(status & 0x0f)])!=0)
						}///end if(!dflg)
					}///end if(needed)
					else
					{

						switch(c) 
						{
						case 0xff:
							type=Skip8(&rp,&size);
							lookfor = SkipVari(&rp,&size);
							lookfor = size - lookfor;
							b[0]=0;
							i=0;
							while(size > (long)lookfor) 
							{
								c=Skip8(&rp,&size);
								if(i < STRINGSIZE) b[i++]=c;
							}
							b[i]=0;
							AddMeta2(b,type);
							break;

						case 0xf0:
							lookfor = SkipDeltaT(&rp,&size);
							c = (char)(lookfor & 0x7f);     //save
							*(rp-1)=0xf0;                   //set
							if(!dflg) sendMIDILong((LPSTR)(rp-1),(UINT)(lookfor+1));//send
							*(rp-1)=c;                      //restore
							lookfor = size - lookfor;
							while(size > (long)lookfor) c=Skip8(&rp,&size);
							if(c!=0xf7) syscont = 1;
							break;
						case 0xf7:
							lookfor = SkipVari(&rp,&size);
							c = (char)(lookfor & 0x7f);     //save
							*(rp-1)=0xf7;                   //set
							if(!dflg) sendMIDILong((LPSTR)(rp-1),(UINT)(lookfor+1));//send
							*(rp-1)=c;                      //restore
							lookfor = size - lookfor;
							while(size > (long)lookfor) c=Skip8(&rp,&size);

							if(syscont && c ==0xf7) syscont=0;
							break;
						default:
							//AfxMessageBox("Bad file - unexpected message", MB_OK);
							TRACE("Bad file - unexpected message %d",j);
							size = -1L;
							trackEndFlg[ntr] = TRUE;
							rp++;
							size--;
							break;
						}
					}///end if(needed) else
					if (size < 1L)
					{
						TRACE("midiProc:end track %d\n",j);
						trackEndFlg[ntr] = TRUE;
						pTrack[ntr] = pTopTrack[ntr];
						trackTics[ntr] = 0;
						if((trackEnv[ntr] & FIP_COMMAND_TRACK)!=0)
						{
							FIPMIDIChannel &= (0xFFFFFFFF ^ seqChannel[j]);
						} 
						else if((trackEnv[ntr] & FOP_COMMAND_TRACK)!=0)
						{
							sPort=CurrentPort;
							for(k=0;k<32;k++)
							{
								CurrentPort=(int)(k/16);
								//note off
								if((FOPMIDIChannel & seqChannel[j] & incTable[k])!=0)
								{
									TRACE("midiProc:note off %d\n",k);
									c1=(unsigned char)(0x80 | (k % 16));
									for(k2=0;k2<128;k2++)
									{
										if(Note[k][k2])
										{
										//send note off
											sendMIDIEvent(c1,(unsigned char)k2,(unsigned char)'0x40');
											Note[k][k2]=FALSE;
										}
									}
								}
							}//end for(k=0;k<16;k++)
							CurrentPort=sPort;
							TRACE("midiProc:fade out %d\n",j);
							for(k=tr;k<tr+nTrack[j];k++)
							{
								trackEndFlg[k] = TRUE;
							    pTrack[k] = pTopTrack[k];
								trackTics[k] = 0;
							}
							ModifyCurrentSeq1(DEL_SEQ,j);
							if(seqTimerCommand[j]) 
							{
								for(k=tr;k<tr+nTrack[j];k++)
								{
									seqPlayTime[k].status=TIMER_WAIT;
								}
							}
							FOPMIDIChannel &= (0xFFFFFFFF ^ seqChannel[j]);
						}//End of if FOP_COMMAND_TRACK
						else if((trackEnv[ntr] & END_COMMAND_TRACK)!=0)
						{
							TRACE("midiProc:end track %d\n",j);
							for(k=tr;k<tr+nTrack[j];k++)
							{
									trackEndFlg[k] = TRUE;
									pTrack[k] = pTopTrack[k];
									trackTics[k] = 0;
							}
							if(seqInitial[j]>0)
							{
								if(
									seqInitial[j]>1)
								{
									ModifyCurrentSeq1(DEL_SEQ,j);
								}
								else
								{
									for(k=0;k<cUpSeq;k++)
									{
										if(vUpSeq[k]==j)
										{
											k2=k;
											break;
										}
									}
									for(k=0,kmin=100,wflg=TRUE;k<nseq;k++)
									{
										if(seqInitial[k]>seqInitial[j]
										&& seqInitial[k]<(int)kmin)
										{
											kmin=seqInitial[k];
											wflg=FALSE;
											vUpSeq[k2]=k;
										}
									}
									if(wflg)
									{
										ModifyCurrentSeq1(DEL_SEQ,j);
										start = FALSE;
									}
									else
									{
										for(k=trk=0;k<vUpSeq[k2];k++)
										{
											trk+=nTrack[k];
										}
										for(k=trk;k<trk+nTrack[vUpSeq[k2]];k++)
										{
											trackEndFlg[k]=FALSE;
											trackStartTime[k]=currentTime;
										}
										allTrackEndFlg = FALSE;
									}
								}
							}//end if seqInitial
							for(k=tr;k<tr+nTrack[j];k++)
							{
									if(seqInitial[j]==0
									&& (trackEnv[k] & ACTIVE_TRACK)!=0
									&& (trackEnv[k] & COMMAND_TRACK)==0
									&& ((trackEnv[k] & ONE_SHOT)!=0
										|| (trackEnv[k] & LOOP)==0))
								{
									ModifyCurrentSeq1(DEL_SEQ,j);
								}
								else if((trackEnv[k] & LOOP)!=0)
								{
									CheckLoopFlg[j] = TRUE;
								}
							}///end for(k=tr;k<tr+nTrack[j];k++)
						}
						else if((trackEnv[ntr] & ACTIVE_TRACK)!=0
							&& (trackEnv[ntr] & COMMAND_TRACK)==0
							&& (trackEnv[ntr] & END_OPTION)==0)
						{
							if(seqInitial[j]>0)
							{
								if(
									seqInitial[j]>1)
								{
									ModifyCurrentSeq1(DEL_SEQ,j);
								}
								else
								{
									for(k=0;k<cUpSeq;k++)
									{
										if(vUpSeq[k]==j)
										{
											k2=k;
											break;
										}
									}
									for(k=0,kmin=100,wflg=TRUE;k<nseq;k++)
									{
										if(seqInitial[k]>seqInitial[j]
											&& seqInitial[k]<(int)kmin)
										{
											kmin=seqInitial[k];
											wflg=FALSE;
											vUpSeq[k2]=k;
										}
									}
									if(wflg)
									{
										ModifyCurrentSeq1(DEL_SEQ,j);
										start = FALSE;
									}
									else
									{
										for(k=trk=0;k<vUpSeq[k2];k++)
										{
											trk+=nTrack[k];
										}
										for(k=trk;k<trk+nTrack[vUpSeq[k2]];k++)
										{
											trackEndFlg[k]=FALSE;
											trackStartTime[k]=currentTime;
										}
										allTrackEndFlg = FALSE;
									}
								}
							}//end if seqInitial
							  if(seqInitial[j]==0
								&& ((trackEnv[ntr] & ONE_SHOT)!=0
									|| (trackEnv[ntr] & LOOP)==0))
							{
								ModifyCurrentSeq1(DEL_SEQ,j);
							}
							if((trackEnv[ntr] & LOOP)!=0)
							{
								TRACE("midiProc:checkloop %d",j);
								CheckLoopFlg[j] = TRUE;
							}
						}
						break;
					}///end if(size < 1L)
				} //while(TRUE)
			} // tr < tracks
		if(!dflg) allSeqEndFlg &= allTrackEndFlg;
		} // j < nfiles
	}
	/*			*/
	/*			*/
	/*			*/
//	if(CurrentPort==DEFAULT_MIDI_OUT_PORT){
#ifndef DEBUG_PRESET
		midiOutReset(hMidiout[ROLAND_SMPU_MIDI_OUT_A]); // all note off
		midiOutReset(hMidiout[ROLAND_SMPU_MIDI_OUT_B]); // all note off
#endif
//	}else{
//		midiOutReset(hMidiout[lCurrentPort]); // all note off
//	}
	/*			*/
	/*			*/
	/*			*/
	delete []CheckLoopFlg;
	MidiThread = (CWinThread *)NULL;
	//TRACE("midiProc:exit");
	return 0; // normal
}

UINT MidiThreadProc( LPVOID pParam )
{
	Cqueens3mid *pObj = (Cqueens3mid *)pParam;

	if(pObj == NULL ||
		!pObj->IsKindOf(RUNTIME_CLASS(Cqueens3mid)))
		return(1);

//	AfxMessageBox("MidiProc()", MB_OK);

	pObj->MidiProc();
	//AfxEndThread(0);

	return 0;
}

void Cqueens3mid::ModifyCurrentSeq1(int type,int seq)
{
	BOOL flg;
	UINT i,j;

	flg=TRUE;
	while(flg)
	{
		while(seqLock1 || seqLock2){}
		seqLock1=TRUE;
		if(seqLock2) seqLock1=FALSE;
		else
		{
			seqLock2=TRUE;
			flg=FALSE;
		}
	}
	if(type==ADD_SEQ)
	{
		flg = TRUE;
		for(i=0;i<cUpSeq;i++)
		{
			if(seq == (int)vUpSeq[i])
			{
				flg = FALSE;
				break;
			}
		}
		if(flg)
		{
				vUpSeq[cUpSeq]=seq;
				cUpSeq++;
			//}
		}
	}
	else
	{
		for(i=0;i<cUpSeq;i++)
		{
			if(vUpSeq[i]==(UINT)seq)
			{
				for(j=i;j<cUpSeq;j++)
				{
					vUpSeq[j]=vUpSeq[j+1];
				}
				cUpSeq--;
				break;
			}
		}
	}
	seqLock1=seqLock2=FALSE;
}

void Cqueens3mid::ModifyCurrentSeq2(int type,int seq)
{
	BOOL flg;
	UINT i,j;

	flg=TRUE;
	while(flg)
	{
		while(seqLock1 || seqLock2){}
		seqLock2=TRUE;
		if(seqLock1) seqLock2=FALSE;
		else
		{
			seqLock1=TRUE;
			flg=FALSE;
		}
	}
	if(type==ADD_SEQ)
	{
		flg = TRUE;
		for(i=0;i<cUpSeq;i++)
		{
			if(seq == (int)vUpSeq[i])
			{
				flg = FALSE;
				break;
			}
		}
		if(flg)
		{
				vUpSeq[cUpSeq]=seq;
				cUpSeq++;
			//}
		}
	}
	else
	{
		for(i=0;i<cUpSeq;i++)
		{
			if(vUpSeq[i]==(UINT)seq)
			{
				for(j=i;j<cUpSeq;j++)
				{
					vUpSeq[j]=vUpSeq[j+1];
				}
				cUpSeq--;
				break;
			}
		}
	}
	seqLock1=seqLock2=FALSE;
}


void Cqueens3mid::StopPart(int part)
{
	int seq;
	int type;
	int i,j,k,tr;
	BOOL dflg;
//	int ii;
//	UINT cUpSeqOrg;
//	int rm = 0;

	type = DEL_SEQ;
//	TRACE("part = %d\n",part);
//	TRACE("cUpSeq = %d\n",cUpSeq);
//	for(i=0;i<cUpSeq;i++)
//	{
//		TRACE("%d(%d)  ",vUpSeq[i],(int)seqGroup[vUpSeq[i]]);
//	}
//	TRACE("\n");
	
//	cUpSeqOrg = cUpSeq;
	for(i=0;i<cUpSeq;i++)
	{
//		TRACE("cUpSeq = %d\n",cUpSeq);
//		TRACE("i = %d \n",i);
		
		if(part == (int)seqGroup[vUpSeq[i]])
		{
//			TRACE("remove %d(%d)\n",vUpSeq[i],(int)seqGroup[vUpSeq[i]]);
			seq = (int)vUpSeq[i];

//			sensors->UnsetTimer(part);

			if(seqSirenCommand[seq] > 0)
			{
				for(j=tr=0;j<seq;j++)
				  tr += nTrack[j];
				if(seqFOPCommand[seq])
				{
					for(k=tr,dflg=TRUE;k<tr+(int)nTrack[seq];k++)
					{
						if((trackEnv[k] & ACTIVE_TRACK)!=0
						&& (trackEnv[k] & COMMAND_TRACK)==0
						&& (trackEnv[k] & FADE_OUT_OPTION)!=0)
						{
							dflg=FALSE;
						}
					}
					if(!dflg)
					{
				        TRACE("StopPart:shut FO\n");
						for(k=tr,dflg=TRUE;k<tr+(int)nTrack[seq];k++)
						{
							if((trackEnv[k] & FOP_COMMAND_TRACK)!=0)
							{
								if(trackEndFlg[k])
								{
								    FOPMIDIChannel |= seqChannel[seq];
								    pTrack[k] = pTopTrack[k];
								    trackTics[k] = 0;
								    trackStatus[k] = 0;
								    trackStartTime[k] = currentTime;
								    trackEndFlg[k] = FALSE;
								}//end if
								dflg=FALSE;//Fade out起動した
							}
						}//end if
					}//end for
				}
				else
				{
  			        ModifyCurrentSeq2(type,seq);
				    for(k=tr;k<tr+(int)nTrack[seq];k++)
				    {
					    trackEndFlg[k]=TRUE;
	//				    if(seqPlayTime[k].status==TIMER_DONE)
	//				    {
	//					    seqPlayTime[k].status=TIMER_WAIT;
	//				    }
				    }
				}//end else
			} //end if
		} //end if
	} //end for
//	for(i=0;i<cUpSeq;i++)
//	{
//		TRACE("%d  ",vUpSeq[i]);
//	}
//	TRACE("\n");
	
}


void Cqueens3mid::deleteMem()
{
	if(pbuffer != NULL)	delete []pbuffer;
	if(pTopTrack != NULL) delete []pTopTrack;
	if(pBottomTrack != NULL) delete []pBottomTrack;
	if(pTrack != NULL) delete []pTrack;
	if(trackSize != NULL) delete []trackSize;
	if(trackStartTime != NULL) delete []trackStartTime;
	if(trackTics != NULL) delete []trackTics;
	if(trackEndFlg != NULL) delete []trackEndFlg;
	if(trackStatus != NULL) delete []trackStatus;
	if(nTrack != NULL)	delete []nTrack;
	if(vUpSeq != NULL) delete []vUpSeq;
	if(Tempo != NULL) delete []Tempo;
	if(division != NULL) delete []division;
	if(seqInitial != NULL) delete []seqInitial;
	if(trackEnv != NULL) delete []trackEnv;
	if(seqStartCommand != NULL) delete []seqStartCommand;
	if(seqEndCommand != NULL) delete []seqEndCommand;
	if(seqFIPCommand != NULL) delete []seqFIPCommand;
	if(seqFOPCommand != NULL) delete []seqFOPCommand;
	if(seqTimerCommand != NULL) delete []seqTimerCommand;
	if(seqSirenCommand != NULL) delete []seqSirenCommand;
	if(seqStartPoint != NULL) delete []seqStartPoint;
	if(seqEndPoint != NULL) delete []seqEndPoint;
	if(seqStartData != NULL) delete []seqStartData;
	if(seqEndData != NULL) delete []seqEndData;
	if(seqFIPData != NULL) delete []seqFIPData;
	if(seqFOPData != NULL) delete []seqFOPData;
	if(seqOutPort != NULL) delete []seqOutPort;
	if(seqChannel != NULL) delete []seqChannel;
	if(seqMid != NULL) delete []seqMid;
	if(seqPid != NULL) delete []seqPid;
//	if(seqPriority != NULL) delete []seqPriority;
	if(seqGroup != NULL) delete []seqGroup;
	if(seqPlayTime != NULL) delete []seqPlayTime;
	if(pTimerEntry != NULL) delete []pTimerEntry;
}
//++++++++++++++++++++++++++++++++++++++++++++++
